package com.etoile.makeupbarr.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by AndroidPC on 07-02-2017.
 */

public class SampleFragmentPagerAdapter extends FragmentPagerAdapter {
    private final ArrayList<String> list;
    private String tabTitles[] = new String[] { "Discription", "Tips", "Ingrdients" ,"Shopping Return"};

    public SampleFragmentPagerAdapter(FragmentManager fm, ArrayList<String> list) {
        super(fm);
        this.list=list;
    }

    @Override
    public int getCount() {
        return tabTitles.length;
    }

    @Override
    public Fragment getItem(int position) {
        if(position==0)
        {
            return PageFragment.newInstance(position+1,list.get(0));
        }
        if(position==1)
        {
            return PageFragment.newInstance(position+1,list.get(1));
        }
        if(position==2)
        {
            return PageFragment.newInstance(position+1,list.get(2));
        }
        if(position==3)
        {
            return PageFragment.newInstance(position+1,list.get(3));
        }


        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }
}