package com.etoile.makeupbarr.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.activity.ZoomImageView;
import com.etoile.makeupbarr.model.DataForProductDetail;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by AndroidPC on 28-01-2017.
 */
public class CustomImageViewAdapter extends RecyclerView.Adapter {
    private final ZoomImageView zoomImageView;
    private final List<DataForProductDetail.ResponseBean.ProductImagesBean> userlist;

    public CustomImageViewAdapter(ZoomImageView zoomImageView, List<DataForProductDetail.ResponseBean.ProductImagesBean> product_images) {
        this.zoomImageView=zoomImageView;
        userlist=product_images;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.image_product_zoom, parent, false);
        holder = new ItemViewHolder(itemView);
        Context context = parent.getContext();
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final ItemViewHolder itemHolder = (ItemViewHolder) holder;
        DataForProductDetail.ResponseBean.ProductImagesBean productImagesBean = userlist.get(position);
        String bigUrl = productImagesBean.getBigUrl();
        Picasso.with(zoomImageView).load(bigUrl).into(itemHolder.imageView);
  itemHolder.image.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
          zoomImageView.finish();
      }
  });
    }

    @Override
    public int getItemCount() {

        return userlist.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView,image;
        public ItemViewHolder(View itemView) {
            super(itemView);
            imageView=(ImageView) itemView.findViewById(R.id.imgItemHome);
            image=(ImageView) itemView.findViewById(R.id.image);
        }
    }
}
