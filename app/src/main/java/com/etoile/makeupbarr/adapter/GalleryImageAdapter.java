package com.etoile.makeupbarr.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.etoile.makeupbarr.R;

/**
 * Created by admin on 1/24/2017.
 */

public class GalleryImageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    int[] mResources;
    RecyclerView recyclerView;


    public GalleryImageAdapter(Context ctx, int[] mResources, RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
        this.mResources = mResources;
    }

    @Override
    public int getItemCount() {

        return mResources.length;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.image_gallery_items, parent, false);
        holder = new ItemViewHolder(itemView);
        Context context = parent.getContext();
        return holder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final ItemViewHolder itemHolder = (ItemViewHolder) holder;
        itemHolder.add_wishlist.setImageResource(mResources[position]);


    }


    public class ItemViewHolder extends RecyclerView.ViewHolder {
        ImageView add_wishlist;

        public ItemViewHolder(View itemView) {
            super(itemView);
            add_wishlist = (ImageView) itemView.findViewById(R.id.image);

        }
    }


}
