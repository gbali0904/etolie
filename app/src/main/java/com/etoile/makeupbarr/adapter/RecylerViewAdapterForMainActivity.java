package com.etoile.makeupbarr.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.Utility.TAGS;
import com.etoile.makeupbarr.activity.AddRemoveWishList;
import com.etoile.makeupbarr.activity.ProductDetailActivity;
import com.etoile.makeupbarr.login.LoginActivity;
import com.etoile.makeupbarr.model.DataForAddRecentView;
import com.etoile.makeupbarr.model.DataForBanner;
import com.etoile.makeupbarr.model.DataForDrawerCategories;
import com.etoile.makeupbarr.model.DataForLogin;
import com.etoile.makeupbarr.model.DataForProductList;
import com.pixelcan.inkpageindicator.InkPageIndicator;

import java.util.List;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by admin on 12/3/2016.
 */
@SuppressLint("NewApi")
public class RecylerViewAdapterForMainActivity extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    public static final int TYPE_HEADER = 0;
    public static final int TYPE_ITEM = 1;
    public static final int TYPE_FOOTER = 2;
    private static final int COLUMN = 2;
    private static OnUpdateListener mListener;
    private final CoordinatorLayout coLay1;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    private Context mContext;
    private List<DataForBanner.ResponseBean> response1;
    private List<DataForProductList.ResponseBean> responseDataForProductList;
    private AdapterforCategoryInAMinActivity recyclerViewAdapter;
    private DataForAddRecentView dataForAddRecentView;
    private String product_id;


    public RecylerViewAdapterForMainActivity(Context context, List<DataForProductList.ResponseBean> responseDataForProductList, CoordinatorLayout coLay) {
        mContext = context;
        this.responseDataForProductList = responseDataForProductList;
        this.coLay1 = coLay;
    }

    public static void setOnUpdateListener(OnUpdateListener listener) {
        mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View v1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_item, parent, false);
            return new HeaderViewHolder(v1);
        } else if (viewType == TYPE_FOOTER) {
            View v2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.footer_item, parent, false);
            return new FooterViewHolder(v2);
        } else if (viewType == TYPE_ITEM) {
            View v3 = LayoutInflater.from(parent.getContext()).inflate(R.layout.pacakge_card_adp, parent, false);
            return new GenericViewHolder(v3);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        latestListGeneric(holder, position);
        headerBannerList(holder);
        footerDiscountList(holder);
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        } else if (isPositionFooter(position)) {
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    private boolean isPositionFooter(int position) {
        return position == responseDataForProductList.size() + 1;
    }

    @Override
    public int getItemCount() {
        return (null != responseDataForProductList ? responseDataForProductList.size() + 2 : 0);
    }

    /*BIND VIEW METHOD */
    private void footerDiscountList(RecyclerView.ViewHolder holder) {

        if (holder instanceof FooterViewHolder) {
            final FooterViewHolder glider = (FooterViewHolder) holder;
            int[] mResources = {R.drawable.offer_special,
                    R.drawable.offer_special1,
                    R.drawable.offer_special2, R.drawable.offer_special3};
            glider.recyclerView.setHasFixedSize(true);
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
            glider.recyclerView.setLayoutManager(mLayoutManager);
            glider.recyclerView.setItemAnimator(new DefaultItemAnimator());
            glider.recyclerView.setAdapter(new GalleryImageAdapter(mContext, mResources, glider.recyclerView));
            if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
                String string = AppController.getInstance().getPreference().getString(TAGS.JSON_USER_DATA, "");
                DataForLogin dataForLogin = AppController.getInstance().getGson().fromJson(string, DataForLogin.class);
                DataForLogin.ResponseBean response = dataForLogin.getResponse();
                String uid = response.getUid();

                AppController.getInstance().getRetrofitServices().productlist("", "", "", "", "", "discount", uid, "").enqueue(new Callback<DataForProductList>() {
                    @Override
                    public void onResponse(Call<DataForProductList> call, Response<DataForProductList> response) {
                        glider.progress_container.setVisibility(View.GONE);
                        DataForProductList loginJsonData = response.body();
                        if (loginJsonData.getStatus().equals("success")) {
                            List<DataForProductList.ResponseBean> response2 = loginJsonData.getResponse();
                            glider.recyclerView1.setHasFixedSize(true);
                            LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                            glider.recyclerView1.setLayoutManager(mLayoutManager);
                            glider.recyclerView1.setItemAnimator(new DefaultItemAnimator());
                            glider.recyclerView1.setAdapter(new RecentViews_Adapter(mContext, response2, glider.recyclerView1, coLay1));
                        } else {
                        }
                    }

                    @Override
                    public void onFailure(Call<DataForProductList> call, Throwable t) {

                    }
                });
            } else {
                AppController.getInstance().getRetrofitServices().productlist("", "", "", "", "", "discount", "", "").enqueue(new Callback<DataForProductList>() {
                    @Override
                    public void onResponse(Call<DataForProductList> call, Response<DataForProductList> response) {
                        glider.progress_container.setVisibility(View.GONE);
                        DataForProductList loginJsonData = response.body();
                        if (loginJsonData.getStatus().equals("success")) {
                            List<DataForProductList.ResponseBean> response2 = loginJsonData.getResponse();
                            glider.recyclerView1.setHasFixedSize(true);
                            LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                            glider.recyclerView1.setLayoutManager(mLayoutManager);
                            glider.recyclerView1.setItemAnimator(new DefaultItemAnimator());
                            glider.recyclerView1.setAdapter(new RecentViews_Adapter(mContext, response2, glider.recyclerView1, coLay1));
                        } else {
                        }
                    }

                    @Override
                    public void onFailure(Call<DataForProductList> call, Throwable t) {

                    }
                });

            }
        }

    }

    private void headerBannerList(RecyclerView.ViewHolder holder) {
        if (holder instanceof HeaderViewHolder) {
            final HeaderViewHolder slider = (HeaderViewHolder) holder;
            //  slider.recyclerView.setHasFixedSize(true);
            //  slider.recyclerView.setLayoutManager(new GridLayoutManager(mContext, 2));

            AppController.getInstance().getRetrofitServices().banner().enqueue(new Callback<DataForBanner>() {
                @Override
                public void onResponse(Call<DataForBanner> call, Response<DataForBanner> response) {
                    slider.progress_container.setVisibility(View.GONE);
                    DataForBanner jsonforbanner = response.body();
                    if (jsonforbanner.getStatus().equalsIgnoreCase("success")) {
                        response1 = jsonforbanner.getResponse();
                        slider.sliderMain.setScrollDurationFactor(7);
                        slider.sliderMain.setAdapter(new PageAdaptewrforBanner(mContext, response1));
                        slider.sliderMain.setInterval(10000);
                        slider.sliderMain.setCycle(true);
                        slider.inkPageIndicator.setViewPager(slider.sliderMain);
                        slider.sliderMain.startAutoScroll();
                    } else {
                        Toast.makeText(mContext, jsonforbanner.getMessage(), Toast.LENGTH_LONG).show();

                    }
                }

                @Override
                public void onFailure(Call<DataForBanner> call, Throwable t) {
                    Toast.makeText(mContext, "Server error can't download the categories", Toast.LENGTH_LONG).show();
                }
            });
            View.OnClickListener listener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            };
            slider.sliderMain.setOnClickListener(listener);
            slider.sliderMain.setTag(holder);


            String string = AppController.getInstance().getPreference().getString(TAGS.JSON_CATEGORY, "");
            DataForDrawerCategories dataForDrawerCategories = AppController.getInstance().getGson().fromJson(string, DataForDrawerCategories.class);
            final List<DataForDrawerCategories.ResponseBean> response = dataForDrawerCategories.getResponse();


            recyclerViewAdapter = new AdapterforCategoryInAMinActivity(mContext, response);
            slider.recyclerView.setAdapter(recyclerViewAdapter);


        }
    }

    private void latestListGeneric(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof GenericViewHolder) {
            final GenericViewHolder genericViewHolder = (GenericViewHolder) holder;
            final DataForProductList.ResponseBean responseBean = responseDataForProductList.get(position - 1);
            String image_url = responseBean.getImage_url();
            genericViewHolder.imgThumbnail.setImageUrl(image_url, imageLoader);
            genericViewHolder.tv_price.setText(responseBean.getPrice());
            genericViewHolder.tv_product_name.setText(responseBean.getProduct_name());
            product_id = responseBean.getProduct_id();
            String price = responseBean.getPrice();
            String decimalprice = AppController.getInstance().decimalValue(Integer.parseInt(price));
            genericViewHolder.tv_price.setText("INR " + decimalprice);
            if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {

                String wishlist = responseBean.getWishlist();
                if (wishlist.equals("0")) {
                    int black = Color.parseColor("black");
                    genericViewHolder.add_wishlist.setColorFilter(black);
                } else {
                    int red = Color.parseColor("red");
                    genericViewHolder.add_wishlist.setColorFilter(red);
                }

                wishListAddRemoveMethod(genericViewHolder, position);


            } else {

                wishListAddRemoveMethod(genericViewHolder, position);


            }


            genericViewHolder.imgThumbnail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    addRecentView(genericViewHolder, position);
                    AppController.getInstance().getPreferenceEditor().putString(TAGS.PRODUCT_ID, responseBean.getProduct_id()).commit();
                    Intent intent = new Intent(mContext, ProductDetailActivity.class);
                    intent.putExtra(TAGS.FLAG, "true");
                    mContext.startActivity(intent);
                }
            });
            genericViewHolder.relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addRecentView(genericViewHolder, position);
                    AppController.getInstance().getPreferenceEditor().putString(TAGS.PRODUCT_ID, responseBean.getProduct_id()).commit();
                    Intent intent = new Intent(mContext, ProductDetailActivity.class);
                    intent.putExtra(TAGS.FLAG, "true");
                    mContext.startActivity(intent);
                }
            });

        }
    }

    private void addRecentView(final GenericViewHolder genericViewHolder, final int position) {
        if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {

            String userdata = AppController.getInstance().getPreference().getString(TAGS.JSON_USER_DATA, "");
            DataForLogin dataForLogin = AppController.getInstance().getGson().fromJson(userdata, DataForLogin.class);
            DataForLogin.ResponseBean response = dataForLogin.getResponse();
            final String uid = response.getUid();
            AppController.getInstance().getPreferenceEditor().putString(TAGS.RECENT_ADD, "").commit();

            AppController.getInstance().getRetrofitServices().recentView(uid, product_id).enqueue(new Callback<DataForAddRecentView>() {
                @Override
                public void onResponse(Call<DataForAddRecentView> call, Response<DataForAddRecentView> response) {
                    dataForAddRecentView = response.body();
                    if (dataForAddRecentView.getStatus().equals("success")) {
                        AppController.getInstance().getPreferenceEditor().putString(TAGS.RECENT_ADD, dataForAddRecentView.getMessage()).commit();
                        Snackbar snackbar = Snackbar.make(coLay1, dataForAddRecentView.getMessage(), Snackbar.LENGTH_LONG);
                        snackbar.show();
                    } else {
                        Snackbar snackbar = Snackbar.make(coLay1, dataForAddRecentView.getMessage(), Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                }

                @Override
                public void onFailure(Call<DataForAddRecentView> call, Throwable t) {
                    Toast.makeText(mContext, "Server error can't download the categories", Toast.LENGTH_LONG).show();
                }
            });
        } else {
            AppController.getInstance().getPreferenceEditor().putString(TAGS.RECENT_ADD, "").commit();
            AppController.getInstance().getRetrofitServices().recentView("", product_id).enqueue(new Callback<DataForAddRecentView>() {
                @Override
                public void onResponse(Call<DataForAddRecentView> call, Response<DataForAddRecentView> response) {
                    dataForAddRecentView = response.body();
                    if (dataForAddRecentView.getStatus().equals("success")) {
                        AppController.getInstance().getPreferenceEditor().putString(TAGS.RECENT_ADD, dataForAddRecentView.getMessage()).commit();
                       /* Snackbar snackbar = Snackbar.make(coLay1, dataForAddRecentView.getMessage(), Snackbar.LENGTH_LONG);
                        snackbar.show();*/
                    } else {
                        /*Snackbar snackbar = Snackbar.make(coLay1, dataForAddRecentView.getMessage(), Snackbar.LENGTH_LONG);
                        snackbar.show();*/
                    }
                }

                @Override
                public void onFailure(Call<DataForAddRecentView> call, Throwable t) {
                    Toast.makeText(mContext, "Server error can't download the categories", Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    private void wishListAddRemoveMethod(final GenericViewHolder genericViewHolder, final int position) {

        genericViewHolder.add_wishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
                    DataForProductList.ResponseBean responseBean = responseDataForProductList.get(position - 1);

                    String product_id = responseBean.getProduct_id();
                    String jsonValueforProductList = AppController.getInstance().getGson().toJson(responseDataForProductList);
                    AppController.getInstance().getPreferenceEditor().putString(TAGS.PRODUCT_LIST, product_id).commit();
                    Log.e("myid", product_id);

                    AddRemoveWishList addRemoveWishList = new AddRemoveWishList();
                    addRemoveWishList.getResponseData(mContext, coLay1);
                    String string = AppController.getInstance().getPreference().getString(TAGS.WISHLIST, "");

                    if (string.equals("product wishlist added successfully")) {
                        int black = Color.parseColor("black");
                        genericViewHolder.add_wishlist.setColorFilter(black);

                    } else {
                        int red = Color.parseColor("red");
                        genericViewHolder.add_wishlist.setColorFilter(red);
                    }
                    if (mListener != null) {
                        mListener.onUpdate();
                    }
                } else {
                    Intent i = new Intent(mContext, LoginActivity.class);
                    mContext.startActivity(i);

                }

            }
        });
    }

    public interface OnUpdateListener {
        void onUpdate();
    }

    /*RECYCLERVIEW HOLDER */
    private class FooterViewHolder extends RecyclerView.ViewHolder {
        FrameLayout progress_container;
        RecyclerView recyclerView1;
        private RecyclerView recyclerView;

        FooterViewHolder(View itemView) {
            super(itemView);
            recyclerView = (RecyclerView) itemView.findViewById(R.id.category_list);
            recyclerView1 = (RecyclerView) itemView.findViewById(R.id.category_list1);
            progress_container = (FrameLayout) itemView.findViewById(R.id.progress_container);
        }
    }

    private class HeaderViewHolder extends RecyclerView.ViewHolder {

        FrameLayout progress_container;
        RecyclerView recyclerView;
        private AutoScrollViewPager sliderMain;
        private InkPageIndicator inkPageIndicator;

        HeaderViewHolder(View itemView) {
            super(itemView);
            sliderMain = (AutoScrollViewPager) itemView.findViewById(R.id.slider_main);
            inkPageIndicator = (InkPageIndicator) itemView.findViewById(R.id.indicator);
            progress_container = (FrameLayout) itemView.findViewById(R.id.progress_container);
            recyclerView = (RecyclerView) itemView.findViewById(R.id.category_list);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new GridLayoutManager(mContext, 3));
        }
    }

    private class GenericViewHolder extends RecyclerView.ViewHolder {

        public NetworkImageView imgThumbnail;
        CardView card_catogery;
        TextView tv_product_name, tv_price;
        RecyclerView recyclerView;
        ImageView add_wishlist;
        RelativeLayout relativeLayout;

        GenericViewHolder(View itemView) {
            super(itemView);
            card_catogery = (CardView) itemView.findViewById(R.id.card_view);
            imgThumbnail = (NetworkImageView) itemView.findViewById(R.id.img_thumbnail);
            tv_product_name = (TextView) itemView.findViewById(R.id.tv_product_name);
            tv_price = (TextView) itemView.findViewById(R.id.tv_price);
            add_wishlist = (ImageView) itemView.findViewById(R.id.add_wishlist);
            relativeLayout = (RelativeLayout) itemView.findViewById(R.id.linearLayout2);
        }
    }

}