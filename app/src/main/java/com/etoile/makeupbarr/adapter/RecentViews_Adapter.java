package com.etoile.makeupbarr.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.Utility.TAGS;
import com.etoile.makeupbarr.activity.AddRemoveWishList;
import com.etoile.makeupbarr.activity.ProductDetailActivity;
import com.etoile.makeupbarr.login.LoginActivity;
import com.etoile.makeupbarr.model.DataForProductList;

import java.util.List;

/**
 * Created by admin on 1/18/2017.
 */

public class RecentViews_Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static OnUpdateListener mListener;
    private final Context context;
    private final CoordinatorLayout coLay1;
   private RecyclerView recyclerView;
    List<DataForProductList.ResponseBean> userList;
    private DataForProductList.ResponseBean user;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();


    public RecentViews_Adapter(Context context, List<DataForProductList.ResponseBean> userList, RecyclerView recyclerView, CoordinatorLayout coLay1) {
        this.context = context;
        this.userList = userList;
        this.recyclerView = recyclerView;
        this.coLay1=coLay1;
    }

    @Override
    public int getItemCount() {

        return userList.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recent_view_list, parent, false);
        holder = new ItemViewHolder(itemView);
        Context context = parent.getContext();
        return holder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final ItemViewHolder itemHolder = (ItemViewHolder) holder;
        user = userList.get(position);
        itemHolder.product_name.setText(user.getProduct_name());

        String price = user.getPrice();
        int listPrice=Integer.parseInt(price);

        String discount = user.getDiscount();
        int discount_int=Integer.parseInt(discount);
        int discountPrice = listPrice - ((listPrice * discount_int) / 100);
        String my_discount=""+discountPrice;

        if(discount.equals("0"))
        {   itemHolder.tv_price_with_dicount.setVisibility(View.GONE);
            itemHolder.tv_price_percentage.setVisibility(View.GONE);
            String decimalprice = AppController.getInstance().decimalValue(Integer.parseInt(price));
            itemHolder.tv_price.setText("INR "+decimalprice);
        }
        else
        {
            String decimalprice = AppController.getInstance().decimalValue(Integer.parseInt(my_discount));
            itemHolder.tv_price.setText("INR "+decimalprice);
            itemHolder.tv_price_with_dicount.setHint(price);
            itemHolder.tv_price_with_dicount.setPaintFlags(itemHolder.tv_price_with_dicount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            itemHolder.tv_price_percentage.setText(discount+"%");
        }

        if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {

            String wishlist = user.getWishlist();
            if (wishlist.equals("0")) {
                int black = Color.parseColor("black");
                itemHolder.add_wishlist.setColorFilter(black);
            } else {
                int red = Color.parseColor("red");
                itemHolder.add_wishlist.setColorFilter(red);
            }
            wishListAddRemoveMethod(itemHolder, position);

        }
        else {
            wishListAddRemoveMethod(itemHolder, position);

        }

        itemHolder.imageView.setImageUrl(user.getImage_url(), imageLoader);

        Log.e("mydata",user.getProduct_id());

        itemHolder.linear_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DataForProductList.ResponseBean responseBean = userList.get(position);
                AppController.getInstance().getPreferenceEditor().putString(TAGS.PRODUCT_ID,responseBean.getProduct_id()).commit();
                Intent intent=new Intent(context, ProductDetailActivity.class);
                intent.putExtra(TAGS.FLAG,"false");
                context.startActivity(intent);

            }
        }); itemHolder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DataForProductList.ResponseBean responseBean = userList.get(position);
                AppController.getInstance().getPreferenceEditor().putString(TAGS.PRODUCT_ID,responseBean.getProduct_id()).commit();
                Intent intent=new Intent(context, ProductDetailActivity.class);
                intent.putExtra(TAGS.FLAG,"false");
                context.startActivity(intent);

            }
        });

    }

    private void wishListAddRemoveMethod(final ItemViewHolder itemHolder, final int position) {

        itemHolder.add_wishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
                    DataForProductList.ResponseBean responseBean =userList.get(position);
                    String product_id = responseBean.getProduct_id();
                    String jsonValueforProductList = AppController.getInstance().getGson().toJson(userList);
                    AppController.getInstance().getPreferenceEditor().putString(TAGS.PRODUCT_LIST, product_id).commit();
                    Log.e("myid", product_id);

                    AddRemoveWishList addRemoveWishList = new AddRemoveWishList();
                    addRemoveWishList.getResponseData(context, coLay1);
                    String string = AppController.getInstance().getPreference().getString(TAGS.WISHLIST, "");

                    if (string.equals("product wishlist added successfully")) {
                        int black = Color.parseColor("black");
                        itemHolder.add_wishlist.setColorFilter(black);

                    } else {
                        int red = Color.parseColor("red");
                        itemHolder.add_wishlist.setColorFilter(red);

                    }
                    if (mListener != null) {
                        mListener.onUpdate();
                    }
                }
                else {
                    Intent i = new Intent(context, LoginActivity.class);
                    context.startActivity(i);

                }

            }
        });
    }
    public static void setOnUpdateListener(OnUpdateListener listener) {
        mListener = listener;
    }

    public interface OnUpdateListener {
        void onUpdate();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        NetworkImageView imageView;
        TextView product_name, tv_price, tv_price_with_dicount, tv_price_percentage;
        LinearLayout linear_layout;
        ImageView add_wishlist;
        public ItemViewHolder(View itemView) {

            super(itemView);
            imageView = (NetworkImageView) itemView.findViewById(R.id.thumbnail);
            product_name = (TextView) itemView.findViewById(R.id.tv_product_name);
            tv_price = (TextView) itemView.findViewById(R.id.tv_price);
            tv_price_with_dicount = (TextView) itemView.findViewById(R.id.tv_price_with_dicount);
            tv_price_percentage = (TextView) itemView.findViewById(R.id.tv_price_percentage);
            linear_layout = (LinearLayout) itemView.findViewById(R.id.linear_layout);
            add_wishlist=(ImageView) itemView.findViewById(R.id.add_wishlist);

        }
    }
}
