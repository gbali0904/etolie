package com.etoile.makeupbarr.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.etoile.makeupbarr.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by admin on 2/6/2017.
 */
public class TabItem1 extends Fragment {

    private final String data;
    @Bind(R.id.textView)
    TextView textView;
    public TabItem1(String dataFormServer) {
        this.data = dataFormServer;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_fragment_1, container, false);
        ButterKnife.bind(this, view);
        textView.setText(data);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
