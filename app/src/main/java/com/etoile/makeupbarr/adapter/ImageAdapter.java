package com.etoile.makeupbarr.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.etoile.makeupbarr.R;

/**
 * Created by admin on 12/8/2016.
 */

public class ImageAdapter  extends BaseAdapter {

    private Context mContext;
    private static LayoutInflater inflater=null;

    public ImageAdapter(Context a) {
        mContext = a;
        inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public int getCount() {
        return data.length;
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder{
        public TextView name,price;
        public ImageView image;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        ViewHolder holder;
        if(convertView==null){
            vi = inflater.inflate(R.layout.image_gallery_items, null);
            holder=new ViewHolder();
            holder.image=(ImageView)vi.findViewById(R.id.image);
            vi.setTag(holder);
        }
        else
            holder=(ViewHolder)vi.getTag();
        holder.name.setText(name[position]);
        holder.price.setText(price[position]);
        final int stub_id=data[position];
        holder.image.setImageResource(stub_id);
        return vi;
    }

    private int[] data = {
            R.drawable.img1, R.drawable.img2,
            R.drawable.img3, R.drawable.img4
    };
    private String[] name = {
            "COVER FX CUSTOM ENHA.", "COVER FX CUSTOM ENHA.",
            "COVER FX CUSTOM ENHA.", "COVER FX CUSTOM ENHA."
    };
    private String[] price = {
            "INR 2,500", "INR 2,500",
            "INR 2,500", "INR 2,500"
    };
}
