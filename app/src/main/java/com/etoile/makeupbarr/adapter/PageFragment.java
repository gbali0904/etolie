package com.etoile.makeupbarr.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.TAGS;

/**
 * Created by AndroidPC on 07-02-2017.
 */
// In this case, the fragment displays simple text based on the page
public class PageFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";

    private int mPage;
    private String stringvalue;
    private String idvalue;

    public static PageFragment newInstance(int page, String tab_values) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        args.putString(TAGS.TAB_VALUES, tab_values);
        PageFragment fragment = new PageFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(ARG_PAGE);
        stringvalue = getArguments().getString(TAGS.TAB_VALUES);
    }

    // Inflate the fragment layout we defined above for this fragment
    // Set the associated text for the title
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_fragment_1, container, false);
        TextView tvTitle = (TextView) view.findViewById(R.id.textView);
        tvTitle.setText(stringvalue);
        return view;
    }
}