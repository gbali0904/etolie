package com.etoile.makeupbarr.adapter;

import android.content.Intent;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.Utility.TAGS;
import com.etoile.makeupbarr.activity.ProductList;
import com.etoile.makeupbarr.activity.SearchViewActivity;
import com.etoile.makeupbarr.model.DataForDrawerCategories;
import com.etoile.makeupbarr.model.DataForSubCategory;

import java.util.List;

import static com.etoile.makeupbarr.R.id.buttonfdormore;

/**
 * Created by admin on 1/11/2017.
 */
public class RecyclerViewAdapterForSearchView extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final List<DataForSubCategory.SubCategoryListBean> userlist;
    public static final int TYPE_HEADER = 0;
    public static final int TYPE_ITEM = 1;
    public static final int TYPE_FOOTER = 2;
    private final SearchViewActivity baseActivity;
    private AdapterforCategoryInAMinActivity recyclerViewAdapter;
    private int num=1;

    public RecyclerViewAdapterForSearchView(SearchViewActivity baseActivity, List<DataForSubCategory.SubCategoryListBean> items) {
        userlist = items;
        this.baseActivity = baseActivity;
    }




    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View v1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_item_search, parent, false);
            return new CategoryViewHolder(v1);
        } else if (viewType == TYPE_FOOTER) {
            View v2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.footer_item_serach, parent, false);
            return new FooterViewHolder(v2);
        } else if (viewType == TYPE_ITEM) {
            View v3 = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_search, parent, false);
            return new SubCategoryViewHolder(v3);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        subcategory(holder, position);
        category(holder, position);
        button(holder,position);

    }

    private void button(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof FooterViewHolder) {

            final FooterViewHolder fotterViewHolder = (FooterViewHolder) holder;
         /*   fotterViewHolder.button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if ((num) * 6 < userlist.size()) {
                        num = num + 1;
                        notifyDataSetChanged();
                    }
                    if ((num) * 6 == userlist.size()) {
                        num = num + 2;
                     notifyDataSetChanged();
                    }
                }
            });*/
        }
    }

    private void category(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof CategoryViewHolder) {
            final CategoryViewHolder categoryViewHolder = (CategoryViewHolder) holder;
            String string = AppController.getInstance().getPreference().getString(TAGS.JSON_CATEGORY, "");
            DataForDrawerCategories dataForDrawerCategories = AppController.getInstance().getGson().fromJson(string, DataForDrawerCategories.class);
            final List<DataForDrawerCategories.ResponseBean> response = dataForDrawerCategories.getResponse();
            recyclerViewAdapter = new AdapterforCategoryInAMinActivity(baseActivity, response);
            categoryViewHolder.recyclerView.setAdapter(recyclerViewAdapter);
        }
    }

    private void subcategory(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof SubCategoryViewHolder) {
            final SubCategoryViewHolder genericViewHolder = (SubCategoryViewHolder) holder;
            final DataForSubCategory.SubCategoryListBean subCategoryListBean = userlist.get(position-1);
            genericViewHolder.TvList.setText(subCategoryListBean.getSub_category_name());
            genericViewHolder.TvList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String sub_category_id = subCategoryListBean.getSub_category_id();
                    String sub_category_name = subCategoryListBean.getSub_category_name();
                    AppController.getInstance().getPreferenceEditor().putString(TAGS.CATEGORY_NAME, sub_category_name).commit();
                    AppController.getInstance().getPreferenceEditor().putString(TAGS.SUB_CATEGORY_ID, sub_category_id).commit();
                    AppController.getInstance().getPreferenceEditor().putString(TAGS.CATEGORY_ID, "").commit();
                    Intent intent = new Intent(baseActivity, ProductList.class);
                    baseActivity.startActivity(intent);
                    baseActivity.finish();
                }
            });

         }
    }

    @Override
    public int getItemCount() {
        return (null != userlist ? userlist.size() + 2 : 0);
       /* if(num*6 > userlist.size()){
            return (null != userlist ? userlist.size() + 2 : 0);
        }
        else{
            return num*6;
        }*/
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        } else if (isPositionFooter(position)) {
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    private boolean isPositionFooter(int position) {
            return position == userlist.size()+1;
      }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        private final RecyclerView recyclerView;

        public CategoryViewHolder(View v1) {
            super(v1);
            recyclerView = (RecyclerView) v1.findViewById(R.id.category_list);
            GridLayoutManager layoutManager = new GridLayoutManager(baseActivity, 3);
            layoutManager.getOrientation();
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(layoutManager);
        }
    }

    public class FooterViewHolder extends RecyclerView.ViewHolder {

        AppCompatButton button;
        public FooterViewHolder(View v2) {
            super(v2);
            button=(AppCompatButton) v2.findViewById(buttonfdormore);
            button.setVisibility(View.GONE);

        }
    }

    public class SubCategoryViewHolder extends RecyclerView.ViewHolder {
        TextView TvList;

        public SubCategoryViewHolder(View v3) {
            super(v3);
            TvList = (TextView) v3.findViewById(R.id.Tv_list);
        }
    }


}
