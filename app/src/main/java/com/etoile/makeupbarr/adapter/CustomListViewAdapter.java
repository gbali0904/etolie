package com.etoile.makeupbarr.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.Utility.TAGS;
import com.etoile.makeupbarr.model.DataForProductDetail;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by admin on 12/10/2016.
 */

public class CustomListViewAdapter  extends RecyclerView.Adapter {

    private final List<DataForProductDetail.ResponseBean.ProductColorShapeBean> userList;
    private final CircleImageView roundedCornerFigrprint;
    private Context context;
    public static String color_shape_id;
    private AnimationDrawable rocketAnimation;
    private int mCheckedPostion=-1;
    public int selectedPosition  = -1;

    private SparseBooleanArray mSelectedItems;

    public CustomListViewAdapter(Context context, List<DataForProductDetail.ResponseBean.ProductColorShapeBean> items, CircleImageView roundedCornerFigrprint) {
        this.context = context;
        userList=items;
        this.roundedCornerFigrprint=roundedCornerFigrprint;
      }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        holder = new ItemViewHolder(itemView);
        Context context = parent.getContext();
        return holder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final ItemViewHolder itemHolder = (ItemViewHolder) holder;
        DataForProductDetail.ResponseBean.ProductColorShapeBean productColorShapeBean = userList.get(position);

     //   itemHolder.txtTitle.setText(productColorShapeBean.getColor_shape_name());
        final String color_shape_code = productColorShapeBean.getColor_shape_code();
        int i = Color.parseColor(color_shape_code);
      itemHolder.imageView.setColorFilter(i);

    itemHolder.album_item.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                DataForProductDetail.ResponseBean.ProductColorShapeBean item = userList.get(position);
                color_shape_id = item.getColor_shape_id();
                String color_shape_name = item.getColor_shape_name();

                if(selectedPosition == position){
                    // do whatever you want to do to make it selected.
                    selectedPosition = -1;
                }
                else {
                    selectedPosition = position;
                    int i = Color.parseColor(color_shape_code);
                    roundedCornerFigrprint.setColorFilter(i);
                    AppController.getInstance().getPreferenceEditor().putString(TAGS.COLOR_SHADES_ID, color_shape_id).commit();
                    Log.e("thisdata", color_shape_id + color_shape_name);
                   // itemHolder.icon1.setImageDrawable(R.drawable.ic_checked);
                    itemHolder.icon1.setVisibility(View.VISIBLE);
                    notifyDataSetChanged();
                }


/*

                if(mSelectedItems.get(position)) {

                    // When selecting an item, save it
                    mSelectedItems.put(position, true); // Set true when selected
                    // set as selected here
                    itemHolder.imageviewfortick.setColorFilter(R.drawable.ic_checked);
                    AppController.getInstance().getPreferenceEditor().putString(TAGS.COLOR_SHADES_ID, color_shape_id).commit();
                    Log.e("thisdata", color_shape_id + color_shape_name);

                } else {

                    itemHolder.imageviewfortick.setColorFilter(R.drawable.blank);
                    // set as not selected here.
                    // SparseBoolean always return false if we haven't add the value yet.
                }

*/


             /*   if (selectedPosition == mCheckedPostion) {
                  //  itemHolder.checkBox.setChecked(false);
                    mCheckedPostion = -1;
                  //  itemHolder.checkBox.setVisibility(View.GONE);
                } else {
                   // itemHolder.checkBox.setVisibility(View.VISIBLE);
                    notifyDataSetChanged();

                    mCheckedPostion = position;
                    int i = Color.parseColor(color_shape_code);
                    roundedCornerFigrprint.setColorFilter(i);
                    AppController.getInstance().getPreferenceEditor().putString(TAGS.COLOR_SHADES_ID, color_shape_id).commit();
                    Log.e("thisdata", color_shape_id + color_shape_name);
                }*/
                /*if (position == mCheckedPostion) {
                    itemHolder.checkBox.setChecked(false);
                    mCheckedPostion = -1;
                } else {
                    mCheckedPostion = position;
                    AppController.getInstance().getPreferenceEditor().putString(TAGS.COLOR_SHADES_ID, color_shape_id).commit();
                    Log.e("thisdata", color_shape_id + color_shape_name);
                    notifyDataSetChanged();
                }*/
            }
        });
    }

    @Override
    public int getItemCount() {

        return userList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
      //  TextView txtTitle;
    ImageView icon1;
     FrameLayout album_item;
        public ItemViewHolder(View itemView) {
            super(itemView);
          //  txtTitle = (TextView) itemView.findViewById(R.id.tv_product_name);
            imageView = (ImageView) itemView.findViewById(R.id.icon);
            icon1=(ImageView) itemView.findViewById(R.id.icon1);
            album_item=(FrameLayout) itemView.findViewById(R.id.album_item);
            album_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedPosition = getLayoutPosition();
                    //notifyItemRangeChanged(0, userList.size()-1);
                    notifyItemChanged(selectedPosition);
                }
            });
        }
    }

}
