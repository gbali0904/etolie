package com.etoile.makeupbarr.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.etoile.makeupbarr.R;

public class ArrayAdapterWithIcon extends ArrayAdapter<String> {

    private final Context context1;
    private  String[] items1;
    private Integer[] images;


    public ArrayAdapterWithIcon(Context context, String[] items, Integer[] images) {
        super(context, R.layout.alert_customlist, items);
         context1 = context;
        this.images = images;
         this.items1 = items;
    }

/*public CustomList(Activity context,String[] web, Integer[] imageId)
{
super(context, R.layout.list_single, web);
this.context = context;
this.web = web;
this.imageId = imageId;

}*/

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.alert_customlist, parent, false);

        TextView textView = (TextView) view.findViewById(R.id.text);
        ImageView imageView=(ImageView) view.findViewById(R.id.image);


        textView.setText(items1[position]);
        imageView.setImageResource(images[position]);


        return view;
    }

}
