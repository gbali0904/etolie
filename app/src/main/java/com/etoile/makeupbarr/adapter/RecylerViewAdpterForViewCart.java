package com.etoile.makeupbarr.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.Utility.TAGS;
import com.etoile.makeupbarr.login.AddressActivity;
import com.etoile.makeupbarr.model.DataForDeleteCart;
import com.etoile.makeupbarr.model.DataForLogin;
import com.etoile.makeupbarr.model.DataForProductList;
import com.etoile.makeupbarr.model.DataForUpdateCart;
import com.etoile.makeupbarr.model.DataForViewAddCart;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by admin on 1/2/2017.
 */


public class RecylerViewAdpterForViewCart extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static OnUpdateListener mListener;
    private final Context context;
    private final Activity activity;
    private final CoordinatorLayout coLay1;
    private final AppCompatButton checkout;
    List<DataForViewAddCart.ResponseBean> userList;
    public static final int TYPE_HEADER = 0;
    public static final int TYPE_ITEM = 1;
    public static final int TYPE_FOOTER = 2;

    int total = 0;

    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    private DataForProductList.ResponseBean user;
    public static int count = 1;
    private String uid;
    private String product_id;

    public RecylerViewAdpterForViewCart(Context ctx, List<DataForViewAddCart.ResponseBean> response, CoordinatorLayout coLay, AppCompatButton checkout) {
        this.context = ctx;
        this.activity = (Activity) ctx;
        userList = response;
        coLay1 = coLay;
        this.checkout = checkout;

    }

    @Override
    public int getItemCount() {

        return (null != userList ? userList.size() + 2 : 0);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View v1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_item_cart, parent, false);
            return new HeaderViewHolder(v1);
        } else if (viewType == TYPE_FOOTER) {
            View v2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.footer_cart_item, parent, false);
            return new FooterViewHolder(v2);
        } else if (viewType == TYPE_ITEM) {
            View v3 = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_cart_product, parent, false);
            return new GenericViewHolder(v3);
        }
        return null;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        cartItemMethod(holder, position);
        if (holder instanceof FooterViewHolder) {
    /*     final FooterViewHolder footerViewHolder = (FooterViewHolder) holder;
         int total=0;
         for (DataForViewAddCart.ResponseBean item : userList) {
             int total_price = Integer.parseInt(item.getTotal_price());
             total+=total_price;
         }
         Log.d("Total is:",""+total);
         String decimalprice = AppController.getInstance().decimalValue(total);
         footerViewHolder.textView.setText(" INR " +decimalprice);
         final int finalTotal = total;*/


        }
    }

    private void cartItemMethod(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof GenericViewHolder) {
            final GenericViewHolder genericViewHolder = (GenericViewHolder) holder;
            final DataForViewAddCart.ResponseBean responseBean = userList.get(position - 1);
            genericViewHolder.productname.setText(responseBean.getProduct_name());
            final String price = responseBean.getPrice();
            final String decimalprice = AppController.getInstance().decimalValue(Integer.parseInt(price));


            String string = AppController.getInstance().getPreference().getString(TAGS.JSON_USER_DATA, "");
            DataForLogin dataForLogin = AppController.getInstance().getGson().fromJson(string, DataForLogin.class);
            DataForLogin.ResponseBean response = dataForLogin.getResponse();
             uid = response.getUid();
             product_id = responseBean.getProduct_id();

            //"<font size='50' color='#337744'>Text Message</font>"
            Spanned spanned1 = Html.fromHtml("<font size='10'></font>" + decimalprice);

            genericViewHolder.productprice.setText(spanned1);
            genericViewHolder.cart_product_quantity.setText(responseBean.getQty());
            String color_code = responseBean.getColor_code();
            int i = Color.parseColor(color_code);
            genericViewHolder.cart_product_shade.setColorFilter(i);
            String totalPrice = responseBean.getTotal_price();
            final String decimalpricetotal1 = AppController.getInstance().decimalValue(Integer.parseInt(totalPrice));
            genericViewHolder.subtotal.setText("INR " +decimalpricetotal1);
            genericViewHolder.networkImageView.setImageUrl(responseBean.getUrl(), imageLoader);

            genericViewHolder.img_minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (count != 1) {
                        count--;
                    }
                    genericViewHolder.cart_product_quantity.setText("" + count);
                    getUpdatedQuantity(count,responseBean);
                }
            });
            genericViewHolder.img_plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    count++;
                    genericViewHolder.cart_product_quantity.setText("" + count);
                    getUpdatedQuantity(count,responseBean);
                }
            });


            for (DataForViewAddCart.ResponseBean item : userList) {
                int total_price = Integer.parseInt(item.getTotal_price());
                total =total+ total_price;

            }



            genericViewHolder.cart_product_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int totalRecord = Integer.parseInt(AppController.getInstance().getPreference().getString(TAGS.TOATAL_RECORD, ""));
                    String updatedRecord = String.valueOf(totalRecord - 1);
                    //increment total record value in preference
                    AppController.getInstance().getPreferenceEditor().putString(TAGS.TOATAL_RECORD, updatedRecord).commit();
                    //update cart bus with integer value
                    AppController.getInstance().bus().send(updatedRecord);
                    product_id = responseBean.getProduct_id();
                    AppController.getInstance().getRetrofitServices().deletecart(uid, product_id).enqueue(new Callback<DataForDeleteCart>() {
                        @Override
                        public void onResponse(Call<DataForDeleteCart> call, Response<DataForDeleteCart> response) {
                            DataForDeleteCart jsonfordeletecart = response.body();
                            if (jsonfordeletecart.getStatus().equalsIgnoreCase("success")) {
                                Snackbar snackbar = Snackbar.make(coLay1, "Successfully Removed", Snackbar.LENGTH_LONG);
                                View snackbarView = snackbar.getView();
                                snackbarView.setBackgroundColor(Color.GRAY);
                                snackbar.show();
                                if (mListener != null) {
                                    mListener.onUpdate();
                                }
                            } else {
                                Snackbar snackbar = Snackbar.make(coLay1, jsonfordeletecart.getMessage(), Snackbar.LENGTH_LONG);
                                View snackbarView = snackbar.getView();
                                snackbarView.setBackgroundColor(Color.GRAY);
                                snackbar.show();
                            }
                        }

                        @Override
                        public void onFailure(Call<DataForDeleteCart> call, Throwable t) {
                            Toast.makeText(context, "Server error", Toast.LENGTH_LONG).show();

                        }
                    });
                }

            });

            checkout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AppController.getInstance().getPreferenceEditor().putString(TAGS.TOTAL_PRICE, String.valueOf(total)).commit();
                    Intent intent1 = new Intent(context, AddressActivity.class);
                    intent1.putExtra(TAGS.FLAG, "true");
                    context.startActivity(intent1);
                }
            });
        }
    }

    private void getUpdatedQuantity(final int count, DataForViewAddCart.ResponseBean responseBean) {
        String count_value = "" +count;
        product_id = responseBean.getProduct_id();
        AppController.getInstance().getRetrofitServices().update_to_cart(uid, product_id, count_value).enqueue(new Callback<DataForUpdateCart>() {
            @Override
            public void onResponse(Call<DataForUpdateCart> call, Response<DataForUpdateCart> response) {
                DataForUpdateCart jsonforcart = response.body();
                if (jsonforcart.getStatus().equalsIgnoreCase("success")) {
                    if (mListener != null) {
                        mListener.onUpdate();
                    }
                } else {
                    Toast.makeText(context, jsonforcart.getMessage(), Toast.LENGTH_LONG).show();

                }


            }

            @Override
            public void onFailure(Call<DataForUpdateCart> call, Throwable t) {
                Toast.makeText(context, "Server error can't download the categories", Toast.LENGTH_LONG).show();
            }
        });

    }



    public static void setOnUpdateListener(OnUpdateListener listener) {

        mListener = listener;
    }

    public void setFilter(List<DataForViewAddCart.ResponseBean> filteredModelList) {
        userList = new ArrayList<>();
        userList.addAll(filteredModelList);
        notifyDataSetChanged();
    }

    public interface OnUpdateListener {
        void onUpdate();
    }

    private class HeaderViewHolder extends RecyclerView.ViewHolder {
        public HeaderViewHolder(View v1) {
            super(v1);
        }
    }

    private class FooterViewHolder extends RecyclerView.ViewHolder {


        public FooterViewHolder(View v2) {
            super(v2);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        } else if (isPositionFooter(position)) {
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    private boolean isPositionFooter(int position) {
        return position == userList.size() + 1;
    }

    private class GenericViewHolder extends RecyclerView.ViewHolder {
        private final NetworkImageView networkImageView;
        LinearLayout cart_product_delete;
        TextView productname, productprice, cart_product_quantity, subtotal, product_discount, cart_product_update, productprice_actuall, productprice_saving;
        ImageView cart_product_shade;
        ImageView img_minus,img_plus;

        public GenericViewHolder(View v3) {
            super(v3);
            networkImageView = (NetworkImageView) itemView.findViewById(R.id.cart_product_image);
            productname = (TextView) itemView.findViewById(R.id.cart_product_name);
            productprice = (TextView) itemView.findViewById(R.id.cart_product_price);
            //   productprice_actuall=(TextView) itemView.findViewById(R.id.product_value);
            //  productprice_saving=(TextView) itemView.findViewById(R.id.product_savingsvalue);
            //    product_discount=(TextView) itemView.findViewById(R.id.product_discount);
            cart_product_quantity = (TextView) itemView.findViewById(R.id.cart_product_quantity);
            cart_product_shade = (ImageView) itemView.findViewById(R.id.cart_product_shade_image);
            cart_product_delete = (LinearLayout) itemView.findViewById(R.id.cart_product_delete);
            subtotal = (TextView) itemView.findViewById(R.id.subtotal);

            img_minus=(ImageView) itemView.findViewById(R.id.img_minus);
            img_plus=(ImageView) itemView.findViewById(R.id.img_plus);
        }
    }
}
