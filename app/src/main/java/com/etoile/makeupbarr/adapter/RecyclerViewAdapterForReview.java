package com.etoile.makeupbarr.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.activity.ReviewViewActivity;
import com.etoile.makeupbarr.model.DataForProductDetail;

import java.util.List;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

/**
 * Created by AndroidPC on 07-02-2017.
 */
public class RecyclerViewAdapterForReview extends RecyclerView.Adapter {
    private final List<DataForProductDetail.ResponseBean.ReviewBean> userList;
    private final ReviewViewActivity context;


    public static final int TYPE_HEADER = 0;
    public static final int TYPE_ITEM = 1;
    public static final int TYPE_FOOTER = 2;
    private DataForProductDetail.ResponseBean.ReviewBean user;

    public RecyclerViewAdapterForReview(ReviewViewActivity reviewViewActivity, List<DataForProductDetail.ResponseBean.ReviewBean> review) {
        context=reviewViewActivity;
        userList=review;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_review_list, parent, false);
        holder = new ItemViewHolder(itemView);
        Context context = parent.getContext();
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ItemViewHolder itemHolder = (ItemViewHolder) holder;
        user = userList.get(position);
        String active = user.getCreated();// "created": "2017-02-07 01:04:43",


        String[] split = active.split(" ");
        String s = split[0];

        itemHolder.title.setText(user.getTitle());
        itemHolder.date.setText(s);
        itemHolder.name.setText("Posted by "+user.getFname()+" "+user.getLname());
        itemHolder.ratinReview.setRating(Float.parseFloat(user.getRating()));
        itemHolder.comment.setText(user.getComment());


    }


    @Override
    public int getItemCount() {
        return userList.size();
    }


    public class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        TextView date;
        TextView name;
        MaterialRatingBar ratinReview;
        TextView comment;
        public ItemViewHolder(View itemView) {
            super(itemView);
            title=(TextView) itemView.findViewById(R.id.title);
            date=(TextView) itemView.findViewById(R.id.date);
            name=(TextView) itemView.findViewById(R.id.name);
            ratinReview=(MaterialRatingBar) itemView.findViewById(R.id.ratin_review);
            comment=(TextView) itemView.findViewById(R.id.comment);
        }
    }
}
