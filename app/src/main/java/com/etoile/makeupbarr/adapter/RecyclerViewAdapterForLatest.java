package com.etoile.makeupbarr.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.model.DataForProductList;

import java.util.List;

/**
 * Created by AndroidPC on 11-01-2017.
 */
public class RecyclerViewAdapterForLatest extends RecyclerView.Adapter {
    private final List<DataForProductList.ResponseBean> userList;
    private final Context context;
    private DataForProductList.ResponseBean responseBean;

    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    public RecyclerViewAdapterForLatest(Context mContext, List<DataForProductList.ResponseBean> responseDataForProductList) {
        this.context = mContext;
        userList = responseDataForProductList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pacakge_card_adp, parent, false);
        holder = new ItemViewHolder(itemView);
        Context context = parent.getContext();
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ItemViewHolder genericViewHolder = (ItemViewHolder) holder;
                       // final Integer feedItem = (Integer) ItemList.get(position - 1);
                        responseBean = userList.get(position);
                        String image_url = responseBean.getImage_url();
                        genericViewHolder.imgThumbnail.setImageUrl(image_url, imageLoader);
                        genericViewHolder.tv_product_name.setText(responseBean.getPrice());
                        genericViewHolder.tv_product_name.setText(responseBean.getProduct_name());
    }

    @Override
    public int getItemCount() {

        return userList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        CardView card_catogery;
        public   NetworkImageView imgThumbnail;
        TextView tv_product_name,tv_price;
        public ItemViewHolder(View itemView) {
            super(itemView);

            card_catogery = (CardView) itemView.findViewById(R.id.card_view);
            imgThumbnail = (NetworkImageView) itemView.findViewById(R.id.img_thumbnail);
            tv_product_name=(TextView) itemView.findViewById(R.id.tv_product_name);
            tv_price=(TextView) itemView.findViewById(R.id.tv_price);
        }
    }
}
