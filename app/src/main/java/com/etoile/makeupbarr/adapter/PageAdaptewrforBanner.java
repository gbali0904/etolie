package com.etoile.makeupbarr.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.model.DataForBanner;

import java.util.List;

/**
 * Created by admin on 12/5/2016.
 */

public class PageAdaptewrforBanner extends PagerAdapter {
    private Context mContext;
    private List<DataForBanner.ResponseBean> userlist;

    ImageLoader imageLoader = AppController.getInstance().getImageLoader();


    public PageAdaptewrforBanner(Context mContext, List<DataForBanner.ResponseBean> userlist) {
        this.mContext = mContext;
        this.userlist = userlist;

    }

    @Override
    public int getCount() {
        return userlist.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.banner_images_activity, container, false);
        container.addView(itemView);
        DataForBanner.ResponseBean responseBean = userlist.get(position);
        NetworkImageView imageView = (NetworkImageView) itemView.findViewById(R.id.img_pager_item);
        imageView.setImageUrl(responseBean.getImage(), imageLoader);

        String main_heading = responseBean.getMain_heading();

        TextView mainheading=(TextView) itemView.findViewById(R.id.mainheading);
        mainheading.setText(main_heading);

        TextView subheading=(TextView) itemView.findViewById(R.id.subheading);
        String sub_heading = responseBean.getSub_heading();
        subheading.setText(sub_heading);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
