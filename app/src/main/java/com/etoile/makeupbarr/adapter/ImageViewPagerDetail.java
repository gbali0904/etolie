package com.etoile.makeupbarr.adapter;

import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.activity.ProductDetailActivity;
import com.etoile.makeupbarr.activity.ZoomImageView;
import com.etoile.makeupbarr.model.DataForProductDetail;

import java.util.List;

/**
 * Created by admin on 12/10/2016.
 */

public class ImageViewPagerDetail extends PagerAdapter {
    private ProductDetailActivity mContext;
    private List<DataForProductDetail.ResponseBean.ProductImagesBean> mResources;


    ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public ImageViewPagerDetail(ProductDetailActivity mContext, List<DataForProductDetail.ResponseBean.ProductImagesBean> mResources) {
        this.mContext = mContext;
        this.mResources = mResources;
    }

    @Override
    public int getCount() {
        return mResources.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.product_pager_detail, container, false);
        NetworkImageView imageView = (NetworkImageView) itemView.findViewById(R.id.img_pager_item);
        DataForProductDetail.ResponseBean.ProductImagesBean productImagesBean = mResources.get(position);
        String bigUrl = productImagesBean.getBigUrl();
        imageView.setImageUrl(String.valueOf(bigUrl), imageLoader);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(mContext,ZoomImageView.class);
                mContext.startActivity(intent);
            }
        });
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
