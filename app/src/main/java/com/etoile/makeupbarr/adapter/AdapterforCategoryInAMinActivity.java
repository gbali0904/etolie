package com.etoile.makeupbarr.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.Utility.TAGS;
import com.etoile.makeupbarr.activity.ProductList;
import com.etoile.makeupbarr.model.DataForDrawerCategories;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by AndroidPC on 19-01-2017.
 */
public class AdapterforCategoryInAMinActivity extends RecyclerView.Adapter {
    private final Context mContext;
    private final List<DataForDrawerCategories.ResponseBean> userlist;
    private DataForDrawerCategories.ResponseBean user;


    public AdapterforCategoryInAMinActivity(Context mContext, List<DataForDrawerCategories.ResponseBean> response) {
        this.mContext=mContext;
        this.userlist =response;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_mainactivity_list, parent, false);
        holder = new ItemViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final ItemViewHolder itemHolder = (ItemViewHolder) holder;
        user = userlist.get(position);


        itemHolder.textView.setText(user.getCategory_name());
         String imageUrl = user.getImage();
        Picasso.with(getApplicationContext()).load(imageUrl)
                .placeholder(R.drawable.my_progress_indeterminate).error(R.drawable.ic_launcher)
                .into( itemHolder.imageView);
        itemHolder.layoutclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                user = userlist.get(position);
                String category_id = user.getCategory_id();
                String category_name = user.getCategory_name();
                AppController.getInstance().getPreferenceEditor().putString(TAGS.SUB_CATEGORY_ID, "").commit();
                AppController.getInstance().getPreferenceEditor().putString(TAGS.CATEGORY_ID, category_id).commit();
                AppController.getInstance().getPreferenceEditor().putString(TAGS.CATEGORY_NAME, category_name).commit();
                Intent intent = new Intent(mContext, ProductList.class);
                mContext.startActivity(intent);
               }
        });

    }

    @Override
    public int getItemCount() {
        return userlist.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;
        LinearLayout layoutclick;
        public ItemViewHolder(  View itemView) {
            super(itemView);
            imageView =(ImageView) itemView.findViewById(R.id.im_productcategory);
            textView=(TextView) itemView.findViewById(R.id.tv_productName);
            layoutclick=(LinearLayout) itemView.findViewById(R.id.layoutclick);
        }
    }
}
