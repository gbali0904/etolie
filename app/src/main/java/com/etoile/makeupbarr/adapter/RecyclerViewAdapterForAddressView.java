package com.etoile.makeupbarr.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.Utility.TAGS;
import com.etoile.makeupbarr.activity.AddressAPIActivity;
import com.etoile.makeupbarr.login.AddAddressActivity;
import com.etoile.makeupbarr.login.AddressActivity;
import com.etoile.makeupbarr.model.DataForHashGenration;
import com.etoile.makeupbarr.model.DataForLogin;
import com.etoile.makeupbarr.model.DataForViewAddress;
import com.etoile.makeupbarr.model.UserAddress;
import com.payUMoney.sdk.PayUmoneySdkInitilizer;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by AndroidPC on 07-01-2017.
 */
public class RecyclerViewAdapterForAddressView extends RecyclerView.Adapter {
    private static OnUpdateListener mListener;
    private final AddressActivity addressActivity;
    private final List<DataForViewAddress.AddressBean> userList;
    private final CoordinatorLayout layout;
    private final AppCompatButton payment;
    private final String flagstring;
    private DataForViewAddress.AddressBean user;
    private String uid = "";
    private String pin = "";
    private String myname = "";
    private String address = "";
    private String locality = "";
    private String city = "";
    private String state = "";
    private String phone = "";
    public int mSelectedItem = -1;
    public String s;
    public String mypaymentaddress, mypaymentlocality, mypaymentcity, mypaymentpincode, mypaymentstate, mypaymentmobile;
    private String amt;
    String fname,email,price;

    public RecyclerViewAdapterForAddressView(AddressActivity addressActivity, List<DataForViewAddress.AddressBean> address, CoordinatorLayout layout, AppCompatButton payment, String flagstring) {
        this.addressActivity = addressActivity;
        userList = address;
        this.layout = layout;
        this.payment = payment;
        this.flagstring=flagstring;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_address_list, parent, false);
        holder = new ItemViewHolder(itemView);
        Context context = parent.getContext();
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final ItemViewHolder itemHolder = (ItemViewHolder) holder;

        user = userList.get(position);
        itemHolder.name.setText(user.getName());
        Spanned spanned = Html.fromHtml("<b>Address: </b>" + user.getAddress()
                + "<br>" + "<b>Locality: </b>" + user.getLocality()
                + "<br>" + "\n<b>City/Pincode: </b>" + user.getCity()
                + "\t" + user.getPincode()
                + "<br>" + "\n<b>State: </b>" + user.getState()
                + "<br>" + "\n<b>Mobile: </b>" + user.getMobile());

        itemHolder.address.setText(spanned);

        itemHolder.radioButton.setChecked(position == mSelectedItem);
        itemHolder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeAddress(position);
            }


        });
        itemHolder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateAddress(position);
            }
        });
    }

    private void updateAddress(int position) {
        DataForViewAddress.AddressBean addressBean = userList.get(position);
        UserAddress user = new UserAddress();
        user.setMobile(addressBean.getMobile());
        user.setState(addressBean.getState());
        user.setPincode(addressBean.getPincode());
        user.setCity(addressBean.getCity());
        user.setLocality(addressBean.getLocality());
        user.setAddress(addressBean.getAddress());
        user.setName(addressBean.getName());
        String jsonValueforaddressView = AppController.getInstance().getGson().toJson(user);
        AppController.getInstance().getPreferenceEditor().putString(TAGS.USER_DATA, jsonValueforaddressView).commit();
        addressBean.getName();
        String address_id = addressBean.getAddress_id();
        AppController.getInstance().getPreferenceEditor().putString(TAGS.ADDRESSID_JSON_DATA, address_id).commit();
        Intent intent = new Intent(addressActivity, AddAddressActivity.class);
        addressActivity.startActivity(intent);
      }

    private void removeAddress(int position) {
        DataForViewAddress.AddressBean addressBean = userList.get(position);
        addressBean.getMobile();
        addressBean.getState();
        addressBean.getPincode();
        addressBean.getCity();
        addressBean.getLocality();
        addressBean.getAddress();
        String address_id = addressBean.getAddress_id();
        AddressAPIActivity.addresstoserver(uid, pin, myname, address, locality, city, state, phone, address_id);
        if (mListener != null) {
            mListener.onUpdate();
        }
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private final TextView name, address, remove, edit;
        RadioButton radioButton;

        public ItemViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            address = (TextView) itemView.findViewById(R.id.etAddress);
            remove = (TextView) itemView.findViewById(R.id.remove);
            edit = (TextView) itemView.findViewById(R.id.edit);

            radioButton = (RadioButton) itemView.findViewById(R.id.radiobtn);

            if(flagstring.equalsIgnoreCase("false")){
                radioButton.setVisibility(View.GONE);
            }

            radioButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    notifyItemRangeChanged(0, userList.size());

                    DataForViewAddress.AddressBean item = userList.get(mSelectedItem);
                    //  Toast.makeText(mContext, item, Toast.LENGTH_LONG).show();
                    s = item.getAddress()
                            + "\n" + item.getLocality()
                            + "\n" + item.getCity()
                            + "\t" + item.getPincode()
                            + "\n" + item.getState()
                            + "\n" + item.getMobile();
                    mypaymentaddress = item.getAddress();
                    mypaymentlocality = item.getLocality();
                    mypaymentcity = item.getCity();
                    mypaymentpincode = item.getPincode();
                    mypaymentstate = item.getState();
                    mypaymentmobile = item.getMobile();
                    Log.e("ThisData", "myadd" + s);

                }
            });


            payment.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    Log.e("ThisData", "myadd" + s);
                        paymentCall(mypaymentaddress, mypaymentcity, mypaymentpincode, mypaymentstate, mypaymentmobile);

                }
            });
        }
    }

    private void paymentCall(final String address, String city, String zip_code, String state_id, String phone) {
        String json_string = AppController.getInstance().getPreference().getString(TAGS.JSON_USER_DATA, "");
        DataForLogin dataForLogin = AppController.getInstance().getGson().fromJson(json_string, DataForLogin.class);
        DataForLogin.ResponseBean response = dataForLogin.getResponse();
        fname = response.getFname();
        email = response.getEmail();
        price = AppController.getInstance().getPreference().getString(TAGS.TOTAL_PRICE, "");

        AppController.getInstance().getRetrofitServices().
                payment_detail(fname, phone, email, address, zip_code,
                        city, state_id, price+".0")
                .enqueue(new Callback<DataForHashGenration>() {
                    @Override
                    public void onResponse(Call<DataForHashGenration> call, Response<DataForHashGenration> response) {
                        DataForHashGenration categoryjson = response.body();
                        Log.e("ThisData", categoryjson.getStatus());
                        if (categoryjson.getStatus().equalsIgnoreCase("success")) {



                            makePayment(categoryjson);
                        } else {
                            Toast.makeText(addressActivity,"!"+categoryjson.getStatus()+" Please Select Address !",Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<DataForHashGenration> call, Throwable t) {
                        Toast.makeText(addressActivity, "Server error can't download the categories", Toast.LENGTH_LONG).show();

                    }
                });

    }

    //for update listner
    public static void setOnUpdateListener(OnUpdateListener listener) {
        mListener = listener;
    }

    public interface OnUpdateListener {
        void onUpdate();
    }
    //PayuMoney
    public void makePayment(DataForHashGenration categoryjson) {


    Log.e("this","key"+categoryjson.getKEY()+"\ntax id"+categoryjson.getTxnid()+
            "\namount"+Double.parseDouble(categoryjson.getAmount())+"\nmid"+categoryjson.getMID()+
            "\nhash"+categoryjson.getHash()

    );
    PayUmoneySdkInitilizer.PaymentParam.Builder builder = new PayUmoneySdkInitilizer.PaymentParam.Builder();
    builder
            .setKey(categoryjson.getKEY())
            .setTnxId( categoryjson.getTxnid())
            .setAmount(Double.parseDouble(categoryjson.getAmount()))
            .setProductName(categoryjson.getProductInfo())
            .setFirstName(categoryjson.getFname())
            .setEmail(categoryjson.getEmail())
            .setPhone(categoryjson.getPhone())
            .setsUrl(categoryjson.getSurl())
            .setfUrl(categoryjson.getFurl())
            .setIsDebug(true)
            .setUdf1("")
            .setUdf2("")
            .setUdf3("")
            .setUdf4("")
            .setUdf5("")
            .setMerchantId(categoryjson.getMID());
    PayUmoneySdkInitilizer.PaymentParam paymentParam = builder.build();

        /*fetchAndSetPaymentHash(paymentParam,categoryjson.getTxnid(),categoryjson.getAmount(),
               "product_name",categoryjson.getFname(),categoryjson.getEmail(),categoryjson.getKEY(),
                "4928174",categoryjson.getSALT(),categoryjson.getPhone(),categoryjson.getAddress(),
               categoryjson.getZip_code(),categoryjson.getCity(),categoryjson.getState_id()
               );*/
    //  calculateServerSideHashAndInitiatePayment(paymentParam);
    paymentParam.setMerchantHash(categoryjson.getHash());

    Log.e("this",categoryjson.getHash());

    PayUmoneySdkInitilizer.startPaymentActivityForResult(addressActivity, paymentParam);

}

}