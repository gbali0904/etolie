package com.etoile.makeupbarr.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.Utility.TAGS;
import com.etoile.makeupbarr.activity.AddRemoveWishList;
import com.etoile.makeupbarr.activity.ProductDetailActivity;
import com.etoile.makeupbarr.activity.ProductList;
import com.etoile.makeupbarr.login.LoginActivity;
import com.etoile.makeupbarr.model.DataForAddRecentView;
import com.etoile.makeupbarr.model.DataForLogin;
import com.etoile.makeupbarr.model.DataForProductList;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecyclerViewAdapterForProductList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static OnUpdateListener mListener;
    private final Context context;
    private final Activity activity;
    private final CoordinatorLayout coLay1;
    String checktype, CurrentStatus;
    List<DataForProductList.ResponseBean> userList;
    String cpvid;
    public static final String KEY_CPVID = "id";
    public static final String KEY_NAME = "username";
    public static final String KEY_APPID = "appid";
    public static final String KEY_CUserList = "userno";
    String passcpvid, passname, passappid;
    //private ImageLoader imageLoader;

    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    private DataForProductList.ResponseBean user;
    private DataForAddRecentView dataForAddRecentView;
    private String product_id;

    public RecyclerViewAdapterForProductList(Context ctx, List<DataForProductList.ResponseBean> response, CoordinatorLayout coLay) {
        this.context = ctx;
        this.activity = (Activity) ctx;
        userList = response;
        coLay1 = coLay;

    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_list, parent, false);
        holder = new ItemViewHolder(itemView);
        Context context = parent.getContext();
        return holder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final ItemViewHolder itemHolder = (ItemViewHolder) holder;
        user = userList.get(position);
        int itemCount = userList.size();
      /*  Spanned itemCounttext = Html.fromHtml("<b><font color='#000000'>Total Item Found :</font></b>\t"+itemCount);*/
        ((ProductList) context).texttoatal.setText("Total item found " + itemCount);
        itemHolder.title.setText(user.getProduct_name());

        String price = user.getPrice();
        int listPrice = Integer.parseInt(price);

        String discount = user.getDiscount();
         product_id = user.getProduct_id();
        int discount_int = Integer.parseInt(discount);
        int discountPrice = listPrice - ((listPrice * discount_int) / 100);
        String my_discount = "" + discountPrice;

        if (discount.equals("0")) {
            itemHolder.discount.setVisibility(View.GONE);
            itemHolder.percentage.setVisibility(View.GONE);
            String decimalprice = AppController.getInstance().decimalValue(Integer.parseInt(price));
            itemHolder.price.setText("INR " + decimalprice);
        } else {
            String decimalprice = AppController.getInstance().decimalValue(Integer.parseInt(my_discount));
            itemHolder.price.setText("INR " + decimalprice);
            itemHolder.discount.setHint(price);
            itemHolder.discount.setPaintFlags(itemHolder.discount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            itemHolder.percentage.setText("(" + discount + "%)");
        }


        if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
            String wishlist = user.getWishlist();
            if (wishlist.equals("0")) {
                int black = Color.parseColor("black");
                itemHolder.add_wishlist.setColorFilter(black);
            } else {
                int red = Color.parseColor("red");
                itemHolder.add_wishlist.setColorFilter(red);
            }

        }
        wishListAddRemoveMethod(itemHolder, position);


        itemHolder.thumbnail.setImageUrl(user.getImage_url(), imageLoader);

        Log.e("mydata", user.getProduct_id());

        itemHolder.relative_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addRecentView(itemHolder,position);
                DataForProductList.ResponseBean responseBean = userList.get(position);
                AppController.getInstance().getPreferenceEditor().putString(TAGS.PRODUCT_ID, responseBean.getProduct_id()).commit();
                Intent intent = new Intent(context, ProductDetailActivity.class);
                intent.putExtra(TAGS.FLAG, "false");
                context.startActivity(intent);
            }
        });
        itemHolder.thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addRecentView(itemHolder,position);
                DataForProductList.ResponseBean responseBean = userList.get(position);
                AppController.getInstance().getPreferenceEditor().putString(TAGS.PRODUCT_ID, responseBean.getProduct_id()).commit();
                Intent intent = new Intent(context, ProductDetailActivity.class);
                intent.putExtra(TAGS.FLAG, "false");
                context.startActivity(intent);

            }
        });
    }



    private void addRecentView(final ItemViewHolder itemViewHolder, final int position) {
        if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {

            String userdata = AppController.getInstance().getPreference().getString(TAGS.JSON_USER_DATA, "");
            DataForLogin dataForLogin = AppController.getInstance().getGson().fromJson(userdata, DataForLogin.class);
            DataForLogin.ResponseBean response = dataForLogin.getResponse();
            final String uid = response.getUid();
            AppController.getInstance().getPreferenceEditor().putString(TAGS.RECENT_ADD, "").commit();

            AppController.getInstance().getRetrofitServices().recentView(uid, product_id).enqueue(new Callback<DataForAddRecentView>() {
                @Override
                public void onResponse(Call<DataForAddRecentView> call, Response<DataForAddRecentView> response) {
                    dataForAddRecentView = response.body();
                    if (dataForAddRecentView.getStatus().equals("success")) {
                        AppController.getInstance().getPreferenceEditor().putString(TAGS.RECENT_ADD, dataForAddRecentView.getMessage()).commit();
                        Snackbar snackbar = Snackbar.make(coLay1, dataForAddRecentView.getMessage(), Snackbar.LENGTH_LONG);
                        snackbar.show();
                    } else {
                        Snackbar snackbar = Snackbar.make(coLay1, dataForAddRecentView.getMessage(), Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                }

                @Override
                public void onFailure(Call<DataForAddRecentView> call, Throwable t) {
                    Toast.makeText(context, "Server error can't download the categories", Toast.LENGTH_LONG).show();
                }
            });
        } else {
            AppController.getInstance().getPreferenceEditor().putString(TAGS.RECENT_ADD, "").commit();
            AppController.getInstance().getRetrofitServices().recentView("", product_id).enqueue(new Callback<DataForAddRecentView>() {
                @Override
                public void onResponse(Call<DataForAddRecentView> call, Response<DataForAddRecentView> response) {
                    dataForAddRecentView = response.body();
                    if (dataForAddRecentView.getStatus().equals("success")) {
                        AppController.getInstance().getPreferenceEditor().putString(TAGS.RECENT_ADD, dataForAddRecentView.getMessage()).commit();
                        /*Snackbar snackbar = Snackbar.make(coLay1, dataForAddRecentView.getMessage(), Snackbar.LENGTH_LONG);
                        snackbar.show();*/
                    } else {
                        /*Snackbar snackbar = Snackbar.make(coLay1, dataForAddRecentView.getMessage(), Snackbar.LENGTH_LONG);
                        snackbar.show();*/
                    }
                }

                @Override
                public void onFailure(Call<DataForAddRecentView> call, Throwable t) {
                    Toast.makeText(context, "Server error can't download the categories", Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    private void wishListAddRemoveMethod(final ItemViewHolder itemHolder, final int position) {

        itemHolder.add_wishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
                    DataForProductList.ResponseBean responseBean = userList.get(position);
                    String product_id = responseBean.getProduct_id();
                    String jsonValueforProductList = AppController.getInstance().getGson().toJson(userList);
                    AppController.getInstance().getPreferenceEditor().putString(TAGS.PRODUCT_LIST, product_id).commit();
                    Log.e("myid", product_id);

                    AddRemoveWishList addRemoveWishList = new AddRemoveWishList();
                    addRemoveWishList.getResponseData(context, coLay1);
                    String string = AppController.getInstance().getPreference().getString(TAGS.WISHLIST, "");

                    if (string.equals("Product wishlist added successfully")) {
                        int black = Color.parseColor("black");
                        itemHolder.add_wishlist.setColorFilter(black);

                    } else {
                        int red = Color.parseColor("red");
                        itemHolder.add_wishlist.setColorFilter(red);

                    }
                    if (mListener != null) {
                        mListener.onUpdate();
                    }
                } else {

                    Intent i = new Intent(context, LoginActivity.class);
                    context.startActivity(i);

                }

            }
        });


    }

    public void setFilter(List<DataForProductList.ResponseBean> filteredModelList) {
        userList = new ArrayList<>();
        userList.addAll(filteredModelList);
        notifyDataSetChanged();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        private final TextView discount;
        private final TextView percentage;
        public TextView title, price;
        public NetworkImageView thumbnail;
        ImageView add_wishlist;
        RelativeLayout relative_layout;

        public ItemViewHolder(View itemView) {

            super(itemView);
            relative_layout = (RelativeLayout) itemView.findViewById(R.id.card_view);
            title = (TextView) itemView.findViewById(R.id.tv_product_name);
            price = (TextView) itemView.findViewById(R.id.tv_price);

            discount = (TextView) itemView.findViewById(R.id.tv_price_with_dicount);
            percentage = (TextView) itemView.findViewById(R.id.tv_price_percentage);


            thumbnail = (NetworkImageView) itemView.findViewById(R.id.thumbnail);
            add_wishlist = (ImageView) itemView.findViewById(R.id.add_wishlist);

        }
    }

    public static void setOnUpdateListener(OnUpdateListener listener) {
        mListener = listener;
    }

    public interface OnUpdateListener {
        void onUpdate();
    }
}
