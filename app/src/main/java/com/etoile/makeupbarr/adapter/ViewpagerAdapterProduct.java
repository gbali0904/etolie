package com.etoile.makeupbarr.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.Utility.TAGS;
import com.etoile.makeupbarr.activity.ProductDetailActivity;
import com.etoile.makeupbarr.model.DataForProductList;

import java.util.List;

/**
 * Created by admin on 12/8/2016.
 */

public class ViewpagerAdapterProduct extends PagerAdapter {
    private final Gallery viewPager;
    private Context mContext;
    int [] mResources;
    LayoutInflater mLayoutInflater;
  /*  private List<DataForProductList.ResponseBean> userList;*/

    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    public ViewpagerAdapterProduct(Context mContext, int[] mResources, Gallery viewPager) {
        this.mContext = mContext;
        this.mResources = mResources;
        this.viewPager= viewPager;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);



    }

    @Override
    public int getCount() {
        return mResources.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.image_gallery_items, container, false);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.image);
        imageView.setImageResource(mResources[position]);

        container.addView(itemView);

        return itemView;

        /*DataForProductList.ResponseBean responseBean = userList.get(position);

        final String product_id = responseBean.getProduct_id();
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppController.getInstance().getPreferenceEditor().putString(TAGS.PRODUCT_ID,product_id).commit();
                Intent intent=new Intent(mContext, ProductDetailActivity.class);
                intent.putExtra(TAGS.FLAG,"true");
                mContext.startActivity(intent);
            }
        });*/

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

}
