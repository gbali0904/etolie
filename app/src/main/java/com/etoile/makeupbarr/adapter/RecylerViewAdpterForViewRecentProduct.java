package com.etoile.makeupbarr.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.Utility.TAGS;
import com.etoile.makeupbarr.model.DataForLogin;
import com.etoile.makeupbarr.model.DataForRecentVIew;
import com.etoile.makeupbarr.model.DataForRemoveRecentView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by admin on 2/20/2017.
 */

public class RecylerViewAdpterForViewRecentProduct extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static OnUpdateListener mListener;
    private final Context context;
    private final Activity activity;
    private final CoordinatorLayout coLay1;
    List<DataForRecentVIew.ResultBean> userList;

    DataForRemoveRecentView removeRecentView;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    private String uid;
    private String product_id;
    private DataForRecentVIew.ResultBean user;

    public RecylerViewAdpterForViewRecentProduct(Context ctx, List<DataForRecentVIew.ResultBean> response, CoordinatorLayout coLay) {
        this.context = ctx;
        this.activity = (Activity) ctx;
        userList = response;
        coLay1 = coLay;
    }

    public static void setOnUpdateListener(OnUpdateListener listener) {
        mListener = listener;
    }

    @Override
    public int getItemCount() {

        return userList.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_recent_view, parent, false);
        holder = new ItemViewHolder(itemView);
        Context context = parent.getContext();
        return holder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final ItemViewHolder itemHolder = (ItemViewHolder) holder;

        user = userList.get(position);
        itemHolder.title.setText(user.getProduct_name());

        String price = user.getPrice();

        int listPrice = Integer.parseInt(price);

        String discount = user.getDiscount();
        int discount_int = Integer.parseInt(discount);
        int discountPrice = listPrice - ((listPrice * discount_int) / 100);
        String my_discount = "" + discountPrice;

        if (discount.equals("0")) {
            itemHolder.discount.setVisibility(View.GONE);
            itemHolder.percentage.setVisibility(View.GONE);
            String decimalprice = AppController.getInstance().decimalValue(Integer.parseInt(price));
            itemHolder.price.setText("INR " + decimalprice);
        } else {
            String decimalprice = AppController.getInstance().decimalValue(Integer.parseInt(my_discount));
            itemHolder.price.setText("INR " + decimalprice);
            itemHolder.discount.setHint(price);
            itemHolder.discount.setPaintFlags(itemHolder.discount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            itemHolder.percentage.setText(discount + "%");
        }


        itemHolder.thumbnail.setImageUrl(user.getImage_url(), imageLoader);

        Log.e("mydata", user.getProduct_id());


        String string = AppController.getInstance().getPreference().getString(TAGS.JSON_USER_DATA, "");
        DataForLogin dataForLogin = AppController.getInstance().getGson().fromJson(string, DataForLogin.class);
        DataForLogin.ResponseBean response = dataForLogin.getResponse();
        uid = response.getUid();
        product_id = user.getProduct_id();

        itemHolder.remove_recentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AppController.getInstance().getRetrofitServices().removeRecentProduct(uid, product_id).enqueue(new Callback<DataForRemoveRecentView>() {
                    @Override
                    public void onResponse(Call<DataForRemoveRecentView> call, Response<DataForRemoveRecentView> response) {

                        DataForRemoveRecentView jsonfordeleterecentView = response.body();
                        if (jsonfordeleterecentView.getStatus().equalsIgnoreCase("success")) {
                            Snackbar snackbar = Snackbar.make(coLay1, jsonfordeleterecentView.getMessage(), Snackbar.LENGTH_LONG);
                            View snackbarView = snackbar.getView();
                            snackbarView.setBackgroundColor(Color.GRAY);
                            snackbar.show();
                            if (mListener != null) {
                                mListener.onUpdate();
                            }
                        } else {
                            Snackbar snackbar = Snackbar.make(coLay1, jsonfordeleterecentView.getMessage(), Snackbar.LENGTH_LONG);
                            View snackbarView = snackbar.getView();
                            snackbarView.setBackgroundColor(Color.GRAY);
                            snackbar.show();
                        }
                    }

                    @Override
                    public void onFailure(Call<DataForRemoveRecentView> call, Throwable t) {
                        Toast.makeText(context, "Server error", Toast.LENGTH_LONG).show();

                    }
                });
            }
        });
        removeRecentProduct(itemHolder, position);
    }

    private void removeRecentProduct(final ItemViewHolder itemHolder, final int position) {
        itemHolder.remove_recentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                product_id = user.getProduct_id();
                AppController.getInstance().getRetrofitServices().removeRecentProduct(uid, product_id).enqueue(new Callback<DataForRemoveRecentView>() {
                    @Override
                    public void onResponse(Call<DataForRemoveRecentView> call, Response<DataForRemoveRecentView> response) {
                        DataForRemoveRecentView jsonfordeleterecentView = response.body();
                        String status = jsonfordeleterecentView.getStatus();
                        if (status.equalsIgnoreCase("success")) {
                            Snackbar snackbar = Snackbar.make(coLay1, jsonfordeleterecentView.getMessage(), Snackbar.LENGTH_LONG);
                            View snackbarView = snackbar.getView();
                            snackbarView.setBackgroundColor(Color.GRAY);
                            snackbar.show();
                            if (mListener != null) {
                                mListener.onUpdate();
                            }
                        } else {
                            Snackbar snackbar = Snackbar.make(coLay1, jsonfordeleterecentView.getMessage(), Snackbar.LENGTH_LONG);
                            View snackbarView = snackbar.getView();
                            snackbarView.setBackgroundColor(Color.GRAY);
                            snackbar.show();
                        }
                    }

                    @Override
                    public void onFailure(Call<DataForRemoveRecentView> call, Throwable t) {
                        Toast.makeText(context, "Server error", Toast.LENGTH_LONG).show();

                    }
                });
            }
        });
    }

    public void setFilter(List<DataForRecentVIew.ResultBean> filteredModelList) {
        userList = new ArrayList<>();
        userList.addAll(filteredModelList);
        notifyDataSetChanged();
    }

    public interface OnUpdateListener {
        void onUpdate();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        private final TextView discount;
        private final TextView percentage;
        public TextView title, price;
        public NetworkImageView thumbnail;
        ImageView remove_recentView;
        RelativeLayout relative_layout;

        public ItemViewHolder(View itemView) {

            super(itemView);
            relative_layout = (RelativeLayout) itemView.findViewById(R.id.card_view);
            title = (TextView) itemView.findViewById(R.id.tv_product_name);
            price = (TextView) itemView.findViewById(R.id.tv_price);

            discount = (TextView) itemView.findViewById(R.id.tv_price_with_dicount);
            percentage = (TextView) itemView.findViewById(R.id.tv_price_percentage);


            thumbnail = (NetworkImageView) itemView.findViewById(R.id.thumbnail);
            remove_recentView = (ImageView) itemView.findViewById(R.id.remove_recentView);

        }
    }
}
