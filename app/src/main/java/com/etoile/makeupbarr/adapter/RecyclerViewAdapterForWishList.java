package com.etoile.makeupbarr.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.Utility.TAGS;
import com.etoile.makeupbarr.activity.AddRemoveWishList;
import com.etoile.makeupbarr.activity.ProductDetailActivity;
import com.etoile.makeupbarr.model.DataForViewWishList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by AndroidPC on 29-12-2016.
 */
public class RecyclerViewAdapterForWishList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static OnUpdateListener mListener;
    private final Context context;
    private final Activity activity;
    private final CoordinatorLayout coLay1;
    List<DataForViewWishList.ResponseBean> userList;

    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    private DataForViewWishList.ResponseBean user;

    public RecyclerViewAdapterForWishList(Context ctx, List<DataForViewWishList.ResponseBean> response, CoordinatorLayout coLay) {
        this.context = ctx;
        this.activity = (Activity) ctx;
        userList = response;
        coLay1 = coLay;
    }

    @Override
    public int getItemCount() {

        return userList.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_wishlist, parent, false);
        holder = new ItemViewHolder(itemView);
        Context context = parent.getContext();
        return holder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final ItemViewHolder itemHolder = (ItemViewHolder) holder;

        user = userList.get(position);
        itemHolder.title.setText(user.getProduct_name());

        String price = user.getPrice();
        int listPrice = Integer.parseInt(price);

        String discount = user.getDiscount();
        int discount_int = Integer.parseInt(discount);
        int discountPrice = listPrice - ((listPrice * discount_int) / 100);
        String my_discount = "" + discountPrice;

        if (discount.equals("0")) {
            itemHolder.discount.setVisibility(View.GONE);
            itemHolder.percentage.setVisibility(View.GONE);
            String decimalprice = AppController.getInstance().decimalValue(Integer.parseInt(price));
            itemHolder.price.setText("INR " + decimalprice);
        } else {
            String decimalprice = AppController.getInstance().decimalValue(Integer.parseInt(my_discount));
            itemHolder.price.setText("INR " + decimalprice);
            itemHolder.discount.setHint(price);
            itemHolder.discount.setPaintFlags(itemHolder.discount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            itemHolder.percentage.setText(discount + "%");
        }
        wishListAddRemoveMethod(itemHolder, position);
        itemHolder.thumbnail.setImageUrl(user.getUrl(), imageLoader);

        itemHolder.addtocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ProductDetailActivity.class);
                intent.putExtra(TAGS.FLAG, "true");
                context.startActivity(intent);
            }
        });


        Log.e("mydata", user.getProduct_id());
    }

    private void wishListAddRemoveMethod(final ItemViewHolder itemHolder, final int position) {
        itemHolder.add_wishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DataForViewWishList.ResponseBean responseBean = userList.get(position);
                String product_id = responseBean.getProduct_id();
                AppController.getInstance().getPreferenceEditor().putString(TAGS.PRODUCT_LIST, product_id).commit();

                AddRemoveWishList addRemoveWishList = new AddRemoveWishList();
                addRemoveWishList.getResponseData(context, coLay1);
                String string = AppController.getInstance().getPreference().getString(TAGS.WISHLIST, "");

                if (string.equals("product wishlist added successfully")) {

                    int black = Color.parseColor("black");
                    itemHolder.add_wishlist.setColorFilter(black);
                    if (mListener != null) {
                        mListener.onUpdate();
                    }
                } else {
                    int red = Color.parseColor("red");
                    itemHolder.add_wishlist.setColorFilter(red);
                    if (mListener != null) {
                        mListener.onUpdate();
                    }
                }
            }
        });
    }

    public static void setOnUpdateListener(OnUpdateListener listener) {
        mListener = listener;
    }

    public void setFilter(List<DataForViewWishList.ResponseBean> filteredModelList) {
        userList = new ArrayList<>();
        userList.addAll(filteredModelList);
        notifyDataSetChanged();
    }


    public interface OnUpdateListener {
        void onUpdate();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        private final TextView discount;
        private final AppCompatButton addtocart;
        private final TextView percentage;
        public TextView title, price;
        public NetworkImageView thumbnail;
        ImageView add_wishlist;
        RelativeLayout relative_layout;

        public ItemViewHolder(View itemView) {

            super(itemView);
            relative_layout = (RelativeLayout) itemView.findViewById(R.id.card_view);
            title = (TextView) itemView.findViewById(R.id.tv_product_name);
            price = (TextView) itemView.findViewById(R.id.tv_price);

            discount = (TextView) itemView.findViewById(R.id.tv_price_with_dicount);
            percentage = (TextView) itemView.findViewById(R.id.tv_price_percentage);


            thumbnail = (NetworkImageView) itemView.findViewById(R.id.thumbnail);
            add_wishlist = (ImageView) itemView.findViewById(R.id.add_wishlist);
            addtocart = (AppCompatButton) itemView.findViewById(R.id.addtocart);
        }
    }


}
