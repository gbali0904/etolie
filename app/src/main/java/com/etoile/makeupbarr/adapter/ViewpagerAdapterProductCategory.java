package com.etoile.makeupbarr.adapter;

import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.Utility.TAGS;
import com.etoile.makeupbarr.activity.ProductList;
import com.etoile.makeupbarr.activity.SearchViewActivity;
import com.etoile.makeupbarr.model.DataForDrawerCategories;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by AndroidPC on 14-01-2017.
 */
public class ViewpagerAdapterProductCategory extends PagerAdapter {


    private final SearchViewActivity baseActivity;
    private final List<DataForDrawerCategories.ResponseBean> userList;

    ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public ViewpagerAdapterProductCategory(SearchViewActivity baseActivity, List<DataForDrawerCategories.ResponseBean> responseBean) {
        this.baseActivity=baseActivity;
        this.userList=responseBean;
    }

    @Override
    public int getCount() {
        return userList.size();
    }
    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = LayoutInflater.from(baseActivity).inflate(R.layout.category_list_activity, container, false);
        final DataForDrawerCategories.ResponseBean responseBean = userList.get(position);

        TextView productName=(TextView) itemView.findViewById(R.id.tv_productName);
        productName.setText(responseBean.getCategory_name());
        CircleImageView circleImageView=(CircleImageView) itemView.findViewById(R.id.im_productcategory);
        String imageUrl = responseBean.getImage();
        Picasso.with(getApplicationContext()).load(imageUrl)
                .placeholder(R.drawable.ic_new).error(R.drawable.ic_launcher)
                .into(circleImageView);
        LinearLayout layout=(LinearLayout) itemView.findViewById(R.id.layout);
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String category_id = responseBean.getCategory_id();
                AppController.getInstance().getPreferenceEditor().putString(TAGS.SUB_CATEGORY_ID, "").commit();
                AppController.getInstance().getPreferenceEditor().putString(TAGS.CATEGORY_ID, category_id).commit();
                Intent intent = new Intent(baseActivity, ProductList.class);
                baseActivity.startActivity(intent);
                baseActivity.finish();
            }
        });


             container.addView(itemView);

        return itemView;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
