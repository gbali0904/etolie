package com.etoile.makeupbarr.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.activity.FilterActivity;
import com.etoile.makeupbarr.model.DataForDrawerCategories;

import java.util.List;

/**
 * Created by AndroidPC on 04-01-2017.
 */
public class RecyclerViewAdapterForCategory extends RecyclerView.Adapter {
    private final List<DataForDrawerCategories.ResponseBean> userList;
    private final FilterActivity context;

    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    public RecyclerViewAdapterForCategory(FilterActivity filterFragment, List<DataForDrawerCategories.ResponseBean> category) {
        this.userList=category;
        this.context =filterFragment;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_list, parent, false);
        holder = new ItemViewHolder(itemView);
        Context context = parent.getContext();
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final ItemViewHolder itemHolder = (ItemViewHolder) holder;
        DataForDrawerCategories.ResponseBean user = userList.get(position);
        itemHolder.networkImageView.setImageUrl(user.getImage(),imageLoader);
        itemHolder.category_name.setText(user.getCategory_name());

        itemHolder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DataForDrawerCategories.ResponseBean user = userList.get(position);
                String category_id = user.getCategory_id();
                Log.i("Category ID",category_id);

            }
        });
    }

    @Override
    public int getItemCount() {

        return userList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private final NetworkImageView networkImageView;
        private final TextView category_name;
        LinearLayout linearLayout;

        public ItemViewHolder(View itemView) {
            super(itemView);
             linearLayout=(LinearLayout) itemView.findViewById(R.id.cr_layout);
             networkImageView=(NetworkImageView) itemView.findViewById(R.id.category_image);
             category_name=(TextView) itemView.findViewById(R.id.category_name);
        }
    }
}
