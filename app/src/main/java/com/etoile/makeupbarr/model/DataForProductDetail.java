package com.etoile.makeupbarr.model;

import java.util.List;

/**
 * Created by AndroidPC on 28-12-2016.
 */

public class DataForProductDetail {


    /**
     * status : success
     * message : Product details.
     * response : {"product_detail":{"product_id":"38","category_name":"Palettes","sub_category_name":"Palettes","product_name":"   MATTE EXPOSURE PALETTE","price":"1500","discount":"15","saving_price":225,"qty":"7","description":"gg","tips":"gh","ingrdients":"vv","shopping_return":"bvg"},"product_color_shape":[{"color_shape_id":"6","color_shape_code":"#fae6db","color_shape_name":"PORCELAIN"}],"product_images":[{"image_name":"eed47e3fbbc857b327e7105e46299980.jpeg","bigUrl":"http://www.makeupbarr.com/Admin/server/php/files/eed47e3fbbc857b327e7105e46299980.jpeg","thumbnailUrl":"http://www.makeupbarr.com/Admin/server/php/files/thumbnail/eed47e3fbbc857b327e7105e46299980.jpeg","mediumUrl":"http://www.makeupbarr.com/Admin/server/php/files/medium/eed47e3fbbc857b327e7105e46299980.jpeg"}],"wishlist":1,"review":[{"review_id":"23","uid":"64","product_id":"38","title":"ghf","comment":"ghi","rating":"3","active":"0","created":"2017-02-01 07:01:50","status":"0","fname":"Hakim","lname":"Khan"}],"added_in_cart":1}
     */

    private String status;
    private String message;
    private ResponseBean response;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public static class ResponseBean {
        /**
         * product_detail : {"product_id":"38","category_name":"Palettes","sub_category_name":"Palettes","product_name":"   MATTE EXPOSURE PALETTE","price":"1500","discount":"15","saving_price":225,"qty":"7","description":"gg","tips":"gh","ingrdients":"vv","shopping_return":"bvg"}
         * product_color_shape : [{"color_shape_id":"6","color_shape_code":"#fae6db","color_shape_name":"PORCELAIN"}]
         * product_images : [{"image_name":"eed47e3fbbc857b327e7105e46299980.jpeg","bigUrl":"http://www.makeupbarr.com/Admin/server/php/files/eed47e3fbbc857b327e7105e46299980.jpeg","thumbnailUrl":"http://www.makeupbarr.com/Admin/server/php/files/thumbnail/eed47e3fbbc857b327e7105e46299980.jpeg","mediumUrl":"http://www.makeupbarr.com/Admin/server/php/files/medium/eed47e3fbbc857b327e7105e46299980.jpeg"}]
         * wishlist : 1
         * review : [{"review_id":"23","uid":"64","product_id":"38","title":"ghf","comment":"ghi","rating":"3","active":"0","created":"2017-02-01 07:01:50","status":"0","fname":"Hakim","lname":"Khan"}]
         * added_in_cart : 1
         */

        private ProductDetailBean product_detail;
        private int wishlist;
        private int added_in_cart;
        private List<ProductColorShapeBean> product_color_shape;
        private List<ProductImagesBean> product_images;
        private List<ReviewBean> review;

        public ProductDetailBean getProduct_detail() {
            return product_detail;
        }

        public void setProduct_detail(ProductDetailBean product_detail) {
            this.product_detail = product_detail;
        }

        public int getWishlist() {
            return wishlist;
        }

        public void setWishlist(int wishlist) {
            this.wishlist = wishlist;
        }

        public int getAdded_in_cart() {
            return added_in_cart;
        }

        public void setAdded_in_cart(int added_in_cart) {
            this.added_in_cart = added_in_cart;
        }

        public List<ProductColorShapeBean> getProduct_color_shape() {
            return product_color_shape;
        }

        public void setProduct_color_shape(List<ProductColorShapeBean> product_color_shape) {
            this.product_color_shape = product_color_shape;
        }

        public List<ProductImagesBean> getProduct_images() {
            return product_images;
        }

        public void setProduct_images(List<ProductImagesBean> product_images) {
            this.product_images = product_images;
        }

        public List<ReviewBean> getReview() {
            return review;
        }

        public void setReview(List<ReviewBean> review) {
            this.review = review;
        }

        public static class ProductDetailBean {
            /**
             * product_id : 38
             * category_name : Palettes
             * sub_category_name : Palettes
             * product_name :    MATTE EXPOSURE PALETTE
             * price : 1500
             * discount : 15
             * saving_price : 225
             * qty : 7
             * description : gg
             * tips : gh
             * ingrdients : vv
             * shopping_return : bvg
             */

            private String product_id;
            private String category_name;
            private String sub_category_name;
            private String product_name;
            private String price;
            private String discount;
            private int saving_price;
            private String qty;
            private String description;
            private String tips;
            private String ingrdients;
            private String shopping_return;

            public String getProduct_id() {
                return product_id;
            }

            public void setProduct_id(String product_id) {
                this.product_id = product_id;
            }

            public String getCategory_name() {
                return category_name;
            }

            public void setCategory_name(String category_name) {
                this.category_name = category_name;
            }

            public String getSub_category_name() {
                return sub_category_name;
            }

            public void setSub_category_name(String sub_category_name) {
                this.sub_category_name = sub_category_name;
            }

            public String getProduct_name() {
                return product_name;
            }

            public void setProduct_name(String product_name) {
                this.product_name = product_name;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getDiscount() {
                return discount;
            }

            public void setDiscount(String discount) {
                this.discount = discount;
            }

            public int getSaving_price() {
                return saving_price;
            }

            public void setSaving_price(int saving_price) {
                this.saving_price = saving_price;
            }

            public String getQty() {
                return qty;
            }

            public void setQty(String qty) {
                this.qty = qty;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getTips() {
                return tips;
            }

            public void setTips(String tips) {
                this.tips = tips;
            }

            public String getIngrdients() {
                return ingrdients;
            }

            public void setIngrdients(String ingrdients) {
                this.ingrdients = ingrdients;
            }

            public String getShopping_return() {
                return shopping_return;
            }

            public void setShopping_return(String shopping_return) {
                this.shopping_return = shopping_return;
            }
        }

        public static class ProductColorShapeBean {
            /**
             * color_shape_id : 6
             * color_shape_code : #fae6db
             * color_shape_name : PORCELAIN
             */

            private String color_shape_id;
            private String color_shape_code;
            private String color_shape_name;

            public String getColor_shape_id() {
                return color_shape_id;
            }

            public void setColor_shape_id(String color_shape_id) {
                this.color_shape_id = color_shape_id;
            }

            public String getColor_shape_code() {
                return color_shape_code;
            }

            public void setColor_shape_code(String color_shape_code) {
                this.color_shape_code = color_shape_code;
            }

            public String getColor_shape_name() {
                return color_shape_name;
            }

            public void setColor_shape_name(String color_shape_name) {
                this.color_shape_name = color_shape_name;
            }
        }

        public static class ProductImagesBean {
            /**
             * image_name : eed47e3fbbc857b327e7105e46299980.jpeg
             * bigUrl : http://www.makeupbarr.com/Admin/server/php/files/eed47e3fbbc857b327e7105e46299980.jpeg
             * thumbnailUrl : http://www.makeupbarr.com/Admin/server/php/files/thumbnail/eed47e3fbbc857b327e7105e46299980.jpeg
             * mediumUrl : http://www.makeupbarr.com/Admin/server/php/files/medium/eed47e3fbbc857b327e7105e46299980.jpeg
             */

            private String image_name;
            private String bigUrl;
            private String thumbnailUrl;
            private String mediumUrl;

            public String getImage_name() {
                return image_name;
            }

            public void setImage_name(String image_name) {
                this.image_name = image_name;
            }

            public String getBigUrl() {
                return bigUrl;
            }

            public void setBigUrl(String bigUrl) {
                this.bigUrl = bigUrl;
            }

            public String getThumbnailUrl() {
                return thumbnailUrl;
            }

            public void setThumbnailUrl(String thumbnailUrl) {
                this.thumbnailUrl = thumbnailUrl;
            }

            public String getMediumUrl() {
                return mediumUrl;
            }

            public void setMediumUrl(String mediumUrl) {
                this.mediumUrl = mediumUrl;
            }
        }

        public static class ReviewBean {
            /**
             * review_id : 23
             * uid : 64
             * product_id : 38
             * title : ghf
             * comment : ghi
             * rating : 3
             * active : 0
             * created : 2017-02-01 07:01:50
             * status : 0
             * fname : Hakim
             * lname : Khan
             */

            private String review_id;
            private String uid;
            private String product_id;
            private String title;
            private String comment;
            private String rating;
            private String active;
            private String created;
            private String status;
            private String fname;
            private String lname;

            public String getReview_id() {
                return review_id;
            }

            public void setReview_id(String review_id) {
                this.review_id = review_id;
            }

            public String getUid() {
                return uid;
            }

            public void setUid(String uid) {
                this.uid = uid;
            }

            public String getProduct_id() {
                return product_id;
            }

            public void setProduct_id(String product_id) {
                this.product_id = product_id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getComment() {
                return comment;
            }

            public void setComment(String comment) {
                this.comment = comment;
            }

            public String getRating() {
                return rating;
            }

            public void setRating(String rating) {
                this.rating = rating;
            }

            public String getActive() {
                return active;
            }

            public void setActive(String active) {
                this.active = active;
            }

            public String getCreated() {
                return created;
            }

            public void setCreated(String created) {
                this.created = created;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getFname() {
                return fname;
            }

            public void setFname(String fname) {
                this.fname = fname;
            }

            public String getLname() {
                return lname;
            }

            public void setLname(String lname) {
                this.lname = lname;
            }
        }
    }
}
