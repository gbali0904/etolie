package com.etoile.makeupbarr.model;

import java.util.List;

/**
 * Created by AndroidPC on 27-12-2016.
 */

public class DataForProjectListing {

    /**
     * status : success
     * message : Product details.
     * response : {"product_detail":{"product_id":"2","category_name":"Face","sub_category_name":"Foundation + Powder","product_name":"Beauty Massage","price":"500","discount":"0","saving_price":"0","qty":"0","description":"<p>sd fsdf dsfds<\/p>","tips":"<p>sdfsdf<\/p>","ingrdients":"<p>s dfdsf<\/p>","shopping_return":"<p>dsfsdfsdf<\/p>"},"product_color_shape":[{"color_shape_id":"2","color_shape_code":"#151516","color_shape_name":"BLACK CREAM"},{"color_shape_id":"1","color_shape_code":"#ef5190","color_shape_name":"PINK CREAM"}],"product_images":[{"image_name":"33e9c971073e034ded871e17d92c3f6f.png","bigUrl":"http://www.makeupbarr.com/Admin/server/php/files/33e9c971073e034ded871e17d92c3f6f.png","thumbnailUrl":"http://www.makeupbarr.com/Admin/server/php/files/thumbnail/33e9c971073e034ded871e17d92c3f6f.png","mediumUrl":"http://www.makeupbarr.com/Admin/server/php/files/medium/33e9c971073e034ded871e17d92c3f6f.png"}],"wishlist":0}
     */

    private String status;
    private String message;
    private ResponseBean response;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public static class ResponseBean {
        /**
         * product_detail : {"product_id":"2","category_name":"Face","sub_category_name":"Foundation + Powder","product_name":"Beauty Massage","price":"500","discount":"0","saving_price":"0","qty":"0","description":"<p>sd fsdf dsfds<\/p>","tips":"<p>sdfsdf<\/p>","ingrdients":"<p>s dfdsf<\/p>","shopping_return":"<p>dsfsdfsdf<\/p>"}
         * product_color_shape : [{"color_shape_id":"2","color_shape_code":"#151516","color_shape_name":"BLACK CREAM"},{"color_shape_id":"1","color_shape_code":"#ef5190","color_shape_name":"PINK CREAM"}]
         * product_images : [{"image_name":"33e9c971073e034ded871e17d92c3f6f.png","bigUrl":"http://www.makeupbarr.com/Admin/server/php/files/33e9c971073e034ded871e17d92c3f6f.png","thumbnailUrl":"http://www.makeupbarr.com/Admin/server/php/files/thumbnail/33e9c971073e034ded871e17d92c3f6f.png","mediumUrl":"http://www.makeupbarr.com/Admin/server/php/files/medium/33e9c971073e034ded871e17d92c3f6f.png"}]
         * wishlist : 0
         */

        private ProductDetailBean product_detail;
        private int wishlist;
        private List<ProductColorShapeBean> product_color_shape;
        private List<ProductImagesBean> product_images;

        public ProductDetailBean getProduct_detail() {
            return product_detail;
        }

        public void setProduct_detail(ProductDetailBean product_detail) {
            this.product_detail = product_detail;
        }

        public int getWishlist() {
            return wishlist;
        }

        public void setWishlist(int wishlist) {
            this.wishlist = wishlist;
        }

        public List<ProductColorShapeBean> getProduct_color_shape() {
            return product_color_shape;
        }

        public void setProduct_color_shape(List<ProductColorShapeBean> product_color_shape) {
            this.product_color_shape = product_color_shape;
        }

        public List<ProductImagesBean> getProduct_images() {
            return product_images;
        }

        public void setProduct_images(List<ProductImagesBean> product_images) {
            this.product_images = product_images;
        }

        public static class ProductDetailBean {
            /**
             * product_id : 2
             * category_name : Face
             * sub_category_name : Foundation + Powder
             * product_name : Beauty Massage
             * price : 500
             * discount : 0
             * saving_price : 0
             * qty : 0
             * description : <p>sd fsdf dsfds</p>
             * tips : <p>sdfsdf</p>
             * ingrdients : <p>s dfdsf</p>
             * shopping_return : <p>dsfsdfsdf</p>
             */

            private String product_id;
            private String category_name;
            private String sub_category_name;
            private String product_name;
            private String price;
            private String discount;
            private String saving_price;
            private String qty;
            private String description;
            private String tips;
            private String ingrdients;
            private String shopping_return;

            public String getProduct_id() {
                return product_id;
            }

            public void setProduct_id(String product_id) {
                this.product_id = product_id;
            }

            public String getCategory_name() {
                return category_name;
            }

            public void setCategory_name(String category_name) {
                this.category_name = category_name;
            }

            public String getSub_category_name() {
                return sub_category_name;
            }

            public void setSub_category_name(String sub_category_name) {
                this.sub_category_name = sub_category_name;
            }

            public String getProduct_name() {
                return product_name;
            }

            public void setProduct_name(String product_name) {
                this.product_name = product_name;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getDiscount() {
                return discount;
            }

            public void setDiscount(String discount) {
                this.discount = discount;
            }

            public String getSaving_price() {
                return saving_price;
            }

            public void setSaving_price(String saving_price) {
                this.saving_price = saving_price;
            }

            public String getQty() {
                return qty;
            }

            public void setQty(String qty) {
                this.qty = qty;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getTips() {
                return tips;
            }

            public void setTips(String tips) {
                this.tips = tips;
            }

            public String getIngrdients() {
                return ingrdients;
            }

            public void setIngrdients(String ingrdients) {
                this.ingrdients = ingrdients;
            }

            public String getShopping_return() {
                return shopping_return;
            }

            public void setShopping_return(String shopping_return) {
                this.shopping_return = shopping_return;
            }
        }

        public static class ProductColorShapeBean {
            /**
             * color_shape_id : 2
             * color_shape_code : #151516
             * color_shape_name : BLACK CREAM
             */

            private String color_shape_id;
            private String color_shape_code;
            private String color_shape_name;

            public String getColor_shape_id() {
                return color_shape_id;
            }

            public void setColor_shape_id(String color_shape_id) {
                this.color_shape_id = color_shape_id;
            }

            public String getColor_shape_code() {
                return color_shape_code;
            }

            public void setColor_shape_code(String color_shape_code) {
                this.color_shape_code = color_shape_code;
            }

            public String getColor_shape_name() {
                return color_shape_name;
            }

            public void setColor_shape_name(String color_shape_name) {
                this.color_shape_name = color_shape_name;
            }
        }

        public static class ProductImagesBean {
            /**
             * image_name : 33e9c971073e034ded871e17d92c3f6f.png
             * bigUrl : http://www.makeupbarr.com/Admin/server/php/files/33e9c971073e034ded871e17d92c3f6f.png
             * thumbnailUrl : http://www.makeupbarr.com/Admin/server/php/files/thumbnail/33e9c971073e034ded871e17d92c3f6f.png
             * mediumUrl : http://www.makeupbarr.com/Admin/server/php/files/medium/33e9c971073e034ded871e17d92c3f6f.png
             */

            private String image_name;
            private String bigUrl;
            private String thumbnailUrl;
            private String mediumUrl;

            public String getImage_name() {
                return image_name;
            }

            public void setImage_name(String image_name) {
                this.image_name = image_name;
            }

            public String getBigUrl() {
                return bigUrl;
            }

            public void setBigUrl(String bigUrl) {
                this.bigUrl = bigUrl;
            }

            public String getThumbnailUrl() {
                return thumbnailUrl;
            }

            public void setThumbnailUrl(String thumbnailUrl) {
                this.thumbnailUrl = thumbnailUrl;
            }

            public String getMediumUrl() {
                return mediumUrl;
            }

            public void setMediumUrl(String mediumUrl) {
                this.mediumUrl = mediumUrl;
            }
        }
    }
}
