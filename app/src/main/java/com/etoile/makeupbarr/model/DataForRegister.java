package com.etoile.makeupbarr.model;

/**
 * Created by AndroidPC on 23-12-2016.
 */

public class DataForRegister {
    /**
     * status : success
     * message : User Registerd Successfully
     * response : {"uid":71,"fname":"anjali","email":"anjali@gmail.com","phone":"0909090909","password":"123456"}
     */

    private String status;
    private String message;
    private ResponseBean response;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public static class ResponseBean {
        /**
         * uid : 71
         * fname : anjali
         * email : anjali@gmail.com
         * phone : 0909090909
         * password : 123456
         */

        private int uid;
        private String fname;
        private String email;
        private String phone;
        private String password;

        public int getUid() {
            return uid;
        }

        public void setUid(int uid) {
            this.uid = uid;
        }

        public String getFname() {
            return fname;
        }

        public void setFname(String fname) {
            this.fname = fname;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }


   /* *//**
     * status : success
     * message : User Registerd Successfully
     * response : {"uid":50,"fname":"abc","lname":"abc","email":"hh@gnvc.bnm","phone":"774534990","password":"1234567","address":"fdxsgvxfcvdxcf"}
     *//*

    private String status;
    private String message;
    private ResponseBean response;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public static class ResponseBean {
        *//**
         * uid : 50
         * fname : abc
         * lname : abc
         * email : hh@gnvc.bnm
         * phone : 774534990
         * password : 1234567
         * address : fdxsgvxfcvdxcf
         *//*

        private int uid;
        private String fname;
        private String lname;
        private String email;
        private String phone;
        private String password;
        private String address;

        public int getUid() {
            return uid;
        }

        public void setUid(int uid) {
            this.uid = uid;
        }

        public String getFname() {
            return fname;
        }

        public void setFname(String fname) {
            this.fname = fname;
        }

        public String getLname() {
            return lname;
        }

        public void setLname(String lname) {
            this.lname = lname;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }
    }*/
}
