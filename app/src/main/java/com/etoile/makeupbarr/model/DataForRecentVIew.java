package com.etoile.makeupbarr.model;

import java.util.List;

/**
 * Created by admin on 2/20/2017.
 */

public class DataForRecentVIew {


    /**
     * status : success
     * message : Recent View listing
     * result : [{"product_id":"29","product_name":"ALWAYS ON GEL EYE LINER","price":"1000","discount":"25","saving_price":250,"qty":"19","image_url":"http://www.makeupbarr.com/Admin/server/php/files/4ea6dce1ea32a6d0a25ec7c39544939e.png","real_price":750}]
     */

    private String status;
    private String message;
    private List<ResultBean> result;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ResultBean> getResult() {
        return result;
    }

    public void setResult(List<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean {
        /**
         * product_id : 29
         * product_name : ALWAYS ON GEL EYE LINER
         * price : 1000
         * discount : 25
         * saving_price : 250
         * qty : 19
         * image_url : http://www.makeupbarr.com/Admin/server/php/files/4ea6dce1ea32a6d0a25ec7c39544939e.png
         * real_price : 750
         */

        private String product_id;
        private String product_name;
        private String price;
        private String discount;
        private int saving_price;
        private String qty;
        private String image_url;
        private int real_price;

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getProduct_name() {
            return product_name;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public int getSaving_price() {
            return saving_price;
        }

        public void setSaving_price(int saving_price) {
            this.saving_price = saving_price;
        }

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }

        public String getImage_url() {
            return image_url;
        }

        public void setImage_url(String image_url) {
            this.image_url = image_url;
        }

        public int getReal_price() {
            return real_price;
        }

        public void setReal_price(int real_price) {
            this.real_price = real_price;
        }
    }
}
