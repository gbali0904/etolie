package com.etoile.makeupbarr.model;

import java.util.List;

/**
 * Created by AndroidPC on 26-12-2016.
 */

public class DataForDrawerCategories {

    private static DataForDrawerCategories mInstance;
    /**
     * status : success
     * message : Category listing.
     * response : [{"category_id":"2","category_name":"Face","image":"http://www.makeupbarr.com/category_images/2/1252-face.png","sub_category":[{"sub_category_id":"2","sub_category_name":"PRIMER"},{"sub_category_id":"3","sub_category_name":"Foundation + Powder"}]}]
     * brand : [{"brand_id":"3","brand_name":"loreal"}]
     */

    private String status;
    private String message;
    private List<ResponseBean> response;
    private List<BrandBean> brand;

    public static DataForDrawerCategories getInstance(){
        if(mInstance==null)
            mInstance=new DataForDrawerCategories();
        return mInstance;
    }

    public static void setInstance(DataForDrawerCategories dataForDrawerCategories){
        mInstance=dataForDrawerCategories;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ResponseBean> getResponse() {
        return response;
    }

    public void setResponse(List<ResponseBean> response) {
        this.response = response;
    }

    public List<BrandBean> getBrand() {
        return brand;
    }

    public void setBrand(List<BrandBean> brand) {
        this.brand = brand;
    }


    public static class ResponseBean {
        /**
         * category_id : 2
         * category_name : Face
         * image : http://www.makeupbarr.com/category_images/2/1252-face.png
         * sub_category : [{"sub_category_id":"2","sub_category_name":"PRIMER"},{"sub_category_id":"3","sub_category_name":"Foundation + Powder"}]
         */

        private String category_id;
        private String category_name;
        private String image;
        private List<SubCategoryBean> sub_category;

        public String getCategory_id() {
            return category_id;
        }

        public void setCategory_id(String category_id) {
            this.category_id = category_id;
        }

        public String getCategory_name() {
            return category_name;
        }

        public void setCategory_name(String category_name) {
            this.category_name = category_name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public List<SubCategoryBean> getSub_category() {
            return sub_category;
        }

        public void setSub_category(List<SubCategoryBean> sub_category) {
            this.sub_category = sub_category;
        }

        public static class SubCategoryBean {
            /**
             * sub_category_id : 2
             * sub_category_name : PRIMER
             */

            private String sub_category_id;
            private String sub_category_name;

            public String getSub_category_id() {
                return sub_category_id;
            }

            public void setSub_category_id(String sub_category_id) {
                this.sub_category_id = sub_category_id;
            }

            public String getSub_category_name() {
                return sub_category_name;
            }

            public void setSub_category_name(String sub_category_name) {
                this.sub_category_name = sub_category_name;
            }
        }
    }

    public static class BrandBean {
        /**
         * brand_id : 3
         * brand_name : loreal
         */

        private String brand_id;
        private String brand_name;

        public String getBrand_id() {
            return brand_id;
        }

        public void setBrand_id(String brand_id) {
            this.brand_id = brand_id;
        }

        public String getBrand_name() {
            return brand_name;
        }

        public void setBrand_name(String brand_name) {
            this.brand_name = brand_name;
        }
    }
}
