package com.etoile.makeupbarr.model;

/**
 * Created by AndroidPC on 26-12-2016.
 */

public class DataForOTPVerify {

    /**
     * status : success
     * message : Otp is matched
     */

    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
