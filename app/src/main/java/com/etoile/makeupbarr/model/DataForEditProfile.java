package com.etoile.makeupbarr.model;

/**
 * Created by AndroidPC on 26-12-2016.
 */

public class DataForEditProfile {
    /**
     * status : success
     * message : Profile has been successfully updated.
     * response : {"uid":"35","fname":"hakim","email":"gbali0904@gmail.com","phone":"8684858787"}
     */

    private String status;
    private String message;
    private ResponseBean response;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public static class ResponseBean {
        /**
         * uid : 35
         * fname : hakim
         * email : gbali0904@gmail.com
         * phone : 8684858787
         */

        private String uid;
        private String fname;
        private String email;
        private String phone;

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getFname() {
            return fname;
        }

        public void setFname(String fname) {
            this.fname = fname;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }
    }







/*
    *//**
     * status : success
     * message : Profile has been successfully updated.
     * response : {"uid":"15","fname":"subhash121","lname":"kumar","email":"subhash@websolutioncentre.com","phone":"1234567890","address":"abc xyz 123"}
     *//*

    private String status;
    private String message;
    private ResponseBean response;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public static class ResponseBean {
        *//**
         * uid : 15
         * fname : subhash121
         * lname : kumar
         * email : subhash@websolutioncentre.com
         * phone : 1234567890
         * address : abc xyz 123
         *//*

        private String uid;
        private String fname;
        private String lname;
        private String email;
        private String phone;
        private String address;

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getFname() {
            return fname;
        }

        public void setFname(String fname) {
            this.fname = fname;
        }

        public String getLname() {
            return lname;
        }

        public void setLname(String lname) {
            this.lname = lname;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }
    }*/
}
