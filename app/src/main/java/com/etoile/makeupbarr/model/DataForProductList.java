package com.etoile.makeupbarr.model;

import java.util.List;

/**
 * Created by AndroidPC on 27-12-2016.
 */

public class DataForProductList {

    /**
     * status : success
     * message : Product listing.
     * response : [{"product_id":"19","category_name":"Face","sub_category_name":"PRIMER","product_name":"Testing","price":"800","discount":"10","saving_price":80,"qty":"10","image_url":"http://www.makeupbarr.com/Admin/server/php/files/147816979901 - Copy (7).jpg","wishlist":"1"}]
     */

    private String status;
    private String message;
    private List<ResponseBean> response;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ResponseBean> getResponse() {
        return response;
    }

    public void setResponse(List<ResponseBean> response) {
        this.response = response;
    }

    public static class ResponseBean {
        /**
         * product_id : 19
         * category_name : Face
         * sub_category_name : PRIMER
         * product_name : Testing
         * price : 800
         * discount : 10
         * saving_price : 80
         * qty : 10
         * image_url : http://www.makeupbarr.com/Admin/server/php/files/147816979901 - Copy (7).jpg
         * wishlist : 1
         */

        private String product_id;
        private String category_name;
        private String sub_category_name;
        private String product_name;
        private String price;
        private String discount;
        private int saving_price;
        private String qty;
        private String image_url;
        private String wishlist;

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getCategory_name() {
            return category_name;
        }

        public void setCategory_name(String category_name) {
            this.category_name = category_name;
        }

        public String getSub_category_name() {
            return sub_category_name;
        }

        public void setSub_category_name(String sub_category_name) {
            this.sub_category_name = sub_category_name;
        }

        public String getProduct_name() {
            return product_name;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public int getSaving_price() {
            return saving_price;
        }

        public void setSaving_price(int saving_price) {
            this.saving_price = saving_price;
        }

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }

        public String getImage_url() {
            return image_url;
        }

        public void setImage_url(String image_url) {
            this.image_url = image_url;
        }

        public String getWishlist() {
            return wishlist;
        }

        public void setWishlist(String wishlist) {
            this.wishlist = wishlist;
        }
    }
}
