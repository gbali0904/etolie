package com.etoile.makeupbarr.model;

import java.util.List;

/**
 * Created by admin on 1/2/2017.
 */

public class DataForViewAddCart {


    /**
     * status : success
     * message : Cart listing
     * response : [{"product_id":"32","product_name":"PHOTO FINISH LASH PRIMER","price":"1074","discount":"12","saving_price":"1220","qty":"1","total_price":"1074","shape_name":"PINK CREAM","color_code":"#ef5190","url":"http://www.makeupbarr.com/Admin/server/php/files/f382698f61d1324e6698ad3dbc376f41.jpeg"}]
     */

    private String status;
    private String message;
    private List<ResponseBean> response;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ResponseBean> getResponse() {
        return response;
    }

    public void setResponse(List<ResponseBean> response) {
        this.response = response;
    }

    public static class ResponseBean {
        /**
         * product_id : 32
         * product_name : PHOTO FINISH LASH PRIMER
         * price : 1074
         * discount : 12
         * saving_price : 1220
         * qty : 1
         * total_price : 1074
         * shape_name : PINK CREAM
         * color_code : #ef5190
         * url : http://www.makeupbarr.com/Admin/server/php/files/f382698f61d1324e6698ad3dbc376f41.jpeg
         */

        private String product_id;
        private String product_name;
        private String price;
        private String discount;
        private String saving_price;
        private String qty;
        private String total_price;
        private String shape_name;
        private String color_code;
        private String url;

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getProduct_name() {
            return product_name;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public String getSaving_price() {
            return saving_price;
        }

        public void setSaving_price(String saving_price) {
            this.saving_price = saving_price;
        }

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }

        public String getTotal_price() {
            return total_price;
        }

        public void setTotal_price(String total_price) {
            this.total_price = total_price;
        }

        public String getShape_name() {
            return shape_name;
        }

        public void setShape_name(String shape_name) {
            this.shape_name = shape_name;
        }

        public String getColor_code() {
            return color_code;
        }

        public void setColor_code(String color_code) {
            this.color_code = color_code;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }
}
