package com.etoile.makeupbarr.model;

/**
 * Created by AndroidPC on 07-01-2017.
 */

public class DataForAddress {


    /**
     * status : success
     * message : address insert successfully.
     * response : {"uid":"35","pincode":"110044","name":"dasd","address":"44444","locality":"dasd","city":"dasd","state":"dsad","mobile":"1234567890"}
     */

    private String status;
    private String message;
    private ResponseBean response;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public static class ResponseBean {
        /**
         * uid : 35
         * pincode : 110044
         * name : dasd
         * address : 44444
         * locality : dasd
         * city : dasd
         * state : dsad
         * mobile : 1234567890
         */

        private String uid;
        private String pincode;
        private String name;
        private String address;
        private String locality;
        private String city;
        private String state;
        private String mobile;

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getPincode() {
            return pincode;
        }

        public void setPincode(String pincode) {
            this.pincode = pincode;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getLocality() {
            return locality;
        }

        public void setLocality(String locality) {
            this.locality = locality;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }
    }
}
