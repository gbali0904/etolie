package com.etoile.makeupbarr.model;

import java.util.List;

/**
 * Created by admin on 12/29/2016.
 */

public class DataForViewWishList {

    /**
     * status : success
     * message : User all wishlist.
     * response : [{"product_id":"15","product_name":"test","price":"500","discount":"0","saving_price":"0","qty":"20","url":"http://www.makeupbarr.com/Admin/server/php/files/1478165653gymnastic.jpg"}]
     */

    private String status;
    private String message;
    private List<ResponseBean> response;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ResponseBean> getResponse() {
        return response;
    }

    public void setResponse(List<ResponseBean> response) {
        this.response = response;
    }

    public static class ResponseBean {
        /**
         * product_id : 15
         * product_name : test
         * price : 500
         * discount : 0
         * saving_price : 0
         * qty : 20
         * url : http://www.makeupbarr.com/Admin/server/php/files/1478165653gymnastic.jpg
         */

        private String product_id;
        private String product_name;
        private String price;
        private String discount;
        private String saving_price;
        private String qty;
        private String url;

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getProduct_name() {
            return product_name;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public String getSaving_price() {
            return saving_price;
        }

        public void setSaving_price(String saving_price) {
            this.saving_price = saving_price;
        }

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }
}
