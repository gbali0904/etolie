package com.etoile.makeupbarr.model;

/**
 * Created by AndroidPC on 03-01-2017.
 */

public class DataForCurrentPassword {

    /**
     * status : success
     * message : your password has been successfully changed
     */

    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
