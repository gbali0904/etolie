package com.etoile.makeupbarr.model;

/**
 * Created by AndroidPC on 12-01-2017.
 */

public class DataForPaymentHash {

    /**
     * status : success
     * hash : d5b366ef3955c2e16e126401c84b80d7bf312535271e3abd388e0e6e76444a7bf3b1a1189ea2fd2911baf39a1f755c1a13929d77029cf341e4f2dff7efad1d29
     */

    private String status;
    private String hash;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }
}
