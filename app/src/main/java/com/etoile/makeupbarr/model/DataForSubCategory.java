package com.etoile.makeupbarr.model;

import java.util.List;

/**
 * Created by AndroidPC on 14-01-2017.
 */

public class DataForSubCategory {

    /**
     * status : success
     * message : Sub Category listing.
     * sub_category_list : [{"sub_category_id":"2","sub_category_name":"PRIMER"}]
     */

    private String status;
    private String message;
    private List<SubCategoryListBean> sub_category_list;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<SubCategoryListBean> getSub_category_list() {
        return sub_category_list;
    }

    public void setSub_category_list(List<SubCategoryListBean> sub_category_list) {
        this.sub_category_list = sub_category_list;
    }

    public static class SubCategoryListBean {
        /**
         * sub_category_id : 2
         * sub_category_name : PRIMER
         */

        private String sub_category_id;
        private String sub_category_name;

        public String getSub_category_id() {
            return sub_category_id;
        }

        public void setSub_category_id(String sub_category_id) {
            this.sub_category_id = sub_category_id;
        }

        public String getSub_category_name() {
            return sub_category_name;
        }

        public void setSub_category_name(String sub_category_name) {
            this.sub_category_name = sub_category_name;
        }
    }
}
