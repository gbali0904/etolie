package com.etoile.makeupbarr.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by AndroidPC on 07-01-2017.
 */

public class DataForViewAddress {


    /**
     * status : success
     * message : Address listing.
     * response : null
     * address : [{"address_id":"3","uid":"35","pincode":"110011","name":"dasd","address":"dsaddcfdsfcdxdc","locality":"dasd","city":"dasd","state":"dsad","mobile":"1234567890"}]
     */

    private String status;
    private String message;
    private Object response;
    private List<AddressBean> address;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getResponse() {
        return response;
    }

    public void setResponse(Object response) {
        this.response = response;
    }

    public List<AddressBean> getAddress() {
        return address;
    }

    public void setAddress(List<AddressBean> address) {
        this.address = address;
    }



    public static class AddressBean implements Serializable {
        /**
         * address_id : 3
         * uid : 35
         * pincode : 110011
         * name : dasd
         * address : dsaddcfdsfcdxdc
         * locality : dasd
         * city : dasd
         * state : dsad
         * mobile : 1234567890
         */

        private String address_id;
        private String uid;
        private String pincode;
        private String name;
        private String address;
        private String locality;
        private String city;
        private String state;
        private String mobile;
        public boolean isSelected;

        public String getAddress_id() {
            return address_id;
        }

        public void setAddress_id(String address_id) {
            this.address_id = address_id;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getPincode() {
            return pincode;
        }

        public void setPincode(String pincode) {
            this.pincode = pincode;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getLocality() {
            return locality;
        }

        public void setLocality(String locality) {
            this.locality = locality;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }


        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean isSelected) {
            this.isSelected = isSelected;
        }


    }



}
