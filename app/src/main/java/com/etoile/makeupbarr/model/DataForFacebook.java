package com.etoile.makeupbarr.model;

/**
 * Created by AndroidPC on 23-12-2016.
 */

public class DataForFacebook {

    /**
     * status : success
     * message : user already registered.
     * response : {"uid":"51","fname":"subhash","lname":"kumar","email":"subhash00123@websolutioncentre.com"}
     */

    private String status;
    private String message;
    private ResponseBean response;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public static class ResponseBean {
        /**
         * uid : 51
         * fname : subhash
         * lname : kumar
         * email : subhash00123@websolutioncentre.com
         */

        private String uid;
        private String fname;
        private String lname;
        private String email;

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getFname() {
            return fname;
        }

        public void setFname(String fname) {
            this.fname = fname;
        }

        public String getLname() {
            return lname;
        }

        public void setLname(String lname) {
            this.lname = lname;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }
}
