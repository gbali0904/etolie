package com.etoile.makeupbarr.model;

/**
 * Created by AndroidPC on 26-12-2016.
 */

public class DataForForgotPassword {

    /**
     * status : success
     * message : Otp has been successfully sent.
     */

    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
