package com.etoile.makeupbarr.model;

import java.util.List;

/**
 * Created by AndroidPC on 16-01-2017.
 */

public class DataForSearch {


    private List<ResponseBean> response;

    public List<ResponseBean> getResponse() {
        return response;
    }

    public void setResponse(List<ResponseBean> response) {
        this.response = response;
    }

    public static class ResponseBean {
        /**
         * sub_category_id : 2
         * sub_category_name : PRIMER
         * category_id : 2
         * category_name : Face
         * image : http://www.makeupbarr.com/category_images/2/1252-face.png
         * brand_id : 2
         * brand_name : lakme
         * product_id : 19
         * product_name : Testing
         * price : 800
         * discount : 10
         * saving_price : 80
         * qty : 10
         * image_url : http://www.makeupbarr.com/Admin/server/php/files/147816979901 - Copy (7).jpg
         */

        private String sub_category_id;
        private String sub_category_name;
        private String category_id;
        private String category_name;
        private String image;
        private String brand_id;
        private String brand_name;
        private String product_id;
        private String product_name;
        private String price;
        private String discount;
        private int saving_price;
        private String qty;
        private String image_url;

        public String getSub_category_id() {
            return sub_category_id;
        }

        public void setSub_category_id(String sub_category_id) {
            this.sub_category_id = sub_category_id;
        }

        public String getSub_category_name() {
            return sub_category_name;
        }

        public void setSub_category_name(String sub_category_name) {
            this.sub_category_name = sub_category_name;
        }

        public String getCategory_id() {
            return category_id;
        }

        public void setCategory_id(String category_id) {
            this.category_id = category_id;
        }

        public String getCategory_name() {
            return category_name;
        }

        public void setCategory_name(String category_name) {
            this.category_name = category_name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getBrand_id() {
            return brand_id;
        }

        public void setBrand_id(String brand_id) {
            this.brand_id = brand_id;
        }

        public String getBrand_name() {
            return brand_name;
        }

        public void setBrand_name(String brand_name) {
            this.brand_name = brand_name;
        }

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getProduct_name() {
            return product_name;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public int getSaving_price() {
            return saving_price;
        }

        public void setSaving_price(int saving_price) {
            this.saving_price = saving_price;
        }

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }

        public String getImage_url() {
            return image_url;
        }

        public void setImage_url(String image_url) {
            this.image_url = image_url;
        }
    }
}
