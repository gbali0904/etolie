package com.etoile.makeupbarr.model;

/**
 * Created by AndroidPC on 10-01-2017.
 */

public class DataForHashGenration {


    /**
     * status : success
     * KEY : dRQuiA
     * SALT : teEkuVg2
     * MID : 4928174
     * amount : 1200
     * txnid : 207d4f3c53f3774d00e7
     * hash : d72c6c99bd894ee115209f2334751369f21b28f98b5b40131b564a533a191144c931dc66d8b75f45fda91518c2c8c5a7f1989049caa4b3d2a07cb594486a6dce
     * fname : gtg
     * phone : 4545454545
     * productInfo :  Products
     * serviceProvider : payu_paisa
     * email : gg@gmail.com
     * address : ggg
     * zip_code : 11111
     * city : delhi
     * state_id : 21
     * Surl : http://www.makeupbarr.com/paymentSuccess.php
     * Furl : http://www.makeupbarr.com/paymentFailure.php
     */

    private String status;
    private String KEY;
    private String SALT;
    private String MID;
    private String amount;
    private String txnid;
    private String hash;
    private String fname;
    private String phone;
    private String productInfo;
    private String serviceProvider;
    private String email;
    private String address;
    private String zip_code;
    private String city;
    private String state_id;
    private String Surl;
    private String Furl;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getKEY() {
        return KEY;
    }

    public void setKEY(String KEY) {
        this.KEY = KEY;
    }

    public String getSALT() {
        return SALT;
    }

    public void setSALT(String SALT) {
        this.SALT = SALT;
    }

    public String getMID() {
        return MID;
    }

    public void setMID(String MID) {
        this.MID = MID;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTxnid() {
        return txnid;
    }

    public void setTxnid(String txnid) {
        this.txnid = txnid;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getProductInfo() {
        return productInfo;
    }

    public void setProductInfo(String productInfo) {
        this.productInfo = productInfo;
    }

    public String getServiceProvider() {
        return serviceProvider;
    }

    public void setServiceProvider(String serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZip_code() {
        return zip_code;
    }

    public void setZip_code(String zip_code) {
        this.zip_code = zip_code;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState_id() {
        return state_id;
    }

    public void setState_id(String state_id) {
        this.state_id = state_id;
    }

    public String getSurl() {
        return Surl;
    }

    public void setSurl(String Surl) {
        this.Surl = Surl;
    }

    public String getFurl() {
        return Furl;
    }

    public void setFurl(String Furl) {
        this.Furl = Furl;
    }
}
