package com.etoile.makeupbarr.model;

/**
 * Created by AndroidPC on 01-02-2017.
 */

public class DataForReview {

    /**
     * status : success
     * message : Review inserted successfully.
     */

    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
