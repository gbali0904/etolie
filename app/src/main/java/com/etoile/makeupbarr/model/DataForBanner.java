package com.etoile.makeupbarr.model;

import java.util.List;

/**
 * Created by admin on 12/27/2016.
 */
public class DataForBanner {


    /**
     * status : success
     * message : Home bannar images listing.
     * response : [{"banner_id":"7","image":"http://makeupbarr.com/Admin/../banner_images/506852-banner.png","link":"http://www.makeupbarr.com/","main_heading":"happy hours begin","sub_heading":"EYE - CATCHING COOL TONES"}]
     */

    private String status;
    private String message;
    private List<ResponseBean> response;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ResponseBean> getResponse() {
        return response;
    }

    public void setResponse(List<ResponseBean> response) {
        this.response = response;
    }

    public static class ResponseBean {
        /**
         * banner_id : 7
         * image : http://makeupbarr.com/Admin/../banner_images/506852-banner.png
         * link : http://www.makeupbarr.com/
         * main_heading : happy hours begin
         * sub_heading : EYE - CATCHING COOL TONES
         */

        private String banner_id;
        private String image;
        private String link;
        private String main_heading;
        private String sub_heading;

        public String getBanner_id() {
            return banner_id;
        }

        public void setBanner_id(String banner_id) {
            this.banner_id = banner_id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public String getMain_heading() {
            return main_heading;
        }

        public void setMain_heading(String main_heading) {
            this.main_heading = main_heading;
        }

        public String getSub_heading() {
            return sub_heading;
        }

        public void setSub_heading(String sub_heading) {
            this.sub_heading = sub_heading;
        }
    }
}
