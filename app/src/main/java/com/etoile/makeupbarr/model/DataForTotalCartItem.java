package com.etoile.makeupbarr.model;

/**
 * Created by AndroidPC on 10-01-2017.
 */

public class DataForTotalCartItem {


    /**
     * status : success
     * total_record : 2
     */

    private String status;
    private int total_record;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getTotal_record() {
        return total_record;
    }

    public void setTotal_record(int total_record) {
        this.total_record = total_record;
    }
}
