package com.etoile.makeupbarr.model;

/**
 * Created by admin on 1/2/2017.
 */

public class DataForDeleteCart {

    /**
     * status : success
     * message : Product has been successfully removed in your cart.
     */

    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

