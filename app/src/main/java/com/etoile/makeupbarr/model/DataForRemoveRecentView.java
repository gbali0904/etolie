package com.etoile.makeupbarr.model;

/**
 * Created by admin on 2/20/2017.
 */

public class DataForRemoveRecentView {


    /**
     * status : success
     * message : Recent View deleted
     */

    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
