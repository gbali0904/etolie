package com.etoile.makeupbarr.model;



/**
 * Created by AndroidPC on 21-12-2016.
 */

public class DataForLogin {

    private static String mInstance;
    /**
     * status : success
     * message : Login Successfully
     * response : {"uid":"35","fname":"gunjan","email":"gbali0904@gmail.com","phone":"1223456677","firebase_id":null}
     */

    private String status;
    private String message;
    private ResponseBean response;
    public static synchronized String getInstance() {
        return mInstance;
    }

    public static void setInstance(String instance){
        mInstance=instance;
    }
    public static void clearInstance(){
        mInstance=null;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public static class ResponseBean {
        /**
         * uid : 35
         * fname : gunjan
         * email : gbali0904@gmail.com
         * phone : 1223456677
         * firebase_id : null
         */

        private String uid;
        private String fname;
        private String email;
        private String phone;
        private Object firebase_id;

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getFname() {
            return fname;
        }

        public void setFname(String fname) {
            this.fname = fname;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public Object getFirebase_id() {
            return firebase_id;
        }

        public void setFirebase_id(Object firebase_id) {
            this.firebase_id = firebase_id;
        }
    }





   /* *//**
     * status : success
     * message : Login Successfully
     * response : {"uid":"35","fname":"gunjan","lname":"bali","email":"gbali0904@gmail.com","phone":"7696343553","address":"t-25, subhash nagar","firebase_id":null}
     *//*

    private String status;
    private String message;
    private ResponseBean response;

    public static synchronized String getInstance() {
        return mInstance;
    }

    public static void setInstance(String instance){
        mInstance=instance;
    }
    public static void clearInstance(){
        mInstance=null;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }


    public static class ResponseBean {
        *//**
         * uid : 35
         * fname : gunjan
         * lname : bali
         * email : gbali0904@gmail.com
         * phone : 7696343553
         * address : t-25, subhash nagar
         * firebase_id : null
         *//*

        private String uid;
        private String fname;
        private String lname;
        private String email;
        private String phone;
        private String address;
        private Object firebase_id;

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getFname() {
            return fname;
        }

        public void setFname(String fname) {
            this.fname = fname;
        }

        public String getLname() {
            return lname;
        }

        public void setLname(String lname) {
            this.lname = lname;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public Object getFirebase_id() {
            return firebase_id;
        }

        public void setFirebase_id(Object firebase_id) {
            this.firebase_id = firebase_id;
        }
    }*/
}
