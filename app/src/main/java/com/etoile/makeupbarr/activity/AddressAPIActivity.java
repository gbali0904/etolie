package com.etoile.makeupbarr.activity;

import android.util.Log;

import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.Utility.TAGS;
import com.etoile.makeupbarr.model.DataForAddress;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by AndroidPC on 09-01-2017.
 */

public class AddressAPIActivity {
    public static void addresstoserver(String uid, String pin, String myname, String address, String locality, String city, String state, String phone, String address_id) {
         AppController.getInstance().getRetrofitServices().address(uid,pin,myname,address,locality,city,state,phone,address_id).enqueue(new Callback<DataForAddress>() {
            @Override
            public void onResponse(Call<DataForAddress> call, Response<DataForAddress> response) {
                DataForAddress categoryjson = response.body();
                if(categoryjson.getStatus().equalsIgnoreCase("")){
                    DataForAddress.ResponseBean response1 = categoryjson.getResponse();
                    String jsonValueforaddress = AppController.getInstance().getGson().toJson(response1);
                    AppController.getInstance().getPreferenceEditor().putString(TAGS.USER_ADD, jsonValueforaddress).commit();
                    Log.e("mydata",categoryjson.getStatus());
                    // getActivity().finish();
                    AppController.getInstance().getPreferenceEditor().putString(TAGS.ADDRESSID_JSON_DATA, "").commit();
                }
            }

            @Override
            public void onFailure(Call<DataForAddress> call, Throwable t) {
                //Toast.makeText(getActivity(), "Server error can't download the categories", Toast.LENGTH_LONG).show();
              //  getActivity().finish();
            }
        });
    }
}
