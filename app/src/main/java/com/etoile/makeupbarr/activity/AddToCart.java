package com.etoile.makeupbarr.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.Utility.TAGS;
import com.etoile.makeupbarr.adapter.RecylerViewAdpterForViewCart;
import com.etoile.makeupbarr.login.AddressActivity;
import com.etoile.makeupbarr.login.EditProfileActivity;
import com.etoile.makeupbarr.login.LoginActivity;
import com.etoile.makeupbarr.model.DataForCurrentPassword;
import com.etoile.makeupbarr.model.DataForLogin;
import com.etoile.makeupbarr.model.DataForViewAddCart;
import com.facebook.login.LoginManager;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;

import java.util.ArrayList;
import java.util.List;

import berlin.volders.badger.BadgeShape;
import berlin.volders.badger.Badger;
import berlin.volders.badger.CountBadge;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.functions.Action1;

import static com.etoile.makeupbarr.R.id.expandabletext;

/**
 * Created by AndroidPC on 30-12-2016.
 */

public class AddToCart extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private static final String TAG = AddToCart.class.getSimpleName();
    private static OnUpdateListener mListener;
    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;
    @Bind(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @Bind(R.id.mycart_empty)
    LinearLayout cartEmpty;
    @Bind(R.id.cart_empty_action)
    AppCompatButton cartEmptyAction;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.checkout)
    AppCompatButton checkout;
    @Bind(R.id.total_items)
    TextView totalItems;
    @Bind(expandabletext)
    TextView expandableText;
    @Bind(R.id.expandableLayout)
    ExpandableRelativeLayout expandableLayout;
    @Bind(R.id.textforprice)
    TextView textforprice;
    @Bind(R.id.textforoffer)
    TextView textforoffer;
    @Bind(R.id.my_cart_total)
    LinearLayout myCartTotal;
    public static  String uid;
    private RecylerViewAdpterForViewCart recyclerViewAdapter;
    private List<DataForViewAddCart.ResponseBean> response1;
    GridLayoutManager mLayoutManager;
    private int COLUMN = 1;
    private String flagstring;
    private CountBadge.Factory circleFactory;
    private int total;
    private int total_price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_cart);
        ButterKnife.bind(this);
        toolbar = getToolbar();
        String json_string = AppController.getInstance().getPreference().getString(TAGS.JSON_USER_DATA, "");
        DataForLogin dataForLogin = AppController.getInstance().getGson().fromJson(json_string, DataForLogin.class);
        DataForLogin.ResponseBean response = dataForLogin.getResponse();
        uid = response.getUid();
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        bagerCount();
        if (bundle != null) {
            flagstring = bundle.getString(TAGS.FLAG);
        }
        String string = AppController.getInstance().getPreference().getString(TAGS.TOATAL_RECORD, "");
        int i = Integer.parseInt(string);

        Spanned spanned;
        if (i <= 1) {
            spanned = Html.fromHtml("<b>" + string + " </b>" + " ITEM");
        } else {
            spanned = Html.fromHtml("<b>" + string + " </b>" + " ITEMS");
        }

        totalItems.setText(spanned);
        AppController.getInstance().bus().toObserverable().subscribe(new Action1<Object>() {
            @Override
            public void call(Object o) {
                if (o instanceof String) {
                    int badgerValue = Integer.valueOf((String) o);
                    Spanned spanned = Html.fromHtml("<b>" + badgerValue + " </b>" + " Item Total");
                    totalItems.setText(spanned);
                }
            }
        });
        recyclerView.setHasFixedSize(true);
        getAddCartValues();
        mLayoutManager = new GridLayoutManager(this, COLUMN);
        mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (recyclerViewAdapter.getItemViewType(position)) {
                    case RecylerViewAdpterForViewCart.TYPE_HEADER:
                        return COLUMN;
                    case RecylerViewAdpterForViewCart.TYPE_ITEM:
                        return 1;
                    case RecylerViewAdpterForViewCart.TYPE_FOOTER:
                        return COLUMN;
                    default:
                        return -1;
                }
            }
        });
        RecylerViewAdpterForViewCart.setOnUpdateListener(new RecylerViewAdpterForViewCart.OnUpdateListener() {
            @Override
            public void onUpdate() {
                AppController.getInstance().bus().toObserverable().subscribe(new Action1<Object>() {
                    @Override
                    public void call(Object o) {
                        if (o instanceof String) {
                            int badgerValue = Integer.valueOf((String) o);
                            Spanned spanned = Html.fromHtml("<b>" + badgerValue + " </b>" + " Item Total");
                            totalItems.setText(spanned);
                        }
                    }
                });
                response1.clear();
                getAddCartValues();
                recyclerViewAdapter.notifyDataSetChanged();
            }
        });

    }

    private Toolbar getToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
        });
        return toolbar;
    }

    private void bagerCount() {
        circleFactory = new CountBadge.Factory(this, BadgeShape.circle(0.6f, Gravity.END | Gravity.TOP));
    }

    private void getAddCartValues() {

        AppController.getInstance().showProgressDialog(this);
        AppController.getInstance().getRetrofitServices().viewaddToCart(uid).enqueue(new Callback<DataForViewAddCart>() {
            @Override
            public void onResponse(Call<DataForViewAddCart> call, Response<DataForViewAddCart> response) {
                AppController.getInstance().hideProgressDialog();
                DataForViewAddCart jsonforcart = response.body();
                if (jsonforcart.getStatus().equalsIgnoreCase("success")) {
                    response1 = jsonforcart.getResponse();
                    if (response1.isEmpty()) {
                        cartEmpty.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                        checkout.setVisibility(View.GONE);
                        myCartTotal.setVisibility(View.GONE);

                        expandableText.setText("OFFER PRICE :\t\t");
                        textforoffer.setText("");
                        textforprice.setText("");

                    } else {

                        for (DataForViewAddCart.ResponseBean item : response1) {
                             total_price = Integer.parseInt(item.getTotal_price());
                            total =total+ total_price;

                        }
                        final String decimalpricetotal = AppController.getInstance().decimalValue(total);
                        Spanned spanned = Html.fromHtml("OFFER PRICE :\t\t" + "<b> INR " + decimalpricetotal + "</b>");
                        expandableText.setText(spanned);

                        textforoffer.setText(" INR " + decimalpricetotal);
                        textforprice.setText(" INR " + decimalpricetotal);
                        expandableText.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                expandableMethod(expandableLayout);
                            }
                        });

                        Log.e("this","total value"+total_price+"\ntotal price"+total);
                        recyclerView.setLayoutManager(mLayoutManager);
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        recyclerViewAdapter = new RecylerViewAdpterForViewCart(AddToCart.this, response1, coordinatorLayout, checkout);
                        recyclerView.setAdapter(recyclerViewAdapter);
                    }
                } else {
                    Toast.makeText(AddToCart.this, jsonforcart.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<DataForViewAddCart> call, Throwable t) {
                AppController.getInstance().hideProgressDialog();
                Toast.makeText(AddToCart.this, "Server error can't download the categories", Toast.LENGTH_LONG).show();
                finish();
            }
        });
    }

    private void expandableMethod(ExpandableRelativeLayout expandableLayout) {
        expandableLayout.toggle();

    }

    @OnClick({R.id.cart_empty_action})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cart_empty_action:
                Intent intent = new Intent(AddToCart.this, MainActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (flagstring.equalsIgnoreCase("flase")) {
            if (mListener != null) {
                mListener.onUpdate();
            }
        } else {
            this.finish();
        }
    }

    public static void setOnUpdateListener(OnUpdateListener listener) {

        mListener = listener;
    }

    public interface OnUpdateListener {
        void onUpdate();
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_productlist, menu);
/*        MenuItem searchItem = menu.findItem(R.id.action_search);
        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(this.SEARCH_SERVICE);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        int searchImgId = android.support.v7.appcompat.R.id.search_button;
        ImageView v = (ImageView) searchView.findViewById(searchImgId);
        v.setImageResource(R.drawable.ic_search_view);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(true); // Iconify the widget
        searchView.setOnQueryTextListener(this);*/

        if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
            String s1 = AppController.getInstance().getPreference().getString(TAGS.TOATAL_RECORD, "");
            int i1 = Integer.parseInt(s1);
            Badger.sett(menu.findItem(R.id.action_cart), circleFactory).setCount(i1);
            AppController.getInstance().bus().toObserverable().subscribe(new Action1<Object>() {
                @Override
                public void call(Object o) {
                    if (o instanceof String) {
                        //  int badgerValue = (int) o;
                        int badgerValue = Integer.valueOf((String) o);
                        Badger.sett(menu.findItem(R.id.action_cart), circleFactory).setCount(badgerValue);
                        Log.d(TAG, "new badger value " + badgerValue);
                    }
                }
            });
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (item.getItemId()) {
            case R.id.action_cart: {
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
                    Intent intent1 = new Intent(AddToCart.this, AddToCart.class);
                    intent1.putExtra(TAGS.FLAG, "true");
                    startActivity(intent1);
                    this.finish();
                } else {
                    Intent i = new Intent(AddToCart.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            }
            break;
        /*    case R.id.action_order1:
            {

            }
            return true;*/
            case R.id.action_wishlist: {
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {

                    Intent intent1 = new Intent(AddToCart.this, WishListActivity.class);
                    startActivity(intent1);
                } else {
                    Intent i = new Intent(AddToCart.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            }
            break;
            case R.id.action_profile: {
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
                    Intent intent1 = new Intent(AddToCart.this, EditProfileActivity.class);
                    startActivity(intent1);
                } else {
                    Intent i = new Intent(AddToCart.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            }

            break;

            case R.id.action_myAddress:
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
                Intent intent4 = new Intent(AddToCart.this, AddressActivity.class);
                intent4.putExtra(TAGS.FLAG, "false");
                startActivity(intent4);
                }else {
                    Intent i = new Intent(AddToCart.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }

                break;
            case R.id.action_changePass:
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
                    changePassword();
                }else {
                Intent i = new Intent(AddToCart.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
                break;
            case R.id.action_logout: {
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
                    LoginManager.getInstance().logOut();
                    AppController.getInstance().getPreferenceEditor().putString(TAGS.JSON_USER_DATA, "").commit();
                    Intent intent2 = new Intent(AddToCart.this, LoginActivity.class);
                    startActivity(intent2);
                    finish();
                } else {
                    Intent i = new Intent(AddToCart.this, AddToCart.class);
                    startActivity(i);
                    finish();
                }
            }

            break;
            default:
                if (id == R.id.ic_dots) {
//do nothing
                } else {
                    onBackPressed();
                }


        }
        return super.onOptionsItemSelected(item);
    }

    private void changePassword() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
// ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.password_activity, null);
        dialogBuilder.setView(dialogView);

        final EditText currentpass = (EditText) dialogView.findViewById(R.id.currentPassword);
        final EditText changepass = (EditText) dialogView.findViewById(R.id.ChangePassword);
        AppCompatButton btnchangepass = (AppCompatButton) dialogView.findViewById(R.id.btn_chng);


        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.setTitle("Change Password");
        alertDialog.show();
        btnchangepass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation(changepass)) {
                    String currentpassword = currentpass.getText().toString();
                    String changedpassword = changepass.getText().toString();
                    AppController.getInstance().getRetrofitServices().changepassword(uid, currentpassword, changedpassword).enqueue(new Callback<DataForCurrentPassword>() {
                        @Override
                        public void onResponse(Call<DataForCurrentPassword> call, Response<DataForCurrentPassword> response) {
                            DataForCurrentPassword dataForCurrentPassword = response.body();
                            if (dataForCurrentPassword.getStatus().equalsIgnoreCase("success")) {
                                Snackbar snackbar = Snackbar.make(coordinatorLayout, dataForCurrentPassword.getMessage(), Snackbar.LENGTH_LONG);
                                snackbar.show();
                                alertDialog.dismiss();
                            } else {
                                Snackbar snackbar = Snackbar.make(coordinatorLayout, dataForCurrentPassword.getMessage(), Snackbar.LENGTH_LONG);
                                snackbar.show();
                                alertDialog.dismiss();
                            }

                        }

                        @Override
                        public void onFailure(Call<DataForCurrentPassword> call, Throwable t) {
                            Toast.makeText(AddToCart.this, "Server error can't download the categories", Toast.LENGTH_LONG).show();
                            finish();
                        }
                    });
                }
            }
        });




/* */
    }

    private boolean validation(EditText changepass) {
        boolean validation = true;
        String password = changepass.getText().toString();
        if (TextUtils.isEmpty(password)) {
            Snackbar snackbar = Snackbar.make(coordinatorLayout, getResources().getString(R.string.error_empty_password), Snackbar.LENGTH_LONG);
            View view = snackbar.getView();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            snackbar.show();
            return false;
        }
//if no error found
        return validation;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }

    private List<DataForViewAddCart.ResponseBean> filter(List<DataForViewAddCart.ResponseBean> models, String query) {
        query = query.toLowerCase();
        final List<DataForViewAddCart.ResponseBean> filteredModelList = new ArrayList<>();
        filteredModelList.clear();
        Log.d(TAG, "querry String: " + query);
        for (DataForViewAddCart.ResponseBean model : models) {
            final String text = model.getProduct_name().toLowerCase();


            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        final List<DataForViewAddCart.ResponseBean> filteredModelList = filter(response1, query);
        recyclerViewAdapter.setFilter(filteredModelList);
        return true;
    }
}
