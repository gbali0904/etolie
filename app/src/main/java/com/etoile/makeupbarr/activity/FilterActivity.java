package com.etoile.makeupbarr.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.Utility.TAGS;
import com.etoile.makeupbarr.model.DataForDrawerCategories;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/*
*
 * Created by AndroidPC on 04-01-2017.
*/



public class FilterActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {


    @Bind(R.id.category)
    Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.filter_fragment);
        ButterKnife.bind(this);


        setCategoryData();
    }

    private void setCategoryData() {
        String userdata = AppController.getInstance().getPreference().getString(TAGS.JSON_CATEGORY, "");
        DataForDrawerCategories dataForCategory = AppController.getInstance().getGson().fromJson(userdata, DataForDrawerCategories.class);
        List<DataForDrawerCategories.ResponseBean> category = dataForCategory.getResponse();
        List<DataForDrawerCategories.BrandBean> brand = dataForCategory.getBrand();


        List<String> cat=new ArrayList<>();
        for(DataForDrawerCategories.ResponseBean responseBean:category)
        {
            String category_name = responseBean.getCategory_name();
            cat.add(category_name);
        }
        // Spinner click listener
        spinner.setOnItemSelectedListener(this);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, cat);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);

    }



    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
        String item = parent.getItemAtPosition(position).toString();

        // Showing selected spinner item
        Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
    }
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }
}
