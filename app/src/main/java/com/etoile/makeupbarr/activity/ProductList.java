package com.etoile.makeupbarr.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.Utility.TAGS;
import com.etoile.makeupbarr.adapter.ArrayAdapterWithIcon;
import com.etoile.makeupbarr.adapter.RecyclerViewAdapterForProductList;
import com.etoile.makeupbarr.login.AddressActivity;
import com.etoile.makeupbarr.login.EditProfileActivity;
import com.etoile.makeupbarr.login.LoginActivity;
import com.etoile.makeupbarr.model.DataForCurrentPassword;
import com.etoile.makeupbarr.model.DataForDrawerCategories;
import com.etoile.makeupbarr.model.DataForLogin;
import com.etoile.makeupbarr.model.DataForProductList;
import com.facebook.login.LoginManager;
import com.yahoo.mobile.client.android.util.rangeseekbar.RangeSeekBar;

import java.util.ArrayList;
import java.util.List;

import berlin.volders.badger.BadgeShape;
import berlin.volders.badger.Badger;
import berlin.volders.badger.CountBadge;
import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.functions.Action1;

/**
 * Created by admin on 12/9/2016.
 */

public class ProductList extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private static final String TAG = ProductList.class.getSimpleName();
    private static OnUpdateListener mListener;
    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;
    @Bind(R.id.co_lay)
    CoordinatorLayout coLay;
    @Bind(R.id.nodata)
    TextView nodata;
    @Bind(R.id.list_empty)
    LinearLayout listEmpty;
    @Bind(R.id.texttoatal)
    public
    TextView texttoatal;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.text_catogry)
    TextView textCatogry;
    @Bind(R.id.button_filter)
    AppCompatButton buttonFilter;
    @Bind(R.id.button_sort)
    AppCompatButton buttonSort;
  /*  AHBottomNavigationAdapter navigationAdapter;*/

    private List<DataForProductList> userlist = new ArrayList<>();
    private RecyclerViewAdapterForProductList recyclerViewAdapter;
    private String short_list = "";
    private String category_id = "";
    private String branch_id = "";
    private String min_price = "";
    private String max_price = "";
    private String brand_id = "";
    private String min_value = "";
    private String max_value = "";
    private List<DataForProductList.ResponseBean> response1;
    private DataForLogin dataForLogin;
    private DataForLogin.ResponseBean response;
    private String uid;
    private CountBadge.Factory circleFactory;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_fragment_product);
        ButterKnife.bind(this);
        bottonNavigationMethod();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        bagerCount();
        if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
            String userdata = AppController.getInstance().getPreference().getString(TAGS.JSON_USER_DATA, "");
            dataForLogin = AppController.getInstance().getGson().fromJson(userdata, DataForLogin.class);
            response = dataForLogin.getResponse();
            uid = response.getUid();
        }

        String categoryName = AppController.getInstance().getPreference().getString(TAGS.CATEGORY_NAME, "");
        textCatogry.setText(categoryName);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(ProductList.this, 2));
        getProductList();
        ProductList.setOnUpdateListener(new OnUpdateListener() {
            @Override
            public void onUpdate() {
                // Toast.makeText(EditProfileActivity.this, "Successfully  Closed\u200E", Toast.LENGTH_SHORT).show();
                response1.clear();
                getProductList();
                recyclerViewAdapter.notifyDataSetChanged();
            }
        });
        RecyclerViewAdapterForProductList.setOnUpdateListener(new RecyclerViewAdapterForProductList.OnUpdateListener() {
            @Override
            public void onUpdate() {
                // Toast.makeText(EditProfileActivity.this, "Successfully  Closed\u200E", Toast.LENGTH_SHORT).show();
                //      response1.clear();
                response1.clear();
                getProductList();
                recyclerViewAdapter.notifyDataSetChanged();
            }
        });
        ProductDetailActivity.setOnUpdateListener(new ProductDetailActivity.OnUpdateListener() {
            @Override
            public void onUpdate() {

                response1.clear();
                getProductList();
                recyclerViewAdapter.notifyDataSetChanged();
            }
        });
    }

    private void getProductList() {
        AppController.getInstance().showProgressDialog(this);
        String category_id_json = AppController.getInstance().getPreference().getString(TAGS.CATEGORY_ID, "");
        String sub_category_id = AppController.getInstance().getPreference().getString(TAGS.SUB_CATEGORY_ID, "");
        brand_id = AppController.getInstance().getPreference().getString(TAGS.BRAND_ID, "");
      //  short_list = AppController.getInstance().getPreference().getString(TAGS.SHORTEDLIST, "");
        Call<DataForProductList> mService = AppController.getInstance().getRetrofitServices().
                productlist(category_id_json, sub_category_id, min_price, max_price, brand_id, short_list, uid, "");//last vale new_in_week
        mService.enqueue(new Callback<DataForProductList>() {
            @Override
            public void onResponse(Call<DataForProductList> call, Response<DataForProductList> response) {
                AppController.getInstance().hideProgressDialog();
                DataForProductList loginJsonData = response.body();
                String jsonValueforProductList = AppController.getInstance().getGson().toJson(loginJsonData);
                Log.e(TAG, jsonValueforProductList);
                if (loginJsonData.getStatus().equalsIgnoreCase("success")) {

                    Log.d(TAG, loginJsonData.getMessage());
                    response1 = loginJsonData.getResponse();
                    if (response1.isEmpty()) {
                        texttoatal.setVisibility(View.GONE);
                        listEmpty.setVisibility(View.VISIBLE);

                    } else {
                        recyclerViewAdapter = new RecyclerViewAdapterForProductList(ProductList.this, response1, coLay);
                        recyclerView.setAdapter(recyclerViewAdapter);
                    }

                } else {
                    Snackbar snackbar = Snackbar.make(coLay, loginJsonData.getMessage(), Snackbar.LENGTH_LONG);
                    snackbar.show();

                }
                AppController.getInstance().getPreferenceEditor().putString(TAGS.BRAND_ID, "").commit();
                AppController.getInstance().getPreferenceEditor().putString(TAGS.SHORTEDLIST, "").commit();
            }

            @Override
            public void onFailure(Call<DataForProductList> call, Throwable t) {
                call.cancel();
                Toast.makeText(ProductList.this, "Please check your network connection and internet permission", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void bagerCount() {
        circleFactory = new CountBadge.Factory(this, BadgeShape.circle(0.6f, Gravity.END | Gravity.TOP));
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_product, menu);


        if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {

            MenuItem item = menu.findItem(R.id.action_logout);
            item.setTitle("Logout");

            String s1 = AppController.getInstance().getPreference().getString(TAGS.TOATAL_RECORD, "");
            int i1 = Integer.parseInt(s1);
            Badger.sett(menu.findItem(R.id.action_cart), circleFactory).setCount(i1);
            AppController.getInstance().bus().toObserverable().subscribe(new Action1<Object>() {
                @Override
                public void call(Object o) {
                    if (o instanceof String) {
                        //  int badgerValue = (int) o;
                        int badgerValue = Integer.valueOf((String) o);
                        Badger.sett(menu.findItem(R.id.action_cart), circleFactory).setCount(badgerValue);
                        Log.d(TAG, "new badger value " + badgerValue);
                    }
                }
            });
        }else {
            MenuItem item = menu.findItem(R.id.action_logout);
            item.setTitle("Login");
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch (item.getItemId()) {
            case R.id.action_search:
                Intent intent = new Intent(ProductList.this, SearchViewActivity.class);
                intent.putExtra(TAGS.FLAG,"false");
                startActivity(intent);
                return true;
            case R.id.action_cart: {
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
                    Intent intent1 = new Intent(ProductList.this, AddToCart.class);
                    intent1.putExtra(TAGS.FLAG, "true");
                    startActivity(intent1);
                } else {
                    Intent i = new Intent(ProductList.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            }
            break;
        /*    case R.id.action_order1:
            {

            }
            return true;*/


            case R.id.action_wishlist: {
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {

                    Intent intent1 = new Intent(ProductList.this, WishListActivity.class);
                    startActivity(intent1);
                } else {
                    Intent i = new Intent(ProductList.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            }
            break;
            case R.id.action_profile: {
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
                    Intent intent1 = new Intent(ProductList.this, EditProfileActivity.class);
                    startActivity(intent1);
                } else {
                    Intent i = new Intent(ProductList.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            }

            break;

            case R.id.action_myAddress:
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {

                    Intent intent4 = new Intent(ProductList.this, AddressActivity.class);
                    intent4.putExtra(TAGS.FLAG, "false");
                    startActivity(intent4);
                }else {
                    Intent i = new Intent(ProductList.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                break;
            case R.id.action_changePass:
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
                    changePassword1();
                }
                else {
                Intent i = new Intent(ProductList.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
                break;

            case R.id.action_logout: {
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
                    LoginManager.getInstance().logOut();
                    AppController.getInstance().getPreferenceEditor().putString(TAGS.JSON_USER_DATA, "").commit();
                    Intent intent2 = new Intent(ProductList.this, LoginActivity.class);
                    startActivity(intent2);
                    finish();
                }else {
                    Intent i = new Intent(ProductList.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }

            }

            break;
            default:
                if(id==R.id.ic_dots)
                {
//do nothing
                }
                else {
                    onBackPressed();
                }


        }
        return super.onOptionsItemSelected(item);
    }

    private void changePassword1() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ProductList.this);
// ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.password_activity, null);
        dialogBuilder.setView(dialogView);

        final EditText currentpass = (EditText) dialogView.findViewById(R.id.currentPassword);
        final EditText changepass = (EditText) dialogView.findViewById(R.id.ChangePassword);
        AppCompatButton btnchangepass = (AppCompatButton) dialogView.findViewById(R.id.btn_chng);


        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.setTitle("Change Password");
        alertDialog.show();
        btnchangepass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation(changepass)) {
                    String currentpassword = currentpass.getText().toString();
                    String changedpassword = changepass.getText().toString();
                    AppController.getInstance().getRetrofitServices().changepassword(uid, currentpassword, changedpassword).enqueue(new Callback<DataForCurrentPassword>() {
                        @Override
                        public void onResponse(Call<DataForCurrentPassword> call, Response<DataForCurrentPassword> response) {
                            DataForCurrentPassword dataForCurrentPassword = response.body();
                            if (dataForCurrentPassword.getStatus().equalsIgnoreCase("success")) {
                                Snackbar snackbar = Snackbar.make(coLay, dataForCurrentPassword.getMessage(), Snackbar.LENGTH_LONG);
                                snackbar.show();
                                alertDialog.dismiss();
                            } else {
                                Snackbar snackbar = Snackbar.make(coLay, dataForCurrentPassword.getMessage(), Snackbar.LENGTH_LONG);
                                snackbar.show();
                                alertDialog.dismiss();
                            }

                        }

                        @Override
                        public void onFailure(Call<DataForCurrentPassword> call, Throwable t) {
                            Toast.makeText(ProductList.this, "Server error can't download the categories", Toast.LENGTH_LONG).show();
                            finish();
                        }
                    });
                }
            }
        });

    }

    private boolean validation(EditText changepass) {
        boolean validation = true;
        String password = changepass.getText().toString();
        if (TextUtils.isEmpty(password)) {
            Snackbar snackbar = Snackbar.make(coLay, getResources().getString(R.string.error_empty_password), Snackbar.LENGTH_LONG);
            View view = snackbar.getView();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            snackbar.show();
            return false;
        }
//if no error found
        return validation;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(ProductList.this, MainActivity.class);
        startActivity(intent);
        finish();

    }

    private void bottonNavigationMethod() {

        buttonFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getfilterList();
            }
        });
        buttonSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getShortedList();
            }
        });
    }

    private void getShortedList() {
        // final CharSequence[] items = {" High To Low "," Low To High "," Latest "};
        //  final Integer[] images = {R.id.icon_only,R.id.icon_only,R.id.icon_only};

        // arraylist to keep the selected items
        final ArrayList seletedItems = new ArrayList();
        final String[] item = new String[]{"Popularity", "Price Low to High ", "Price High to Low ", "Newest First", "Discount"};
        final Integer[] icons = new Integer[]{R.drawable.ic_ascendant_sort, R.drawable.ic_sort_sort, R.drawable.ic_sort_sort, R.drawable.ic_badge, R.drawable.ic_discount_dis};

        ListAdapter adapter = new ArrayAdapterWithIcon(this, item, icons);

        new AlertDialog.Builder(this)
                .setAdapter(adapter, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int position) {
                        if (position == 0) {
                            short_list = "Popularity";
                        }
                        if (position == 1) {
                            short_list = "price-desc";
                        }
                        if (position == 2) {
                            short_list = "price-asc";
                        }
                        if (position == 3) {
                            short_list = "latest";
                        }
                        if (position == 4) {
                            short_list = "discount";
                        }
                        if (mListener != null) {
                            mListener.onUpdate();
                        }
                    }
                }).show();
    }

    private void getfilterList() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setCancelable(false);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.filter_fragment, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = dialogBuilder.create();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(alertDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        final Spinner spinnercategory = (Spinner) dialogView.findViewById(R.id.category);
        Spinner spinnerbrand = (Spinner) dialogView.findViewById(R.id.brand);
        final RangeSeekBar crystalRangeSeekbar = (RangeSeekBar) dialogView.findViewById(R.id.rangeSeekbar);
        crystalRangeSeekbar.setSelectedMaxValue(2999);
        crystalRangeSeekbar.setSelectedMinValue(1);
        crystalRangeSeekbar.setRangeValues(1, 30000);

        final TextView minvalue = (TextView) dialogView.findViewById(R.id.min);
        final TextView maxvalue = (TextView) dialogView.findViewById(R.id.max);
        crystalRangeSeekbar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar bar, Object minValue, Object maxValue) {
                minvalue.setText(String.valueOf(minValue));
                maxvalue.setText(String.valueOf(maxValue));

                min_value = String.valueOf(minValue);
                max_value = String.valueOf(maxValue);

            }
        });


        String userdata = AppController.getInstance().getPreference().getString(TAGS.JSON_CATEGORY, "");
        DataForDrawerCategories dataForCategory = AppController.getInstance().getGson().fromJson(userdata, DataForDrawerCategories.class);
        final List<DataForDrawerCategories.ResponseBean> category = dataForCategory.getResponse();
        final List<DataForDrawerCategories.BrandBean> brand = dataForCategory.getBrand();
        //for category
        final List<String> categorylist = new ArrayList<>();
        categorylist.add(0, "All");
        for (DataForDrawerCategories.ResponseBean responseBean : category) {
            String category_name = responseBean.getCategory_name();
            categorylist.add(category_name);
        }
        //   ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categorylist);

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getSupportActionBar().getThemedContext(),
                R.layout.activityspinner,
                categorylist);
        dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        // dataAdapter.add("Select your Category!");
        //     spinnercategory.setDropDownHorizontalOffset(android.R.layout.browser_link_context_header);
        spinnercategory.setAdapter(dataAdapter);


        spinnercategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (spinnercategory.getSelectedItem() == "All") {
                    category_id = "";
                } else {
                    DataForDrawerCategories.ResponseBean responseBean = category.get(position - 1);
                    category_id = responseBean.getCategory_id();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //for brand

        final List<String> brandlist = new ArrayList<>();

        brandlist.add(0, "All");
        for (DataForDrawerCategories.BrandBean brandBean : brand) {
            String category_name = brandBean.getBrand_name();
            brandlist.add(category_name);
        }
      /*  ArrayAdapter<String> dataAdapterforBrand = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, brandlist);
        spinnerbrand.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
*/
        ArrayAdapter<String> dataAdapterforBrand = new ArrayAdapter<String>(getSupportActionBar().getThemedContext(),
                R.layout.activityspinner,
                brandlist);
        dataAdapterforBrand.setDropDownViewResource(R.layout.spinner_dropdown_item);
        // dataAdapter.add("Select your Category!");
        spinnerbrand.setAdapter(dataAdapterforBrand);
        spinnerbrand.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                // Showing selected spinner item
                //  Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();

                if (item.equals("All")) {
                    branch_id = "";
                } else {
                    DataForDrawerCategories.BrandBean brandBean = brand.get(position - 1);
                    branch_id = brandBean.getBrand_id();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        AppCompatButton reset = (AppCompatButton) dialogView.findViewById(R.id.reset);
        AppCompatButton cancel = (AppCompatButton) dialogView.findViewById(R.id.cancel);

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (minvalue == null) {
                    min_price = "";
                }
                if (max_value == null) {
                    max_price = "";
                } else {

                    min_price = "" + min_value;
                    max_price = "" + max_value;
                }
                brand_id = branch_id;
                String category_id_json = category_id;

                AppController.getInstance().getPreferenceEditor().putString(TAGS.CATEGORY_ID, category_id_json).commit();
                AppController.getInstance().getPreferenceEditor().putString(TAGS.SUB_CATEGORY_ID, "").commit();
                Log.e(TAG, "minvalue" + min_price + "\nmaxvalue" + max_price + "\nbrand" + branch_id + "\ncategoryid" + category_id);
                alertDialog.dismiss();
                if (mListener != null) {
                    mListener.onUpdate();
                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
        alertDialog.getWindow().setAttributes(lp);

    }

    public static void setOnUpdateListener(OnUpdateListener listener) {
        mListener = listener;
    }

    interface OnUpdateListener {
        void onUpdate();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }

    private List<DataForProductList.ResponseBean> filter(List<DataForProductList.ResponseBean> models, String query) {
        query = query.toLowerCase();
        final List<DataForProductList.ResponseBean> filteredModelList = new ArrayList<>();
        filteredModelList.clear();
        Log.d(TAG, "querry String: " + query);
        for (DataForProductList.ResponseBean model : models) {
            final String text = model.getProduct_name().toLowerCase();


            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        final List<DataForProductList.ResponseBean> filteredModelList = filter(response1, query);
        recyclerViewAdapter.setFilter(filteredModelList);
        return true;
    }

}
