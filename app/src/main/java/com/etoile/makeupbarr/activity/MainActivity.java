package com.etoile.makeupbarr.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.appeaser.sublimenavigationviewlibrary.OnNavigationMenuEventListener;
import com.appeaser.sublimenavigationviewlibrary.SublimeBaseMenuItem;
import com.appeaser.sublimenavigationviewlibrary.SublimeGroup;
import com.appeaser.sublimenavigationviewlibrary.SublimeMenu;
import com.appeaser.sublimenavigationviewlibrary.SublimeNavigationView;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationAdapter;
import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.Utility.TAGS;
import com.etoile.makeupbarr.adapter.RecentViews_Adapter;
import com.etoile.makeupbarr.adapter.RecylerViewAdapterForMainActivity;
import com.etoile.makeupbarr.login.AddressActivity;
import com.etoile.makeupbarr.login.EditProfileActivity;
import com.etoile.makeupbarr.login.LoginActivity;
import com.etoile.makeupbarr.model.DataForCurrentPassword;
import com.etoile.makeupbarr.model.DataForDrawerCategories;
import com.etoile.makeupbarr.model.DataForLogin;
import com.etoile.makeupbarr.model.DataForProductList;
import com.etoile.makeupbarr.model.DataForTotalCartItem;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import berlin.volders.badger.BadgeShape;
import berlin.volders.badger.Badger;
import berlin.volders.badger.CountBadge;
import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.functions.Action1;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String TAG = MainActivity.class.getSimpleName();
    private static final int CATEGORY_PRODUCT = -1;
    private static final int CATEGORY_WHATSNEW = -2;
    private static final int CATEGORY_BEST_SELLER = -3;
    final Set<Target> targets = new HashSet<>();
    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;
    @Bind(R.id.navigation_view)
    SublimeNavigationView sublimeNavigationView;
    @Bind(R.id.bottom_navigation)
    AHBottomNavigation bottomNavigation;
    @Bind(R.id.co_lay)
    CoordinatorLayout coLay;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    RecylerViewAdapterForMainActivity recyclerViewAdapter;
    GridLayoutManager mLayoutManager;
    private int COLUMN = 2;
    private DrawerLayout drawer;
    private FragmentManager fm;
    private FragmentTransaction ft;
    private CircleImageView imProfile;
    private ActionBarDrawerToggle toggle;
    private AHBottomNavigationAdapter navigationAdapter;
    private SublimeGroup group;
    private ArrayList<SublimeBaseMenuItem> navMenuList;
    private DataForDrawerCategories.ResponseBean responseBean;
    private int array_list_for_menuItem;
    private SublimeBaseMenuItem sublimeBaseMenuItem1;
    private List<DataForDrawerCategories.ResponseBean.SubCategoryBean> sub_category;
    private HashMap<SublimeBaseMenuItem, MenuIndexes> menuAndIndexList;
    private String totalcartNo = "";
    private boolean doubleBackToExitPressedOnce = false;
    private List<DataForProductList.ResponseBean> responseDataForProductList;
    private int i1;
    private SublimeBaseMenuItem sublimeBaseMenuItem;
    private Bitmap bitMapimage;
    private String uid;
    private CountBadge.Factory circleFactory;
    private String name = "";
    private String email = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        FacebookSdk.sdkInitialize(getApplicationContext());

        if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
            String string = AppController.getInstance().getPreference().getString(TAGS.JSON_USER_DATA, "");
            DataForLogin dataForLogin = AppController.getInstance().getGson().fromJson(string, DataForLogin.class);
            DataForLogin.ResponseBean response = dataForLogin.getResponse();
            uid = response.getUid();
            name = response.getFname();
            email = response.getEmail();
        }
        final Toolbar toolbar = getToolbar();
        getTotalCartNo();
        circleFactory = new CountBadge.Factory(this, BadgeShape.circle(0.6f, Gravity.END | Gravity.TOP));


        bottonNavigationMethod();
        try {
            showDynamicMenuFrom(sublimeNavigationView.getMenu(), DataForDrawerCategories.getInstance().getResponse(), toolbar);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        recyclerView.setHasFixedSize(true);
        recyclerViewAdpterMethod();
        mLayoutManager = new GridLayoutManager(this, COLUMN);
        mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (recyclerViewAdapter.getItemViewType(position)) {
                    case RecylerViewAdapterForMainActivity.TYPE_HEADER:
                        return COLUMN;
                    case RecylerViewAdapterForMainActivity.TYPE_ITEM:
                        return 1;
                    case RecylerViewAdapterForMainActivity.TYPE_FOOTER:
                        return COLUMN;
                    default:
                        return -1;
                }
            }
        });

        RecylerViewAdapterForMainActivity.setOnUpdateListener(new RecylerViewAdapterForMainActivity.OnUpdateListener() {
            @Override
            public void onUpdate() {
                responseDataForProductList.clear();
                recyclerViewAdpterMethod();
                recyclerViewAdapter.notifyDataSetChanged();
            }
        });
        RecentViews_Adapter.setOnUpdateListener(new RecentViews_Adapter.OnUpdateListener() {
            @Override
            public void onUpdate() {
                responseDataForProductList.clear();
                recyclerViewAdpterMethod();
                recyclerViewAdapter.notifyDataSetChanged();
            }
        });

    }

    private void recyclerViewAdpterMethod() {

        AppController.getInstance().showProgressDialog(this);
        if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
            AppController.getInstance().getRetrofitServices().productlist("", "", "", "", "", "", uid, "new_in_week").enqueue(new Callback<DataForProductList>() {
                @Override
                public void onResponse(Call<DataForProductList> call, Response<DataForProductList> response) {
                    AppController.getInstance().hideProgressDialog();
                    DataForProductList loginJsonData = response.body();
                    String jsonValueforProductList = AppController.getInstance().getGson().toJson(loginJsonData);
                    Log.e(TAG, jsonValueforProductList);
                    if (loginJsonData.getStatus().equals("success")) {
                        Log.d(TAG, loginJsonData.getMessage());
                        responseDataForProductList = loginJsonData.getResponse();
                        recyclerView.setLayoutManager(mLayoutManager);
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        recyclerViewAdapter = new RecylerViewAdapterForMainActivity(MainActivity.this, responseDataForProductList, coLay);
                        recyclerView.setAdapter(recyclerViewAdapter);
                    } else {
                    }
                }

                @Override
                public void onFailure(Call<DataForProductList> call, Throwable t) {
                    AppController.getInstance().hideProgressDialog();
                }
            });
        } else {
            AppController.getInstance().getRetrofitServices().productlist("", "", "", "", "", "", "", "new_in_week").enqueue(new Callback<DataForProductList>() {
                @Override
                public void onResponse(Call<DataForProductList> call, Response<DataForProductList> response) {
                    AppController.getInstance().hideProgressDialog();
                    DataForProductList loginJsonData = response.body();
                    String jsonValueforProductList = AppController.getInstance().getGson().toJson(loginJsonData);
                    Log.e(TAG, jsonValueforProductList);
                    if (loginJsonData.getStatus().equals("success")) {
                        Log.d(TAG, loginJsonData.getMessage());
                        responseDataForProductList = loginJsonData.getResponse();
                        recyclerView.setLayoutManager(mLayoutManager);
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        recyclerViewAdapter = new RecylerViewAdapterForMainActivity(MainActivity.this, responseDataForProductList, coLay);
                        recyclerView.setAdapter(recyclerViewAdapter);
                    } else {
                    }
                }

                @Override
                public void onFailure(Call<DataForProductList> call, Throwable t) {

                }
            });

        }
    }

    private void getTotalCartNo() {
        if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
            AppController.getInstance().getRetrofitServices().carttotal(uid).enqueue(new Callback<DataForTotalCartItem>() {
                @Override
                public void onResponse(Call<DataForTotalCartItem> call, Response<DataForTotalCartItem> response) {
                    DataForTotalCartItem jsonforTotalCart = response.body();
                    totalcartNo = "" + jsonforTotalCart.getTotal_record();
                    //update cart bus with integer value
                    AppController.getInstance().bus().send(totalcartNo);
                    AppController.getInstance().getPreferenceEditor().putString(TAGS.TOATAL_RECORD, totalcartNo).commit();

                }

                @Override
                public void onFailure(Call<DataForTotalCartItem> call, Throwable t) {
                }
            });

        }
    }

    private void showDynamicMenuFrom(final SublimeMenu menu, final List<DataForDrawerCategories.ResponseBean> response, Toolbar toolbar) throws MalformedURLException {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        TextView user_name = (TextView) sublimeNavigationView.getHeaderView().findViewById(R.id.editProfileName);
        TextView user_email = (TextView) sublimeNavigationView.getHeaderView().findViewById(R.id.editProfileEmail);
        user_name.setText(name);
        user_email.setText(email);

        CoordinatorLayout coLay = (CoordinatorLayout) findViewById(R.id.co_lay);
        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close) {

        };
        if (drawer != null) {
            drawer.setScrimColor(Color.TRANSPARENT);
            drawer.setDrawerListener(toggle);
        }
        menuAndIndexList = new HashMap<>();
        menuAndIndexList.clear();

        //   drawer.setStatusBarBackground(R.drawable.dialog_bg);

        final SublimeGroup groupbase = menu.addGroup(true, false, true, true, SublimeGroup.CheckableBehavior.NONE);
        SublimeBaseMenuItem product = menu.addTextItem(groupbase.getGroupId(), "Product", null, false).setIcon(R.drawable.ic_box);
        menuAndIndexList.put(product, new MenuIndexes(CATEGORY_PRODUCT, -1));

        menu.addSeparatorItem(product.getGroupId()).setIcon(R.color.gray);
        if (response != null) {// if json has categories
            int[] integers = new int[]{R.drawable.bronzer, R.drawable.eye, R.drawable.mirror, R.drawable.eye_shadow, R.drawable.purse, R.drawable.margarita};
            int s1 = 0;

            for (DataForDrawerCategories.ResponseBean responseBean : response) {
                if (responseBean.getCategory_id() != null && responseBean.getSub_category().size() > 0) {// has subcategory

                    String image1 = responseBean.getImage();
                    int resID = getResources().getIdentifier(image1, "drawable", getPackageName());


                    group = menu.addGroup(true, true, true, true, SublimeGroup.CheckableBehavior.SINGLE);
                    sublimeBaseMenuItem = menu.addGroupHeaderItem(group.getGroupId(), "" + responseBean.getCategory_name(), null, false);

                    if (s1 < 6) {
                        Log.e(TAG, "image url " + integers[s1]);
                        sublimeBaseMenuItem.setIcon(integers[s1]);
                        s1++;
                    }
                    // loadMenuIcon(sublimeBaseMenuItem, image1);
                    for (DataForDrawerCategories.ResponseBean.SubCategoryBean subCategoryBean : responseBean.getSub_category()) {
                        sublimeBaseMenuItem1 = menu.addTextItem(group.getGroupId(), "" + subCategoryBean.getSub_category_name(), null, true);
                        menuAndIndexList.put(sublimeBaseMenuItem1, new MenuIndexes(response.indexOf(responseBean), responseBean.getSub_category().indexOf(subCategoryBean)));
                    }
                    menu.addSeparatorItem(group.getGroupId()).setIcon(R.color.gray);
                } else {//doesn't have sub group
                    Log.d(TAG, "subcat not found");
                    //create non collapsible group
                    SublimeGroup nonCollapseGroup = menu.addGroup(true, false, true, true, SublimeGroup.CheckableBehavior.NONE);
                    SublimeBaseMenuItem sublimeBaseMenuItem = menu.addTextItem(nonCollapseGroup.getGroupId(), "" + responseBean.getCategory_name(), null, false);
                    sublimeBaseMenuItem.setIcon(R.drawable.margarita);
                    menuAndIndexList.put(sublimeBaseMenuItem, new MenuIndexes(response.indexOf(responseBean), 0));
                    menu.addSeparatorItem(nonCollapseGroup.getGroupId()).setIcon(R.color.gray);
                }
            }
        }
        final SublimeGroup group3 = menu.addGroup(true, false, true, true, SublimeGroup.CheckableBehavior.NONE);
        SublimeBaseMenuItem sublimeBaseMenuItem = menu.addTextItem(group3.getGroupId(), "Whats new", null, false).setIcon(R.drawable.ic_badge);

        menu.addSeparatorItem(group3.getGroupId()).setIcon(R.color.gray);
        menuAndIndexList.put(sublimeBaseMenuItem, new MenuIndexes(CATEGORY_WHATSNEW, -2));

        SublimeBaseMenuItem sublimeBaseMenuItem2 = menu.addTextItem(group3.getGroupId(), "Best Seller", null, false).setIcon(R.drawable.ic_quality);

        menu.addSeparatorItem(group3.getGroupId()).setIcon(R.color.gray);
        menuAndIndexList.put(sublimeBaseMenuItem2, new MenuIndexes(CATEGORY_BEST_SELLER, -3));
        SublimeBaseMenuItem sublimeBaseMenuItem3 = menu.addTextItem(group3.getGroupId(), "About Us", null, false).setIcon(R.drawable.ic_care_about);

        menu.addSeparatorItem(group3.getGroupId()).setIcon(R.color.gray);
        menuAndIndexList.put(sublimeBaseMenuItem3, new MenuIndexes(CATEGORY_BEST_SELLER, -4));


        sublimeNavigationView.setNavigationMenuEventListener(new OnNavigationMenuEventListener() {

            @Override
            public boolean onNavigationMenuEvent(Event event, SublimeBaseMenuItem menuItem) {

                switch (event) {
                    case CHECKED:
                        Log.i(TAG, "Item checked");
                        menuItem.getTitle();
                        break;
                    case UNCHECKED:
                        Log.i(TAG, "Item unchecked");
                        break;
                    case GROUP_EXPANDED:
                        List<DataForDrawerCategories.ResponseBean> response1 = response;
                        responseBean = response1.get(array_list_for_menuItem);
                        String category_id = responseBean.getCategory_id();
                        sub_category = responseBean.getSub_category();

                        Log.e(TAG, "category id" + category_id);
                        Log.i(TAG, "Group expanded");
                        break;
                    case GROUP_COLLAPSED:
                        Log.i(TAG, "Group collapsed");
                        break;
                    default:

                        MenuIndexes menuIndexes = menuAndIndexList.get(menuItem);
                        if (menuIndexes.categoryIndex >= 0) {// not equal to -1
                            DataForDrawerCategories instance = DataForDrawerCategories.getInstance();
                            DataForDrawerCategories.ResponseBean responseBean = instance.getResponse().get(menuIndexes.categoryIndex);

                            String category_id1 = responseBean.getCategory_id();
                            String category_name = responseBean.getCategory_name();
                            String sub_category_id;
                            String name;
                            if (responseBean.getSub_category() != null && responseBean.getSub_category().size() > 0) {
                                sub_category_id = responseBean.getSub_category().get(menuIndexes.subcategoryIndex).getSub_category_id();
                                name = responseBean.getSub_category().get(menuIndexes.subcategoryIndex).getSub_category_name();
                            } else {
                                sub_category_id = "0";
                                name = responseBean.getCategory_name();
                            }
                            Log.d(TAG, "categoryId" + category_id1 +
                                    "\nsubCategoryId" + sub_category_id +
                                    "\nName" + name);

                            switch (Integer.parseInt(category_id1)) {
                                case CATEGORY_PRODUCT:
                                    Toast.makeText(MainActivity.this, "not an Intent", Toast.LENGTH_SHORT).show();
                                    break;
                                case CATEGORY_BEST_SELLER:
                                    Toast.makeText(MainActivity.this, "not an Intent", Toast.LENGTH_SHORT).show();
                                    break;
                                case CATEGORY_WHATSNEW:
                                    Toast.makeText(MainActivity.this, "not an Intent", Toast.LENGTH_SHORT).show();

                                    break;
                                default:
                                    Toast.makeText(MainActivity.this, "Send Intent", Toast.LENGTH_SHORT).show();
                                    AppController.getInstance().getPreferenceEditor().putString(TAGS.SUB_CATEGORY_ID, sub_category_id).commit();
                                    AppController.getInstance().getPreferenceEditor().putString(TAGS.CATEGORY_ID, category_id1).commit();
                                    AppController.getInstance().getPreferenceEditor().putString(TAGS.CATEGORY_NAME, category_name).commit();
                                    Intent intent = new Intent(MainActivity.this, ProductList.class);
                                    startActivity(intent);
                                    finish();
                            }

                        } else {
                            Log.d(TAG, "categoryIndex: " + menuIndexes.categoryIndex +
                                    "\nsubCategoryIndex: " + menuIndexes.subcategoryIndex);
                            if (menuItem.getTitle().equals("Whats new")) {
                                AppController.getInstance().getPreferenceEditor().putString(TAGS.SUB_CATEGORY_ID, "").commit();
                                AppController.getInstance().getPreferenceEditor().putString(TAGS.CATEGORY_ID, "").commit();
                                AppController.getInstance().getPreferenceEditor().putString(TAGS.BRAND_ID, "").commit();
                                AppController.getInstance().getPreferenceEditor().putString(TAGS.SHORTEDLIST, "latest").commit();
                                Intent intent1 = new Intent(MainActivity.this, ProductList.class);
                                startActivity(intent1);
                                finish();
                            }
                        }

                }
                return true;
            }
        });
    }

    private void loadMenuIcon(final SublimeBaseMenuItem menuItem, String url) {

        Target target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
                BitmapDrawable mBitmapDrawable = new BitmapDrawable(getResources(), bitmap);
                //  menuItem.setIcon(mBitmapDrawable);
            }

            @Override
            public void onBitmapFailed(Drawable drawable) {
            }

            @Override
            public void onPrepareLoad(Drawable drawable) {
            }
        };

        targets.add(target);

        Picasso.with(this).load(url).into(target);

    }


    private void bottonNavigationMethod() {
        navigationAdapter = new AHBottomNavigationAdapter(this, R.menu.bottom_nav_items);
        navigationAdapter.setupWithBottomNavigation(bottomNavigation);
        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                if (position == 0) {
                    if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
                        Intent intent = new Intent(MainActivity.this, WishListActivity.class);
                        startActivity(intent);
                        //    finish();
                    } else {
                        Intent i = new Intent(MainActivity.this, LoginActivity.class);
                        startActivity(i);
                        //    finish();
                    }
                }
                if (position == 1) {

                        Intent intent = new Intent(MainActivity.this, RecentViewActivity.class);
                        startActivity(intent);



                }
                if (position == 2) {
                    Toast.makeText(getApplicationContext(), "here is notification", Toast.LENGTH_LONG).show();

                }
                return true;
            }
        });
    }

    private Toolbar getToolbar() {
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onBackPressed();
                        }
                    });
                } else {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                    toggle.syncState();
                    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            drawer.openDrawer(GravityCompat.START);
                        }
                    });
                }
            }
        });
        return toolbar;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onClick(View v) {
        if (v == imProfile) {
            drawer.closeDrawer(GravityCompat.START);
            fm = getSupportFragmentManager();
            ft = fm.beginTransaction();
//            ft.replace(R.id.frame_container, new ProilfeFragment());
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            ft.addToBackStack(null);
            ft.commit();
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            finish();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit",
                Toast.LENGTH_SHORT).show();

    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        AppController.getInstance().bus().toObserverable().subscribe(new Action1<Object>() {
            @Override
            public void call(Object o) {
                if (o instanceof String) {
                    //  int badgerValue = (int) o;
                    int badgerValue = Integer.valueOf((String) o);
                    Badger.sett(menu.findItem(R.id.action_shopping_bag), circleFactory).setCount(badgerValue);
                    Log.d(TAG, "new badger value " + badgerValue);
                }
            }
        });
        if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
            MenuItem item = menu.findItem(R.id.action_logout);
            item.setTitle("Logout");
        } else {
            MenuItem item = menu.findItem(R.id.action_logout);
            item.setTitle("Login");
        }


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (item.getItemId()) {
            case R.id.action_search:
                Intent intent = new Intent(MainActivity.this, SearchViewActivity.class);
                intent.putExtra(TAGS.FLAG, "true");
                startActivity(intent);
                finish();
                return true;
            case R.id.action_shopping_bag:
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
                    Intent intent1 = new Intent(MainActivity.this, AddToCart.class);
                    intent1.putExtra(TAGS.FLAG, "true");
                    startActivity(intent1);
                } else {
                    Intent i = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                return true;
            case R.id.action_order:
                break;
            case R.id.action_wishlist:
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {

                    Intent intent1 = new Intent(MainActivity.this, WishListActivity.class);
                    startActivity(intent1);
                } else {
                    Intent i = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                break;
            case R.id.action_profile:
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
                    Intent intent1 = new Intent(MainActivity.this, EditProfileActivity.class);
                    startActivity(intent1);
                } else {
                    Intent i = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                break;
            case R.id.action_myAddress:
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {

                    Intent intent4 = new Intent(MainActivity.this, AddressActivity.class);
                    intent4.putExtra(TAGS.FLAG, "false");
                    startActivity(intent4);
                } else {
                    Intent i = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                break;
            case R.id.action_changePass:
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
                    changePassword();
                } else {
                    Intent i = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                break;
            case R.id.action_logout:
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
                    LoginManager.getInstance().logOut();
                    AppController.getInstance().getPreferenceEditor().putString(TAGS.JSON_USER_DATA, "").commit();
                    Intent intent2 = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(intent2);
                    finish();
                } else {
                    Intent i = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                break;
// return true;
            default:
                return super.onOptionsItemSelected(item);

        }
        return true;
    }

    private void changePassword() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
// ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.password_activity, null);
        dialogBuilder.setView(dialogView);

        final EditText currentpass = (EditText) dialogView.findViewById(R.id.currentPassword);
        final EditText changepass = (EditText) dialogView.findViewById(R.id.ChangePassword);
        AppCompatButton btnchangepass = (AppCompatButton) dialogView.findViewById(R.id.btn_chng);


        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
        btnchangepass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation(changepass)) {
                    String currentpassword = currentpass.getText().toString();
                    String changedpassword = changepass.getText().toString();
                    AppController.getInstance().getRetrofitServices().changepassword(uid, currentpassword, changedpassword).enqueue(new Callback<DataForCurrentPassword>() {
                        @Override
                        public void onResponse(Call<DataForCurrentPassword> call, Response<DataForCurrentPassword> response) {
                            DataForCurrentPassword dataForCurrentPassword = response.body();
                            if (dataForCurrentPassword.getStatus().equalsIgnoreCase("success")) {
                                Snackbar snackbar = Snackbar.make(coLay, dataForCurrentPassword.getMessage(), Snackbar.LENGTH_LONG);
                                snackbar.show();
                                alertDialog.dismiss();
                            } else {
                                Snackbar snackbar = Snackbar.make(coLay, dataForCurrentPassword.getMessage(), Snackbar.LENGTH_LONG);
                                snackbar.show();
                                alertDialog.dismiss();
                            }

                        }

                        @Override
                        public void onFailure(Call<DataForCurrentPassword> call, Throwable t) {
                            Toast.makeText(MainActivity.this, "Server error can't download the categories", Toast.LENGTH_LONG).show();
                            finish();
                        }
                    });
                }
            }
        });




/* */
    }


    private boolean validation(EditText changepass) {
        boolean validation = true;
        String password = changepass.getText().toString();
        if (TextUtils.isEmpty(password)) {
            Snackbar snackbar = Snackbar.make(coLay, getResources().getString(R.string.error_empty_password), Snackbar.LENGTH_LONG);
            View view = snackbar.getView();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            snackbar.show();
            return false;
        }
//if no error found
        return validation;
    }

    class MenuIndexes {
        private final int categoryIndex;
        private final int subcategoryIndex;

        public MenuIndexes(int categoryIndex, int subcategoryIndex) {
            this.categoryIndex = categoryIndex;
            this.subcategoryIndex = subcategoryIndex;
        }
    }
}

