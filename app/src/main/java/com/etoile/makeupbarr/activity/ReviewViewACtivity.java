package com.etoile.makeupbarr.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.Utility.TAGS;
import com.etoile.makeupbarr.adapter.RecyclerViewAdapterForReview;
import com.etoile.makeupbarr.model.DataForProductDetail;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;

/**
 * Created by AndroidPC on 07-02-2017.
 */
public class ReviewViewActivity extends AppCompatActivity {
    @Bind(R.id.rv_user)
    RecyclerView recyclerView;
    @Bind(R.id.ratin_review_average)
    MaterialRatingBar ratinReviewAverage;
    @Bind(R.id.rating)
    TextView rating;
    @Bind(R.id.ratinguser)
    TextView ratinguser;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    private RecyclerViewAdapterForReview recyclerViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.review_activity);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(ReviewViewActivity.this));
        getReviewData();
    }

    private void getReviewData() {
        String userdata = AppController.getInstance().getPreference().getString(TAGS.JSON_FOR_PRODUCT_DETAIL, "");
        DataForProductDetail dataForLogin = AppController.getInstance().getGson().fromJson(userdata, DataForProductDetail.class);
        DataForProductDetail.ResponseBean response = dataForLogin.getResponse();
        List<DataForProductDetail.ResponseBean.ReviewBean> review = response.getReview();
        List<Float> doubles = new ArrayList<>();
        Float sum = 0.0f;
        for (DataForProductDetail.ResponseBean.ReviewBean reviewBean : review) {
            doubles.add(Float.valueOf(reviewBean.getRating()));
        }
        for (int i = 0; i < doubles.size(); i++) {
            sum += doubles.get(i);
        }
        HashSet<String> stringHashSet = new HashSet<>();
        for (DataForProductDetail.ResponseBean.ReviewBean reviewBean : review) {
            stringHashSet.add(reviewBean.getUid());
        }
        int a = 0;
        for (int i = 0; i < stringHashSet.size(); i++) {
            a = stringHashSet.size();
        }
        ratinguser.setText("Based on " + a + " user rating");
        String averagevalue = String.valueOf(sum / doubles.size());
        Log.e("this", "" + averagevalue);
        ratinReviewAverage.setRating(Float.parseFloat(averagevalue));
        rating.setText(averagevalue);

        recyclerViewAdapter = new RecyclerViewAdapterForReview(ReviewViewActivity.this, review);
        recyclerView.setAdapter(recyclerViewAdapter);
    }
    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
       // getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
