package com.etoile.makeupbarr.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;

import com.astuetz.PagerSlidingTabStrip;
import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.TAGS;
import com.etoile.makeupbarr.adapter.SampleFragmentPagerAdapter;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by admin on 2/6/2017.
 */

public class DesciptionFragment extends AppCompatActivity {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.tabs)
    PagerSlidingTabStrip tabsStrip;
    @Bind(R.id.viewpager)
    ViewPager viewpager;
    ArrayList<String> list;
    String list_values;
    private String tab_values;
    private int tab_values_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_fragment_one);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        tab_values_id = intent.getExtras().getInt(TAGS.TAB_VALUES_ID);
        tab_values = intent.getExtras().getString(TAGS.TAB_VALUES);
        list = getIntent().getStringArrayListExtra(TAGS.TAB_VALUES_LIST);
        showDataInTab(tab_values, tab_values_id);
        tabsStrip.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {

                tabsStrip.setTabBackground(R.color.gray);
            }
            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    private void showDataInTab(String tab_values, int tab_values_id) {
        Log.d("this", tab_values + "\n" + tab_values_id);
        viewpager.setAdapter(new SampleFragmentPagerAdapter(getSupportFragmentManager(), list));
        tabsStrip.setViewPager(viewpager);
        viewpager.setCurrentItem(tab_values_id);
        tabsStrip.setTabBackground(R.color.gray);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        // getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}