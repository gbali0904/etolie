package com.etoile.makeupbarr.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;

import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.Utility.TAGS;
import com.etoile.makeupbarr.adapter.CustomImageViewAdapter;
import com.etoile.makeupbarr.model.DataForProductDetail;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by AndroidPC on 28-01-2017.
 */
public class ZoomImageView extends AppCompatActivity {


    @Bind(R.id.coloListView)
    RecyclerView coloListView;
    private CustomImageViewAdapter recyclerViewAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.zoom_product_list);
        ButterKnife.bind(this);
        String image_view = AppController.getInstance().getPreference().getString(TAGS.JSON_FOR_PRODUCT_DETAIL, "");
        DataForProductDetail dataForProductDetail = AppController.getInstance().getGson().fromJson(image_view, DataForProductDetail.class);
        DataForProductDetail.ResponseBean productlistresponse = dataForProductDetail.getResponse();
        List<DataForProductDetail.ResponseBean.ProductImagesBean> product_images = productlistresponse.getProduct_images();

        coloListView.setHasFixedSize(true);
        // coloListView.setLayoutManager(new LinearLayoutManager(ProductDetailActivity.this));
        //  coloListView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true));
        StaggeredGridLayoutManager staggeredGridLayoutManager =
                new StaggeredGridLayoutManager(
                        1, //The number of Columns in the grid
                        LinearLayoutManager.HORIZONTAL);
        coloListView.setLayoutManager(staggeredGridLayoutManager);

        recyclerViewAdapter = new CustomImageViewAdapter(ZoomImageView.this, product_images);
        coloListView.setAdapter(recyclerViewAdapter);
    }

}