package com.etoile.makeupbarr.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.Utility.TAGS;
import com.etoile.makeupbarr.adapter.RecylerViewAdpterForViewRecentProduct;
import com.etoile.makeupbarr.login.AddressActivity;
import com.etoile.makeupbarr.login.EditProfileActivity;
import com.etoile.makeupbarr.login.LoginActivity;
import com.etoile.makeupbarr.login.LoginPage;
import com.etoile.makeupbarr.model.DataForCurrentPassword;
import com.etoile.makeupbarr.model.DataForLogin;
import com.etoile.makeupbarr.model.DataForRecentVIew;
import com.facebook.login.LoginManager;

import java.util.ArrayList;
import java.util.List;

import berlin.volders.badger.BadgeShape;
import berlin.volders.badger.Badger;
import berlin.volders.badger.CountBadge;
import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.functions.Action1;

/**
 * Created by admin on 2/20/2017.
 */

public class RecentViewActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private static final String TAG = RecentViewActivity.class.getSimpleName();
    private static OnUpdateListener mListener;
    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.wishlist_content)
    RelativeLayout wishlistContent;
    @Bind(R.id.mycart_empty)
    LinearLayout mycartEmpty;
    @Bind(R.id.recentView_recycler)
    RecyclerView recentViewRecycler;
    @Bind(R.id.recentView_root)
    CoordinatorLayout recentViewRoot;


    private RecylerViewAdpterForViewRecentProduct recyclerViewAdapter;
    private List<DataForRecentVIew.ResultBean> response1;
    private DataForLogin dataForLogin;
    private DataForLogin.ResponseBean response;
    private String uid;
    private CountBadge.Factory circleFactory;

    public static void setOnUpdateListener(OnUpdateListener listener) {
        mListener = listener;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recent_view);
        ButterKnife.bind(this);

        toolbar = getToolbar();
        bagerCount();
        getAddRecentProduct();


        recentViewRecycler.setHasFixedSize(true);
        recentViewRecycler.setLayoutManager(new GridLayoutManager(RecentViewActivity.this, 2));
        RecylerViewAdpterForViewRecentProduct.setOnUpdateListener(new RecylerViewAdpterForViewRecentProduct.OnUpdateListener() {
            @Override
            public void onUpdate() {
                // Toast.makeText(EditProfileActivity.this, "Successfully  Closed\u200E", Toast.LENGTH_SHORT).show();
                response1.clear();
                recyclerViewAdapter.notifyDataSetChanged();
                getAddRecentProduct();

            }
        });

    }

    private Toolbar getToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
        });
        return toolbar;
    }

    private void getAddRecentProduct() {

        if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
            String string = AppController.getInstance().getPreference().getString(TAGS.JSON_USER_DATA, "");
            DataForLogin dataForLogin = AppController.getInstance().getGson().fromJson(string, DataForLogin.class);
            DataForLogin.ResponseBean response = dataForLogin.getResponse();
            uid = response.getUid();
            AppController.getInstance().showProgressDialog(this);
            Call<DataForRecentVIew> mService = AppController.getInstance().getRetrofitServices().viewRecentProduct(uid);
            mService.enqueue(new Callback<DataForRecentVIew>() {
                @Override
                public void onResponse(Call<DataForRecentVIew> call, Response<DataForRecentVIew> response) {
                    AppController.getInstance().hideProgressDialog();
                    DataForRecentVIew recentViewJsonData = response.body();
                    if (recentViewJsonData.getStatus().equals("success")) {
                        response1 = recentViewJsonData.getResult();
                        if (response1.isEmpty()) {
                            mycartEmpty.setVisibility(View.VISIBLE);
                            recentViewRecycler.setVisibility(View.GONE);
                        } else {
                            recyclerViewAdapter = new RecylerViewAdpterForViewRecentProduct(RecentViewActivity.this, response1, recentViewRoot);
                            recentViewRecycler.setAdapter(recyclerViewAdapter);
                        }
                    } else {
                        Snackbar snackbar = Snackbar.make(recentViewRoot, recentViewJsonData.getMessage(), Snackbar.LENGTH_LONG);
                        snackbar.show();

                    }
                }

                @Override
                public void onFailure(Call<DataForRecentVIew> call, Throwable t) {
                    call.cancel();
                    Toast.makeText(RecentViewActivity.this, "Please check your network connection and internet permission", Toast.LENGTH_LONG).show();
                }
            });
        }else {

            AppController.getInstance().showProgressDialog(this);
            Call<DataForRecentVIew> mService = AppController.getInstance().getRetrofitServices().viewRecentProduct("");
            mService.enqueue(new Callback<DataForRecentVIew>() {
                @Override
                public void onResponse(Call<DataForRecentVIew> call, Response<DataForRecentVIew> response) {
                    AppController.getInstance().hideProgressDialog();
                    DataForRecentVIew recentViewJsonData = response.body();
                    if (recentViewJsonData.getStatus().equals("success")) {
                        response1 = recentViewJsonData.getResult();
                        if (response1.isEmpty()) {
                            mycartEmpty.setVisibility(View.VISIBLE);
                            recentViewRecycler.setVisibility(View.GONE);
                        } else {
                            recyclerViewAdapter = new RecylerViewAdpterForViewRecentProduct(RecentViewActivity.this, response1, recentViewRoot);
                            recentViewRecycler.setAdapter(recyclerViewAdapter);
                        }
                    } else {
                        Snackbar snackbar = Snackbar.make(recentViewRoot, recentViewJsonData.getMessage(), Snackbar.LENGTH_LONG);
                        snackbar.show();

                    }
                }

                @Override
                public void onFailure(Call<DataForRecentVIew> call, Throwable t) {
                    call.cancel();
                    Toast.makeText(RecentViewActivity.this, "Please check your network connection and internet permission", Toast.LENGTH_LONG).show();
                }
            });

        }

    }

    private void bagerCount() {
        circleFactory = new CountBadge.Factory(this, BadgeShape.circle(.5f, Gravity.END | Gravity.TOP));
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_productlist, menu);
       /* // menuItem.setIcon(buildCounterDrawable(count, R.drawable.ic_shopping_cart));
        MenuItem searchItem = menu.findItem(R.id.action_search);
        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(this.SEARCH_SERVICE);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        int searchImgId = android.support.v7.appcompat.R.id.search_button;
        ImageView v = (ImageView) searchView.findViewById(searchImgId);
        v.setImageResource(R.drawable.ic_search_view);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(true); // Iconify the widget
        searchView.setOnQueryTextListener(this);*/


        MenuItem item = menu.findItem(R.id.action_cart);

        String s1 = AppController.getInstance().getPreference().getString(TAGS.TOATAL_RECORD, "");
        int i1 = Integer.parseInt(s1);
        Badger.sett(menu.findItem(R.id.action_cart), circleFactory).setCount(i1);

        AppController.getInstance().bus().toObserverable().subscribe(new Action1<Object>() {
            @Override
            public void call(Object o) {
                if (o instanceof String) {
                    //  int badgerValue = (int) o;
                    int badgerValue = Integer.valueOf((String) o);
                    Badger.sett(menu.findItem(R.id.action_cart), circleFactory).setCount(badgerValue);
                    Log.d(TAG, "new badger value " + badgerValue);
                }
            }
        });


        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (item.getItemId()) {
            case R.id.action_cart: {
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
                    Intent intent1 = new Intent(RecentViewActivity.this, AddToCart.class);
                    intent1.putExtra(TAGS.FLAG, "true");
                    startActivity(intent1);
                } else {
                    Intent i = new Intent(RecentViewActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            }
            break;
        /*    case R.id.action_order1:
            {

            }
            return true;*/


            case R.id.action_wishlist: {
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
                    Intent intent1 = new Intent(RecentViewActivity.this, WishListActivity.class);
                    startActivity(intent1);
                    finish();
                } else {
                    Intent i = new Intent(RecentViewActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            }
            break;
            case R.id.action_profile: {
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
                    Intent intent1 = new Intent(RecentViewActivity.this, EditProfileActivity.class);
                    startActivity(intent1);
                } else {
                    Intent i = new Intent(RecentViewActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            }

            break;

            case R.id.action_myAddress:
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
                    Intent intent4 = new Intent(RecentViewActivity.this, AddressActivity.class);
                    intent4.putExtra(TAGS.FLAG, "false");
                    startActivity(intent4);
                } else {
                    Intent i = new Intent(RecentViewActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }

                break;
            case R.id.action_changePass:
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
                    changePassword();
                } else {
                    Intent i = new Intent(RecentViewActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                break;
            case R.id.action_logout: {
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {

                    LoginManager.getInstance().logOut();
                    AppController.getInstance().getPreferenceEditor().putString(TAGS.JSON_USER_DATA, "").commit();
                    Intent intent2 = new Intent(RecentViewActivity.this, LoginPage.class);
                    startActivity(intent2);
                    finish();
                } else {
                    Intent i = new Intent(RecentViewActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            }

            break;
            default:
                if (id == R.id.ic_dots) {
//do nothing
                } else {
                    onBackPressed();
                }

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void changePassword() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
// ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.password_activity, null);
        dialogBuilder.setView(dialogView);

        final EditText currentpass = (EditText) dialogView.findViewById(R.id.currentPassword);
        final EditText changepass = (EditText) dialogView.findViewById(R.id.ChangePassword);
        AppCompatButton btnchangepass = (AppCompatButton) dialogView.findViewById(R.id.btn_chng);


        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.setTitle("Change Password");
        alertDialog.show();
        btnchangepass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation(changepass)) {
                    String currentpassword = currentpass.getText().toString();
                    String changedpassword = changepass.getText().toString();
                    AppController.getInstance().getRetrofitServices().changepassword(uid, currentpassword, changedpassword).enqueue(new Callback<DataForCurrentPassword>() {
                        @Override
                        public void onResponse(Call<DataForCurrentPassword> call, Response<DataForCurrentPassword> response) {
                            DataForCurrentPassword dataForCurrentPassword = response.body();
                            if (dataForCurrentPassword.getStatus().equalsIgnoreCase("success")) {
                                Snackbar snackbar = Snackbar.make(recentViewRoot, dataForCurrentPassword.getMessage(), Snackbar.LENGTH_LONG);
                                snackbar.show();
                                alertDialog.dismiss();
                            } else {
                                Snackbar snackbar = Snackbar.make(recentViewRoot, dataForCurrentPassword.getMessage(), Snackbar.LENGTH_LONG);
                                snackbar.show();
                                alertDialog.dismiss();
                            }

                        }

                        @Override
                        public void onFailure(Call<DataForCurrentPassword> call, Throwable t) {
                            Toast.makeText(RecentViewActivity.this, "Server error can't download the categories", Toast.LENGTH_LONG).show();
                            finish();
                        }
                    });
                }
            }
        });




/* */
    }

    private boolean validation(EditText changepass) {
        boolean validation = true;
        String password = changepass.getText().toString();
        if (TextUtils.isEmpty(password)) {
            Snackbar snackbar = Snackbar.make(recentViewRoot, getResources().getString(R.string.error_empty_password), Snackbar.LENGTH_LONG);
            View view = snackbar.getView();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            snackbar.show();
            return false;
        }
//if no error found
        return validation;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }

    private List<DataForRecentVIew.ResultBean> filter(List<DataForRecentVIew.ResultBean> models, String query) {
        query = query.toLowerCase();
        final List<DataForRecentVIew.ResultBean> filteredModelList = new ArrayList<>();
        filteredModelList.clear();
        Log.d(TAG, "querry String: " + query);
        for (DataForRecentVIew.ResultBean model : models) {
            final String text = model.getProduct_name().toLowerCase();


            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        final List<DataForRecentVIew.ResultBean> filteredModelList = filter(response1, query);
        recyclerViewAdapter.setFilter(filteredModelList);
        return true;
    }

    public interface OnUpdateListener {
        void onUpdate();
    }
}
