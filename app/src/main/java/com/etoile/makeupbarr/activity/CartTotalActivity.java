package com.etoile.makeupbarr.activity;

import android.util.Log;

import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.Utility.TAGS;
import com.etoile.makeupbarr.model.DataForTotalCartItem;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by AndroidPC on 10-01-2017.
 */

public class CartTotalActivity {
    private static String total_record;
    private static OnUpdateListener mListener;

    public static String totalNoOfCart(String uid) {
        //    address_id = AppController.getInstance().getPreference().getString(TAGS.ADDRESSID_JSON_DATA, "");
        AppController.getInstance().getRetrofitServices().carttotal(uid).enqueue(new Callback<DataForTotalCartItem>() {
            @Override
            public void onResponse(Call<DataForTotalCartItem> call, Response<DataForTotalCartItem> response) {
                DataForTotalCartItem jsonforTotalCart = response.body();
                 total_record = ""+jsonforTotalCart.getTotal_record();
                String jsonValuefornLogin = AppController.getInstance().getGson().toJson(jsonforTotalCart);
                AppController.getInstance().getPreferenceEditor().putString(TAGS.TOATAL_RECORD, total_record).commit();
                Log.e("thisdata",""+total_record);
                if(mListener!=null){
                    mListener.onUpdate();
                }
            }

            @Override
            public void onFailure(Call<DataForTotalCartItem> call, Throwable t) {
            }
        });

        return total_record;
    }


    public static void setOnUpdateListener(OnUpdateListener listener) {
        mListener = listener;
    }

    public interface OnUpdateListener {
        void onUpdate();
    }
}
