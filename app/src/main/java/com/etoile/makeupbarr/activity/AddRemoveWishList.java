package com.etoile.makeupbarr.activity;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.widget.Toast;

import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.Utility.TAGS;
import com.etoile.makeupbarr.model.DataForAddRemoveWishList;
import com.etoile.makeupbarr.model.DataForLogin;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by AndroidPC on 28-12-2016.
 */

public class AddRemoveWishList {
    private DataForAddRemoveWishList mywishlist;

    public void getResponseData(final Context context, final CoordinatorLayout coLay) {
        AppController.getInstance().getPreferenceEditor().putString(TAGS.WISHLIST ,"").commit();

        String userdata = AppController.getInstance().getPreference().getString(TAGS.JSON_USER_DATA, "");
        DataForLogin dataForLogin = AppController.getInstance().getGson().fromJson(userdata, DataForLogin.class);
        DataForLogin.ResponseBean response = dataForLogin.getResponse();
        String uid = response.getUid();
        String productlist = AppController.getInstance().getPreference().getString(TAGS.PRODUCT_LIST, "");
        AppController.getInstance().getRetrofitServices().wishlist(productlist,uid).enqueue(new Callback<DataForAddRemoveWishList>() {
            @Override
            public void onResponse(Call<DataForAddRemoveWishList> call, Response<DataForAddRemoveWishList> response) {
                 mywishlist = response.body();
                if(mywishlist.getStatus().equals("success"))
                {
                    AppController.getInstance().getPreferenceEditor().putString(TAGS.WISHLIST , mywishlist.getMessage()).commit();
                    Snackbar snackbar = Snackbar.make(coLay, mywishlist.getMessage(), Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
                else {
                    Snackbar snackbar = Snackbar.make(coLay, mywishlist.getMessage(), Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
            @Override
            public void onFailure(Call<DataForAddRemoveWishList> call, Throwable t) {
                Toast.makeText(context, "Server error can't download the categories", Toast.LENGTH_LONG).show();
            }
        });
    }
}
