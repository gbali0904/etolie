package com.etoile.makeupbarr.activity;

import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.CallSuper;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.Utility.TAGS;
import com.etoile.makeupbarr.adapter.RecyclerViewAdapterForSearchView;
import com.etoile.makeupbarr.model.DataForDrawerCategories;
import com.etoile.makeupbarr.model.DataForSubCategory;
import com.lapism.searchview.SearchAdapter;
import com.lapism.searchview.SearchHistoryTable;
import com.lapism.searchview.SearchItem;
import com.lapism.searchview.SearchView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.etoile.makeupbarr.R.id.recyclerView;
import static com.etoile.makeupbarr.R.id.searchView;

/**
 * Created by AndroidPC on 13-01-2017.
 */
public class SearchViewActivity extends AppCompatActivity {

    protected static final String EXTRA_KEY_TEXT = "text";
    private static final String EXTRA_KEY_VERSION = "version";
    private static final String EXTRA_KEY_THEME = "theme";
    private static final String EXTRA_KEY_VERSION_MARGINS = "version_margins";
    protected SearchView mSearchView = null;
    protected ActionBarDrawerToggle mActionBarDrawerToggle = null;
    @Bind(recyclerView)
    RecyclerView recyclerViewforserach;
    private SearchHistoryTable mHistoryDatabase;
    private int COLUMN = 1;
    private RecyclerViewAdapterForSearchView recyclerViewAdapter;
    private GridLayoutManager mLayoutManager;
    HashMap<String, String> categorytringStringHashMap;
    HashMap<String, String> subcategoryStringHashMap;
    HashMap<String, String> brandStringHashMap;
    private String categoryString = "";
    private String subcategoryString = "";
    private String brandString = "";
    ImageView imageView;
    private String flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main_search);
        ButterKnife.bind(this);

        Intent intent=getIntent();
         flag = intent.getExtras().getString(TAGS.FLAG);

        imageView=(ImageView) findViewById(R.id.imgclick);

        getData();
        setSearchView();
        mSearchView.setNavigationIcon(R.drawable.ic_arrow_back);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        customSearchView();
        recyclerViewforserach.setHasFixedSize(true);
        mLayoutManager = new GridLayoutManager(this, COLUMN);
        mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (recyclerViewAdapter.getItemViewType(position)) {
                    case RecyclerViewAdapterForSearchView.TYPE_HEADER:
                        return COLUMN;
                    case RecyclerViewAdapterForSearchView.TYPE_ITEM:
                        return 1;
                    case RecyclerViewAdapterForSearchView.TYPE_FOOTER:
                        return COLUMN;
                    default:
                        return -1;
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(flag.equalsIgnoreCase("true"))
        {
            Intent intent = new Intent(SearchViewActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }
        else {
            this.finish();
        }



    }

      @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SearchView.SPEECH_REQUEST_CODE && resultCode == RESULT_OK) {
            List<String> results = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if (results != null && results.size() > 0) {
                String searchWrd = results.get(0);
                if (!TextUtils.isEmpty(searchWrd)) {
                    if (mSearchView != null) {
                        mSearchView.setQuery(searchWrd, true);
                    }
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    // it can be in OnCreate
    protected void setSearchView() {
        mHistoryDatabase = new SearchHistoryTable(this);
        mSearchView = (SearchView) findViewById(searchView);
        if (mSearchView != null) {
            mSearchView.setHint(R.string.search);

            mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    mSearchView.setArrowOnly(false);
                    mSearchView.close(false);
                    return true;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    return false;
                }
            });

            mSearchView.setVoiceText("Set permission on Android 6.0+ !");
            mSearchView.setOnVoiceClickListener(new SearchView.OnVoiceClickListener() {
                @Override
                public void onVoiceClick() {
                    // permission
                }
            });

            List<SearchItem> suggestionsList = new ArrayList<>();

            categorytringStringHashMap = new HashMap<>();
            subcategoryStringHashMap = new HashMap<>();
            brandStringHashMap = new HashMap<>();


            List<DataForDrawerCategories.ResponseBean> response = DataForDrawerCategories.getInstance().getResponse();
            for (DataForDrawerCategories.ResponseBean responseBean : response) {
                String category_name = responseBean.getCategory_name();
                String category_id = responseBean.getCategory_id();
                suggestionsList.add(new SearchItem(category_name));
                categorytringStringHashMap.put(category_name, category_id);

                List<DataForDrawerCategories.ResponseBean.SubCategoryBean> sub_category = responseBean.getSub_category();
                for (DataForDrawerCategories.ResponseBean.SubCategoryBean subCategoryBean : sub_category) {
                    String sub_category_name = subCategoryBean.getSub_category_name();
                    String sub_category_id = subCategoryBean.getSub_category_id();
                    suggestionsList.add(new SearchItem(sub_category_name));
                    subcategoryStringHashMap.put(sub_category_name, sub_category_id);
                }
            }
            String userdata = AppController.getInstance().getPreference().getString(TAGS.JSON_CATEGORY, "");
            DataForDrawerCategories dataForCategory = AppController.getInstance().getGson().fromJson(userdata, DataForDrawerCategories.class);
            final List<DataForDrawerCategories.BrandBean> brand = dataForCategory.getBrand();
            //for brand
            for (DataForDrawerCategories.BrandBean responseBean : brand) {
                String brand_name = responseBean.getBrand_name();
                String brand_id = responseBean.getBrand_id();
                suggestionsList.add(new SearchItem(brand_name));
                brandStringHashMap.put(brand_name, brand_id);
            }


            SearchAdapter searchAdapter = new SearchAdapter(this, suggestionsList);
            searchAdapter.addOnItemClickListener(new SearchAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    TextView textView = (TextView) view.findViewById(R.id.textView_item_text);
                    String query = textView.getText().toString();
                    getDatafordetail(query);
                    mSearchView.close(false);
                }
            });
            mSearchView.setAdapter(searchAdapter);
        }
    }

    private void getDatafordetail(String query) {
        mHistoryDatabase.addItem(new SearchItem(query));
        categoryString = categorytringStringHashMap.get(query);
        subcategoryString = subcategoryStringHashMap.get(query);
        brandString = brandStringHashMap.get(query);
        System.out.println("category id is:" + categoryString + "\n subcategory id is :" + subcategoryString
                + "\nbrand id is:" + brandString);

        AppController.getInstance().getPreferenceEditor().putString(TAGS.SUB_CATEGORY_ID, subcategoryString).commit();
        AppController.getInstance().getPreferenceEditor().putString(TAGS.CATEGORY_ID, categoryString).commit();
        AppController.getInstance().getPreferenceEditor().putString(TAGS.BRAND_ID, brandString).commit();
        Intent intent = new Intent(SearchViewActivity.this, ProductList.class);
        startActivity(intent);
        finish();
    }

    protected void customSearchView() {
        Bundle extras = getIntent().getExtras();
        if (extras != null && mSearchView != null) {
            mSearchView.setVersion(extras.getInt(EXTRA_KEY_VERSION));
            mSearchView.setVersionMargins(extras.getInt(EXTRA_KEY_VERSION_MARGINS));
            mSearchView.setTheme(extras.getInt(EXTRA_KEY_THEME), true);
            mSearchView.setQuery(extras.getString(EXTRA_KEY_TEXT), false);
                 }
    }

    @CallSuper
    protected void getData() {
        AppController.getInstance().getRetrofitServices().view_sub_category().enqueue(new Callback<DataForSubCategory>() {
            @Override
            public void onResponse(Call<DataForSubCategory> call, Response<DataForSubCategory> response) {
                DataForSubCategory body = response.body();
                List<DataForSubCategory.SubCategoryListBean> sub_category_list = body.getSub_category_list();
                recyclerViewforserach.setLayoutManager(mLayoutManager);
                recyclerViewforserach.setItemAnimator(new DefaultItemAnimator());
                recyclerViewAdapter = new RecyclerViewAdapterForSearchView(SearchViewActivity.this, sub_category_list);
                recyclerViewforserach.setAdapter(recyclerViewAdapter);
            }

            @Override
            public void onFailure(Call<DataForSubCategory> call, Throwable t) {
                Toast.makeText(SearchViewActivity.this, "Server error can't Search", Toast.LENGTH_LONG).show();
                finish();
            }
        });

    }

}
