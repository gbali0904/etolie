package com.etoile.makeupbarr.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.Utility.Events;
import com.etoile.makeupbarr.Utility.TAGS;
import com.etoile.makeupbarr.adapter.CustomListViewAdapter;
import com.etoile.makeupbarr.adapter.ImageViewPagerDetail;
import com.etoile.makeupbarr.login.AddressActivity;
import com.etoile.makeupbarr.login.EditProfileActivity;
import com.etoile.makeupbarr.login.LoginActivity;
import com.etoile.makeupbarr.login.LoginPage;
import com.etoile.makeupbarr.model.DataForAddToCart;
import com.etoile.makeupbarr.model.DataForCurrentPassword;
import com.etoile.makeupbarr.model.DataForLogin;
import com.etoile.makeupbarr.model.DataForProductDetail;
import com.etoile.makeupbarr.model.DataForReview;
import com.facebook.login.LoginManager;
import com.pixelcan.inkpageindicator.InkPageIndicator;

import java.util.ArrayList;
import java.util.List;

import berlin.volders.badger.BadgeShape;
import berlin.volders.badger.Badger;
import berlin.volders.badger.CountBadge;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;
import de.hdodenhof.circleimageview.CircleImageView;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

/**
 * Created by admin on 12/10/2016.
 */


public class ProductDetailActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener, View.OnClickListener {
    private static final String TAG = ProductDetailActivity.class.getSimpleName();
    public static int count = 1;
    private static OnUpdateListener mListener;
    private static OnUpdateListenerProductDetail thisClassListener;
    public MenuItem item;
    CountBadge.Factory circleFactory;
    CountBadge badge;
    @Bind(R.id.img_minus)
    ImageView imgMinus;
    @Bind(R.id.tv_count)
    TextView tvCount;
    @Bind(R.id.img_plus)
    ImageView imgPlus;
    @Bind(R.id.save_item)
    LinearLayout saveItem;
    @Bind(R.id.add_to_cart)
    AppCompatButton addToCart;
    @Bind(R.id.linearLayout2)
    LinearLayout linearLayout2;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.view_pager)
    AutoScrollViewPager viewPager;
    @Bind(R.id.rounded_corner_figrprint)
    CircleImageView roundedCornerFigrprint;
    @Bind(R.id.img_wish)
    ImageView add_wishlist;
    @Bind(R.id.indicator)
    InkPageIndicator indicator;
    @Bind(R.id.product_name)
    TextView libelleProduit;
    @Bind(R.id.lbl_stock)
    TextView price_Stock;
    @Bind(R.id.stock)
    TextView dicount_stock;
    @Bind(R.id.percent)
    TextView percent;
    @Bind(R.id.tv_discription)
    TextView tvDiscription;
    @Bind(R.id.tv_tips)
    TextView tvTips;
    @Bind(R.id.tv_ingride)
    TextView tvIngride;
    @Bind(R.id.tv_shopping_return)
    TextView tvShoppingReturn;
    @Bind(R.id.tv_review)
    TextView tvReview;
    @Bind(R.id.tv_review4)
    TextView tvReview4;
    @Bind(R.id.ratin_review)
    MaterialRatingBar ratinReview;
    @Bind(R.id.main_layout)
    CoordinatorLayout coLay;
    @Bind(R.id.llHsView)
    LinearLayout llHsView;
    @Bind(R.id.hsView)
    HorizontalScrollView hsView;
    @Bind(R.id.activity_main)
    LinearLayout activityMain;
    @Bind(R.id.view_bag)
    AppCompatButton viewBag;
    private int circleMargin;
    private int circleDimension;
    private int objectCounter;
    private CircleImageView previouslySelectedColorOption;
    private List<DataForProductDetail.ResponseBean.ProductColorShapeBean> product_color_shape;
    private ImageViewPagerDetail mAdapter;
    private int dotsCount;
    private ImageView[] dots;
    private String product_id;
    private Subscriber busSubscription;
    private DataForLogin dataForLogin;
    private DataForLogin.ResponseBean response;
    private String uid;
    private String flagstring;
    private CustomListViewAdapter recyclerViewAdapter;
    private String finalRating;

    private List<CircleImageView> colorOptionList;


    private String description;
    private String tips;
    private String ingrdients;
    private String shopping_return;
    private int added_in_cart;

    public static void setOnUpdateListener(OnUpdateListener listener) {
        mListener = listener;
    }

    public static void setOnUpdateListenerProductDetail(OnUpdateListenerProductDetail listener) {
        thisClassListener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        circleMargin = getPixelFromDP(10);//define circleMargin
        circleDimension = getPixelFromDP(40);//define width and height

        if (bundle != null) {
            flagstring = bundle.getString(TAGS.FLAG);
        }
        if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
            String userdata = AppController.getInstance().getPreference().getString(TAGS.JSON_USER_DATA, "");
            dataForLogin = AppController.getInstance().getGson().fromJson(userdata, DataForLogin.class);
            response = dataForLogin.getResponse();
            uid = response.getUid();

        }
        getProdutData();
        AddToCart.setOnUpdateListener(new AddToCart.OnUpdateListener() {
            @Override
            public void onUpdate() {
                // Toast.makeText(EditProfileActivity.this, "Successfully  Closed\u200E", Toast.LENGTH_SHORT).show();
                colorOptionList.clear();
                llHsView.removeAllViews();
                getProdutData();
            }
        });

        ProductDetailActivity.setOnUpdateListenerProductDetail(new OnUpdateListenerProductDetail() {
            @Override
            public void onUpdateProductDetail() {
                colorOptionList.clear();
                llHsView.removeAllViews();
                getProdutData();

            }
        });


        toolbar = getToolbar();
        bagerCount();
        tvCount.setText("" + count);
    }

    private void bagerCount() {
        circleFactory = new CountBadge.Factory(this, BadgeShape.circle(0.6f, Gravity.END | Gravity.TOP));
    }

    private Toolbar getToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
        });
        return toolbar;
    }

    private void getProdutData() {

        AppController.getInstance().showProgressDialog(this);
        final String product_detail = AppController.getInstance().getPreference().getString(TAGS.PRODUCT_ID, "");
        Log.d("this data", "" + uid);

        AppController.getInstance().getRetrofitServices().product(product_detail, uid).enqueue(new Callback<DataForProductDetail>() {
            @Override
            public void onResponse(Call<DataForProductDetail> call, Response<DataForProductDetail> response) {
                AppController.getInstance().hideProgressDialog();
                DataForProductDetail jsonproductdetail = response.body();
                String jsonValueforproductdetail = AppController.getInstance().getGson().toJson(jsonproductdetail);
                AppController.getInstance().getPreferenceEditor().putString(TAGS.JSON_FOR_PRODUCT_DETAIL, jsonValueforproductdetail).commit();
                if (jsonproductdetail.getStatus().equals("success")) {
                    DataForProductDetail.ResponseBean productlistresponse = jsonproductdetail.getResponse();
                    product_color_shape = productlistresponse.getProduct_color_shape();
                    DataForProductDetail.ResponseBean.ProductDetailBean product_detail1 = productlistresponse.getProduct_detail();
                    List<DataForProductDetail.ResponseBean.ProductImagesBean> product_images = productlistresponse.getProduct_images();
                    List<DataForProductDetail.ResponseBean.ReviewBean> review = productlistresponse.getReview();
                    added_in_cart = productlistresponse.getAdded_in_cart();
                    if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
                        if (added_in_cart == 1) {
                            addToCart.setVisibility(View.GONE);
                            viewBag.setVisibility(View.VISIBLE);
                        } else {
                            addToCart.setVisibility(View.VISIBLE);
                            viewBag.setVisibility(View.GONE);
                        }
                    }else {
                        addToCart.setVisibility(View.VISIBLE);
                        viewBag.setVisibility(View.GONE);
                    }

                    detDiscription(product_detail1);
                    imageViewAdpater(product_images);
                    sliderViewAdpter(product_color_shape);
                    reviewShown(review);
                    if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
                        int wishlist = productlistresponse.getWishlist();
                        if (wishlist == 0) {
                            int black = Color.parseColor("black");
                            add_wishlist.setColorFilter(black);
                        } else {
                            int red = Color.parseColor("red");
                            add_wishlist.setColorFilter(red);
                        }
                    }

                    wishListAddRemoveMethod(product_detail1.getProduct_id());

                } else {
                    Snackbar snackbar = Snackbar.make(coLay, jsonproductdetail.getMessage(), Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }


            @Override
            public void onFailure(Call<DataForProductDetail> call, Throwable t) {
                Toast.makeText(ProductDetailActivity.this, "Server error can't download the categories", Toast.LENGTH_LONG).show();
            }

        });
    }

    private void wishListAddRemoveMethod(final String product_id) {
        add_wishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
                    AppController.getInstance().getPreferenceEditor().putString(TAGS.PRODUCT_LIST, product_id).commit();
                    Log.e("myid", product_id);
                    AddRemoveWishList addRemoveWishList = new AddRemoveWishList();
                    addRemoveWishList.getResponseData(ProductDetailActivity.this, coLay);
                    String string = AppController.getInstance().getPreference().getString(TAGS.WISHLIST, "");
                    if (string.equals("product wishlist added successfully")) {
                        int black = Color.parseColor("black");
                        add_wishlist.setColorFilter(black);

                    } else {
                        int red = Color.parseColor("red");
                        add_wishlist.setColorFilter(red);

                    }
                    if (thisClassListener != null) {
                        thisClassListener.onUpdateProductDetail();
                    }
                } else {
                    Intent i = new Intent(ProductDetailActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_productlist, menu);

        if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {

            MenuItem item = menu.findItem(R.id.action_logout);
            item.setTitle("Logout");

            String s1 = AppController.getInstance().getPreference().getString(TAGS.TOATAL_RECORD, "");
            int i1 = Integer.parseInt(s1);
            Badger.sett(menu.findItem(R.id.action_cart), circleFactory).setCount(i1);
            AppController.getInstance().bus().toObserverable().subscribe(new Action1<Object>() {
                @Override
                public void call(Object o) {
                    if (o instanceof String) {
                        //  int badgerValue = (int) o;
                        int badgerValue = Integer.valueOf((String) o);
                        Badger.sett(menu.findItem(R.id.action_cart), circleFactory).setCount(badgerValue);
                        Log.d(TAG, "new badger value " + badgerValue);
                    }
                }
            });
        }
        else {
            MenuItem item = menu.findItem(R.id.action_logout);
            item.setTitle("Login");
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (item.getItemId()) {
            case R.id.action_cart: {
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
                    Intent intent1 = new Intent(ProductDetailActivity.this, AddToCart.class);
                    intent1.putExtra(TAGS.FLAG, "true");
                    startActivity(intent1);
                } else {
                    Intent i = new Intent(ProductDetailActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            }
            break;
            case R.id.action_order: {

            }
            return true;
            case R.id.action_wishlist: {
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {

                    Intent intent1 = new Intent(ProductDetailActivity.this, WishListActivity.class);
                    startActivity(intent1);
                } else {
                    Intent i = new Intent(ProductDetailActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            }
            break;
            case R.id.action_profile: {
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
                    Intent intent1 = new Intent(ProductDetailActivity.this, EditProfileActivity.class);
                    startActivity(intent1);
                } else {
                    Intent i = new Intent(ProductDetailActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            }

            break;

            case R.id.action_myAddress:
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {

                    Intent intent4 = new Intent(ProductDetailActivity.this, AddressActivity.class);
                    intent4.putExtra(TAGS.FLAG, "false");
                    startActivity(intent4);
                }else {
                    Intent i = new Intent(ProductDetailActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }


                break;
            case R.id.action_changePass:
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
                    changePassword1();
                }
                else {
                    Intent i = new Intent(ProductDetailActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }


                break;
            case R.id.action_logout: {
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
                    LoginManager.getInstance().logOut();
                    AppController.getInstance().getPreferenceEditor().putString(TAGS.JSON_USER_DATA, "").commit();
                    Intent intent2 = new Intent(ProductDetailActivity.this, LoginActivity.class);
                    startActivity(intent2);
                    finish();
                }
                else {
                    Intent i = new Intent(ProductDetailActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            }

            break;
            default:
                if (id == R.id.ic_dots) {
//do nothing
                } else {
                    onBackPressed();
                }

        }
        return super.onOptionsItemSelected(item);
    }

    private void changePassword1() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ProductDetailActivity.this);
// ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.password_activity, null);
        dialogBuilder.setView(dialogView);

        final EditText currentpass = (EditText) dialogView.findViewById(R.id.currentPassword);
        final EditText changepass = (EditText) dialogView.findViewById(R.id.ChangePassword);
        AppCompatButton btnchangepass = (AppCompatButton) dialogView.findViewById(R.id.btn_chng);


        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.setTitle("Change Password");
        alertDialog.show();
        btnchangepass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation(changepass)) {
                    String currentpassword = currentpass.getText().toString();
                    String changedpassword = changepass.getText().toString();
                    AppController.getInstance().getRetrofitServices().changepassword(uid, currentpassword, changedpassword).enqueue(new Callback<DataForCurrentPassword>() {
                        @Override
                        public void onResponse(Call<DataForCurrentPassword> call, Response<DataForCurrentPassword> response) {
                            DataForCurrentPassword dataForCurrentPassword = response.body();
                            if (dataForCurrentPassword.getStatus().equalsIgnoreCase("success")) {
                                Snackbar snackbar = Snackbar.make(coLay, dataForCurrentPassword.getMessage(), Snackbar.LENGTH_LONG);
                                snackbar.show();
                                alertDialog.dismiss();
                            } else {
                                Snackbar snackbar = Snackbar.make(coLay, dataForCurrentPassword.getMessage(), Snackbar.LENGTH_LONG);
                                snackbar.show();
                                alertDialog.dismiss();
                            }

                        }

                        @Override
                        public void onFailure(Call<DataForCurrentPassword> call, Throwable t) {
                            Toast.makeText(ProductDetailActivity.this, "Server error can't download the categories", Toast.LENGTH_LONG).show();
                            finish();
                        }
                    });
                }
            }
        });

    }

    private boolean validation(EditText changepass) {
        boolean validation = true;
        String password = changepass.getText().toString();
        if (TextUtils.isEmpty(password)) {
            Snackbar snackbar = Snackbar.make(coLay, getResources().getString(R.string.error_empty_password), Snackbar.LENGTH_LONG);
            View view = snackbar.getView();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            snackbar.show();
            return false;
        }
//if no error found
        return validation;
    }

    private void detDiscription(final DataForProductDetail.ResponseBean.ProductDetailBean product_detail1) {
        libelleProduit.setText(product_detail1.getProduct_name());
        String price = product_detail1.getPrice();
        int listPrice = Integer.parseInt(price);

        product_id = product_detail1.getProduct_id();

        String discount = product_detail1.getDiscount();
        int discount_int = Integer.parseInt(discount);
        int discountPrice = listPrice - ((listPrice * discount_int) / 100);
        String my_discount = "" + discountPrice;
        if (discount.equals("0")) {
            dicount_stock.setVisibility(View.GONE);
            percent.setVisibility(View.GONE);
            String formatter = AppController.getInstance().decimalValue(Integer.parseInt(price));
            price_Stock.setText("INR " + formatter);
        } else {
            dicount_stock.setText(price);
            dicount_stock.setPaintFlags(dicount_stock.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            percent.setText("(" + discount + "%)");
            String formatter = AppController.getInstance().decimalValue(Integer.parseInt(my_discount));
            price_Stock.setText("INR " + formatter);
        }

        //for description
        if (product_detail1.getDescription().equals("")) {
            tvDiscription.setVisibility(View.GONE);
        }
        //for tips
        if (product_detail1.getTips().equals("")) {
            tvTips.setVisibility(View.GONE);
        }
        //for ingrident
        if (product_detail1.getIngrdients().equals("")) {
            tvIngride.setVisibility(View.GONE);
        }
        //for shooping
        if (product_detail1.getShopping_return().equals("")) {
            tvShoppingReturn.setVisibility(View.GONE);
        } else {

            final List<String> strings=new ArrayList<>();

            description = product_detail1.getDescription();
            strings.add(description);
            tips = product_detail1.getTips();
            strings.add(tips);
            ingrdients = product_detail1.getIngrdients();
            strings.add(ingrdients);
            shopping_return = product_detail1.getShopping_return();
            strings.add(shopping_return);

            tvDiscription.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    showDescription(description,0,strings);
                }
            });
            tvTips.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    showDescription(tips, 1,strings);
                }
            });
            tvIngride.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    showDescription(ingrdients, 2,strings);
                }
            });
            tvShoppingReturn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    showDescription(shopping_return, 3,strings);
                }
            });

        }
    }

    private void showDescription(String product_discription, int intvalue, List<String> strings) {
        Intent intent=new Intent(ProductDetailActivity.this,DesciptionFragment.class);
        intent.putExtra(TAGS.TAB_VALUES, product_discription);
        intent.putExtra(TAGS.TAB_VALUES_ID, intvalue);
        intent.putStringArrayListExtra(TAGS.TAB_VALUES_LIST, (ArrayList<String>) strings);
        startActivity(intent);
    }

    private void reviewShown(final List<DataForProductDetail.ResponseBean.ReviewBean> review) {

         int size = review.size();
        tvReview4.setText("Read "+size+"  Reviews");
        tvReview4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             Intent intent=new Intent(ProductDetailActivity.this,ReviewViewActivity.class);
             startActivity(intent);
            }
        });



            String rating = null;
            float abc;
            for (DataForProductDetail.ResponseBean.ReviewBean reviewBean : review) {
                rating = reviewBean.getRating();
                tvReview.setText(rating);
            }
                  ratinReview.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                    final int numStars = ratingBar.getNumStars();
                    final float rating = ratingBar.getRating();
                    // tvReview1.setText(" " + rating + "/" + numStars);
                    //  tvReview1.setText(String.valueOf(rating)+ "/" + numStars);
                    //     showDialog(String.valueOf(rating));
                    if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {

                        showDialog(rating);
                    }
                    else {
                        Intent i = new Intent(ProductDetailActivity.this, LoginActivity.class);
                        startActivity(i);
                    }

                }
            });


        }

    private void sliderViewAdpter(List<DataForProductDetail.ResponseBean.ProductColorShapeBean> product_color_shape) {
        colorOptionList = new ArrayList<>();
        for (DataForProductDetail.ResponseBean.ProductColorShapeBean bean : product_color_shape) {
            CircleImageView colorPickerOptionPrimary = newColorPickerOption(bean.getColor_shape_code());
            //add newly created option to colorOptionList
            colorOptionList.add(colorPickerOptionPrimary);
            //add newly created option to horizontal scroll view
            llHsView.addView(colorPickerOptionPrimary);
        }


    }

    private CircleImageView newColorPickerOption(String colorRes) {

        //create new circular imageview
        CircleImageView img = new CircleImageView(this);
        img.setTag("color_" + objectCounter);
        objectCounter = ++objectCounter;


        //set layout params [width, height, circleMargin]
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(circleDimension, circleDimension);
        params.setMargins(circleMargin, 0, circleMargin, 0);
        img.setLayoutParams(params);

        //set fill color it will be used which check drawable will be added on item click
        img.setFillColor(Color.parseColor(colorRes));
        //set color as image drawable because set fill do not work unless a drawable is set
        img.setImageDrawable(new ColorDrawable(Color.parseColor(colorRes)));
        //set border
        // img.setBorderWidth(3);

        img.setOnClickListener(this);
        return img;
    }

    private void imageViewAdpater(List<DataForProductDetail.ResponseBean.ProductImagesBean> product_images) {
        viewPager.setScrollDurationFactor(7);
        mAdapter = new ImageViewPagerDetail(ProductDetailActivity.this, product_images);
        viewPager.setAdapter(mAdapter);
        viewPager.setInterval(10000);
        viewPager.setCycle(true);
        indicator.setViewPager(viewPager);
        viewPager.startAutoScroll();

    }

    @OnClick({R.id.add_to_cart, R.id.img_minus, R.id.img_plus, R.id.view_bag})
    public void onClickButterKnife(View view) {
        switch (view.getId()) {
            case R.id.add_to_cart:
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
                    String color_shape_id = AppController.getInstance().getPreference().getString(TAGS.COLOR_SHADES_ID, "");
                    String json_string = AppController.getInstance().getPreference().getString(TAGS.JSON_USER_DATA, "");
                    DataForLogin dataForLogin = AppController.getInstance().getGson().fromJson(json_string, DataForLogin.class);
                    DataForLogin.ResponseBean response = dataForLogin.getResponse();
                    String uid = response.getUid();
                    if (!color_shape_id.equals("")) {
                        addCartValues(uid, product_id, count, color_shape_id);
                        AppController.getInstance().getPreferenceEditor().putString(TAGS.COLOR_SHADES_ID, "").commit();


                    } else {
                        Snackbar snackbar = Snackbar.make(coLay, "Please Select Color", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }

                } else {
                    Intent intent = new Intent(ProductDetailActivity.this, LoginPage.class);
                    startActivity(intent);
                }


                break;
            case R.id.img_minus:
                if (count != 1) {
                    count--;
                }
                tvCount.setText("" + count);
                break;
            case R.id.img_plus:
                count++;
                tvCount.setText("" + count);
                break;
            case R.id.view_bag:
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
                    Intent intent1 = new Intent(ProductDetailActivity.this, AddToCart.class);
                    intent1.putExtra(TAGS.FLAG, "true");
                    startActivity(intent1);
                } else {
                    Intent i = new Intent(ProductDetailActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
               break;
        }
    }

    private void addCartValues(String uid, String product_id, int count, String color_shape_id) {
        String count_value = "" + count;
        AppController.getInstance().getRetrofitServices().addToCart(uid, product_id, count_value, color_shape_id).enqueue(new Callback<DataForAddToCart>() {
            @Override
            public void onResponse(Call<DataForAddToCart> call, Response<DataForAddToCart> response) {
                DataForAddToCart jsonforcart = response.body();
                if (jsonforcart.getStatus().equalsIgnoreCase("success")) {
                    //get total record from freperence
                    int totalRecord = Integer.parseInt(AppController.getInstance().getPreference().getString(TAGS.TOATAL_RECORD, "0"));
                    String updatedRecord = String.valueOf(totalRecord + 1);
                    //increment total record value in preference
                    AppController.getInstance().getPreferenceEditor().putString(TAGS.TOATAL_RECORD, updatedRecord).commit();
                    //update cart bus with integer value
                    AppController.getInstance().bus().send(updatedRecord);
                    viewBag.setVisibility(View.VISIBLE);
                    addToCart.setVisibility(View.GONE);
                } else {
                    Toast.makeText(ProductDetailActivity.this, jsonforcart.getMessage(), Toast.LENGTH_LONG).show();

                }


            }

            @Override
            public void onFailure(Call<DataForAddToCart> call, Throwable t) {
                Toast.makeText(ProductDetailActivity.this, "Server error can't download the categories", Toast.LENGTH_LONG).show();
                finish();
            }
        });
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        for (int i = 0; i < dotsCount; i++) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                dots[i].setImageDrawable(getDrawable(R.drawable.nonselecteditem_dot));
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dots[position].setImageDrawable(getDrawable(R.drawable.selecteditem_dot));
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        autoUnsubBus();
        busSubscription = (Subscriber) AppController.getInstance().bus().toObserverable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        new Action1<Object>() {
                            @Override
                            public void call(Object o) {
                                handlerBus(o);
                            }
                        }
                );
    }

    private void handlerBus(Object o) {
        if (o instanceof Events.Message) {
            //   tvContent.setText(((Events.Message) o).message);
//            ((Events.Message) o).message);
            switch (item.getItemId()) {
                case R.id.action_cart:
                    badge = Badger.sett(item, null);
                    int i = badge.getCount() + 1;
                    badge.setCount(i);
            }
        }
    }

    private void autoUnsubBus() {
        if (busSubscription != null && !busSubscription.isUnsubscribed()) {
            busSubscription.unsubscribe();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        autoUnsubBus();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (flagstring.equalsIgnoreCase("false")) {
            if (mListener != null) {
                mListener.onUpdate();
            }
        } else {
            this.finish();
        }
    }

    //for Review
    private void showDialog(float s) {
        final float rating = s;
        finalRating = String.valueOf(rating);

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setCancelable(true);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.rate_activity, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.setTitle("Product Review");
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(alertDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        MaterialRatingBar materialRatingBar = (MaterialRatingBar) dialogView.findViewById(R.id.rating);
        materialRatingBar.setRating(rating);
        materialRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                float rating1 = ratingBar.getRating();
                finalRating = String.valueOf(rating1);
            }

        });
        final EditText editTitle = (EditText) dialogView.findViewById(R.id.et_tilte);
        final EditText editComment = (EditText) dialogView.findViewById(R.id.et_comment);
        final CoordinatorLayout diloglayout = (CoordinatorLayout) dialogView.findViewById(R.id.diloglayout);
        Button btn = (Button) dialogView.findViewById(R.id.btn_submit);

        final String title = editTitle.getText().toString();
        final String comment = editComment.getText().toString();
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validationfortext(diloglayout, editTitle, editComment)) {
                    uploadReview(finalRating, editTitle, editComment, alertDialog);
                }
            }
        });
        alertDialog.show();
        alertDialog.getWindow().setAttributes(lp);
    }

    private void uploadReview(String finalRating, EditText ettitle, EditText etcomment, final AlertDialog alertDialog) {
        String title = ettitle.getText().toString();
        String comment = etcomment.getText().toString();

        Log.e("this", "uid" + uid + "\nproduct_id" + product_id + "\nRAting" + finalRating + "\ntitle" + title + "\ncomment" + comment);
        AppController.getInstance().getRetrofitServices().add_review(uid, product_id, title, comment, finalRating).enqueue(new Callback<DataForReview>() {
            @Override
            public void onResponse(Call<DataForReview> call, Response<DataForReview> response) {
                DataForReview body = response.body();
                Log.e("this", body.getMessage());
                Toast.makeText(ProductDetailActivity.this, body.getMessage(), Toast.LENGTH_LONG).show();
                alertDialog.dismiss();
                if (thisClassListener != null) {
                    thisClassListener.onUpdateProductDetail();
                }
            }

            @Override
            public void onFailure(Call<DataForReview> call, Throwable t) {
                Toast.makeText(ProductDetailActivity.this, "Server error in Review Upload", Toast.LENGTH_LONG).show();
                finish();
            }
        });

    }

    private boolean validationfortext(CoordinatorLayout diloglayout, EditText editTitle, EditText editComment) {
        boolean validation = true;
        String title = editTitle.getText().toString();
        String comment = editComment.getText().toString();
        if (TextUtils.isEmpty(title)) {
            Snackbar snackbar = Snackbar.make(diloglayout, getResources().getString(R.string.error_empty_address), Snackbar.LENGTH_LONG);
            View view = snackbar.getView();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            snackbar.show();
            return false;
        }
        if (TextUtils.isEmpty(comment)) {
            Snackbar snackbar = Snackbar.make(diloglayout, getResources().getString(R.string.error_empty_address), Snackbar.LENGTH_LONG);
            View view = snackbar.getView();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            snackbar.show();
            return false;
        }
//if no error found
        return validation;
    }

    @Override
    public void onClick(View v) {

        if (v instanceof CircleImageView) {
            switchSelection(v);
        }
    }

    /*
    * Convert DP into Pixel
    * */
    private int getPixelFromDP(int dpValue) {
        int px = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dpValue,
                getResources().getDisplayMetrics()
        );
        return px;
    }

    private void switchSelection(final View v) {

        CircleImageView currentSelectedColorOption = (CircleImageView) v;
        DataForProductDetail.ResponseBean.ProductColorShapeBean currentColorShapeBean = product_color_shape.get(colorOptionList.indexOf(currentSelectedColorOption));
        roundedCornerFigrprint.setImageDrawable(new ColorDrawable(Color.parseColor(currentColorShapeBean.getColor_shape_code())));
        String color_shape_id = currentColorShapeBean.getColor_shape_id();
        AppController.getInstance().getPreferenceEditor().putString(TAGS.COLOR_SHADES_ID, color_shape_id).commit();

        if (previouslySelectedColorOption == null) {
            currentSelectedColorOption.setImageResource(R.drawable.check);
        } else {
            //get product color shape from model for this particular option
            DataForProductDetail.ResponseBean.ProductColorShapeBean previousColorShapeBean = product_color_shape.get(colorOptionList.indexOf(previouslySelectedColorOption));
            //reset previously selected option
            previouslySelectedColorOption.setImageDrawable(new ColorDrawable(Color.parseColor(previousColorShapeBean.getColor_shape_code())));
            //mark currently selected option
            currentSelectedColorOption.setImageResource(R.drawable.check);
        }
        //update selected color option with current color option
        previouslySelectedColorOption = currentSelectedColorOption;
        hsView.smoothScrollTo(v.getLeft() - circleMargin, v.getTop());
    }


    public interface OnUpdateListener {
        void onUpdate();
    }

    public interface OnUpdateListenerProductDetail {
        void onUpdateProductDetail();
    }

}
