package com.etoile.makeupbarr.rest;

import com.etoile.makeupbarr.model.DataForAddRecentView;
import com.etoile.makeupbarr.model.DataForAddRemoveWishList;
import com.etoile.makeupbarr.model.DataForAddToCart;
import com.etoile.makeupbarr.model.DataForAddress;
import com.etoile.makeupbarr.model.DataForBanner;
import com.etoile.makeupbarr.model.DataForCurrentPassword;
import com.etoile.makeupbarr.model.DataForDeleteCart;
import com.etoile.makeupbarr.model.DataForDrawerCategories;
import com.etoile.makeupbarr.model.DataForEditProfile;
import com.etoile.makeupbarr.model.DataForFacebook;
import com.etoile.makeupbarr.model.DataForForgotPassword;
import com.etoile.makeupbarr.model.DataForHashGenration;
import com.etoile.makeupbarr.model.DataForLogin;
import com.etoile.makeupbarr.model.DataForOTPVerify;
import com.etoile.makeupbarr.model.DataForPaymentHash;
import com.etoile.makeupbarr.model.DataForProductDetail;
import com.etoile.makeupbarr.model.DataForProductList;
import com.etoile.makeupbarr.model.DataForRecentVIew;
import com.etoile.makeupbarr.model.DataForRegister;
import com.etoile.makeupbarr.model.DataForRemoveRecentView;
import com.etoile.makeupbarr.model.DataForResetPassword;
import com.etoile.makeupbarr.model.DataForReview;
import com.etoile.makeupbarr.model.DataForSubCategory;
import com.etoile.makeupbarr.model.DataForTotalCartItem;
import com.etoile.makeupbarr.model.DataForUpdateCart;
import com.etoile.makeupbarr.model.DataForViewAddCart;
import com.etoile.makeupbarr.model.DataForViewAddress;
import com.etoile.makeupbarr.model.DataForViewWishList;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface ApiInterface {
    @GET("login_info/{user}/{password}")
    Call<DataForLogin> login(@Path("user") String email, @Path("password") String password);


    @GET("registration/")
    Call<DataForRegister> register(@Query("fname") String fname,
                                   @Query("email") String email, @Query("phone") String phone,
                                   @Query("password") String password);

    @GET("facebook_login/")
    Call<DataForFacebook> loginwithfacebbok(@Query("fname") String fname, @Query("lname") String lname,
                                            @Query("email") String email);

    @GET("forgot_password/")
    Call<DataForForgotPassword> forgotPassword(@Query("email") String email);

    @GET("otp_verify/")
    Call<DataForOTPVerify> otpVerify(@Query("email") String email_id, @Query("otp") String otp);

    @GET("reset_password/")
    Call<DataForResetPassword> resetPassword(@Query("email") String email, @Query("password") String newpassword);

    @ FormUrlEncoded
    @POST("edit_profile/")
    Call<DataForEditProfile> editProfile(@Field("uid") String uid, @Field("fname") String fname ,
                                         @Field("phone") String phone);


    @ FormUrlEncoded
    @POST("product_list")
    Call<DataForProductList> productlist(@Field("category_id") String category_id,
                                         @Field("sub_category_id") String sub_category_id,
                                         @Field("min_price") String min_price,
                                         @Field("max_price") String max_price,
                                         @Field("brand_id") String brand_id,
                                         @Field("sort") String sort,
                                         @Field("uid") String uid,
                                         @Field("new_in_week") String new_in_week

    );

    /*      sub_category_id:2
        min_price:0
        max_price:1000
        brand_id:1
        sort:discount*/


    @GET("category/")
    Call<DataForDrawerCategories> category();


    @ FormUrlEncoded
    @POST("wishlist")
    Call<DataForAddRemoveWishList> wishlist(@Field("product_id") String product_id, @Field("uid") String uid);

    @ FormUrlEncoded
    @POST("product_detail")
    Call<DataForProductDetail> product(@Field("product_id") String product_id, @Field("uid") String uid);

    @ FormUrlEncoded
    @POST("view_wishlist")
    Call<DataForViewWishList> viewWishlist(@Field("uid") String uid);


    @ FormUrlEncoded
    @POST("recent_view_all")
    Call<DataForRecentVIew> viewRecentProduct(@Field("uid") String uid);

    @ FormUrlEncoded
    @POST("recent_view_remove")
    Call<DataForRemoveRecentView> removeRecentProduct(@Field("uid") String uid, @Field("product_id") String product_id);


    @ FormUrlEncoded
    @POST("add_to_cart")
    Call<DataForAddToCart> addToCart(@Field("uid") String uid, @Field("product_id") String product_id,
                                     @Field("qty") String qty, @Field("shape_id") String shape_id);


    @GET("home_bannar")
    Call<DataForBanner> banner();

    @ FormUrlEncoded
    @POST("view_cart")
    Call<DataForViewAddCart> viewaddToCart(@Field("uid") String uid);

    @ FormUrlEncoded
    @POST("remove_to_cart")
    Call<DataForDeleteCart> deletecart(@Field("uid") String uid, @Field("product_id") String product_id);


    @ FormUrlEncoded
    @POST("change_password")
    Call<DataForCurrentPassword> changepassword(@Field("uid") String uid,
                                                @Field("current_password") String current_password,
                                                @Field("new_password") String new_password);

    @ FormUrlEncoded
    @POST("address")//add_address
    Call<DataForAddress> address(@Field("uid") String uid,
                                 @Field("pincode") String pincode,
                                 @Field("name") String name,
                                 @Field("address") String address,
                                 @Field("locality") String locality,
                                 @Field("city") String city,
                                 @Field("state") String state,
                                 @Field("mobile") String mobile,
                                 @Field("address_id") String address_id
    );


    @ FormUrlEncoded
    @POST("recent_view_insert")//add_RecentView_product
    Call<DataForAddRecentView> recentView(@Field("uid") String uid,
                                       @Field("product_id") String product_id
    );

    @ FormUrlEncoded
    @POST("recent_view_insert")//add_RecentView_product
    Call<DataForAddRecentView> recentViewSkip(@Field("product_id") String product_id
    );



    @ FormUrlEncoded
    @POST("view_address")
    Call<DataForViewAddress> viewaddress(@Field("uid") String uid);

    @ FormUrlEncoded
    @POST("update_address")
    Call<DataForDrawerCategories> updateAddress(@Field("uid") String uid,
                                                @Field("pincode") String pincode,
                                                @Field("name") String name,
                                                @Field("address") String address,
                                                @Field("locality") String locality,
                                                @Field("city") String city,
                                                @Field("state") String state,
                                                @Field("mobile") String mobile,
                                                @Field("address_id") String address_id);

    @ FormUrlEncoded
    @POST("cart_item")
    Call<DataForTotalCartItem> carttotal(@Field("uid") String uid);


    @ FormUrlEncoded
    @POST("payment_detail")
    Call<DataForHashGenration> payment_detail(@Field("fname") String fname,
                                              @Field("phone") String phone, @Field("email") String email,
                                              @Field("address") String address, @Field("zip_code") String zip_code,
                                              @Field("city") String city, @Field("state_id") String state_id,
                                              @Field("amount") String amount);

    /* 'key','salt','amount','txnid','firstname','phone','email','productinfo'*/

    @FormUrlEncoded
    @POST("get_hash_key")
    Call<DataForPaymentHash> getPaymentHash(@Field("key") String key, @Field("salt") String salt,
                                            @Field("amount") String amount, @Field("txnid") String txnid,
                                            @Field("firstname") String firstname, @Field("phone") String phone,
                                            @Field("email") String email, @Field("productinfo") String productinfo
    );


    @GET("view_sub_category")
    Call<DataForSubCategory> view_sub_category();

    @FormUrlEncoded
    @POST("add_review")
    Call<DataForReview> add_review(@Field("uid") String uid, @Field("product_id") String product_id,
                                   @Field("title") String title, @Field("comment") String comment, @Field("rating") String rating);
    @FormUrlEncoded
    @POST("update_to_cart   ")
    Call<DataForUpdateCart> update_to_cart(@Field("uid") String uid,
                                           @Field("product_id") String product_id,
                                           @Field("qty") String qty);

}

