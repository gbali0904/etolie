package com.etoile.makeupbarr.Utility;

/**
 * Created by apple on 17/09/16.
 */
public class TAGS {
    public static final String USER_JSON = "user_json";
    public static final String UID = "user_id";
    public static final String JSON_USER_DATA = "json_login_data";
    public static final String JSON_REGISTER_DATA = "json_register_data";
    public static final String PRODUCT_LIST = "product_list";
    public static final String CATEGORY_ID = "drawer_catergory_id";
    public static final String SUB_CATEGORY_ID = "sub_category_id";
    public static final String PRODUCT_ID = "product_id";
    public static final String WISHLIST = "my_wish_list";
    public static final String COLOR_SHADES_ID = "color_shades_id";
    public static final String JSON_CATEGORY = "json_category_list";
    public static final String WISHLISTDATA = "json_for_wishlist";
    public static final String ADDRESSID_JSON_DATA = "json_address_data";
    public static final String USER_DATA = "user_data_for_address";
    public static final String TOATAL_RECORD = "totalk_cart_value";
    public static final String TOTAL_PRICE = "total+price";
    public static final String FLAG = "false_value";
    public static final String SUB_CATEGORY_LIST = "sub_cateory_list";
    public static final String BRAND_ID = "brand_id";
    public static final String SHORTEDLIST = "short_list";
    public static final String JSON_FOR_PRODUCT_DETAIL = "image_view_list";
    public static final String CATEGORY_NAME = "categorty_name";
    public static final String PRODUCTDETAIL = "product_detail";
    public static final String TAB_VALUES = "tab_values";
    public static final String TAB_VALUES_ID = "tab_id";
    public static final String TAB_VALUES_LIST = "tab_value_list";
    public static final String TAB_VALUES_BOOLEAN = "flag";
    public static String Email_ID="email_id";

    public static String RECENT_ADD ="add_recent";

    public static final String IP = "http://passportconsultantonline.com/";
    public static final String IP2 = "https://www.machinesdealer.com/Api/v1/";
    public static final String FIREBASE_URL = "https://work-scheduling-23a32.firebaseio.com/";
    public static final String FIREBASE_ID = "firebase_id";
    //public  static final String FIREBASE_message = "message";

    public static final String ADMINID = "adminid";
    public static String USER_ID = "uid";
    public static String USER_EMAIL = "email";
    public static String NODE_REG = "register";
    public static String NODE_LOG = "login";
    public static String NODE_TASK = "task_emp";
    public static String NODE_PROFILE = "view_profile";
    public static String PASS_KEY = "password";
    public static String USER_NAME = "name";
    public static String USER_MO = "phone";
    public static String USER_MOB = "mobile";
    public static String USER_IMG = "user_img";
    public static String USER_IM = "profile_img";
    public static String USER_TYPE = "user_type";
    public static String USER_ADD = "address";
    public static String NODE_EDIT_PROF = "edit_profile";
    public static String NODE_CNG_PASS = "password_change";

}
