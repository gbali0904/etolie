package com.etoile.makeupbarr.Utility;

import android.app.Application;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.model.DataForLogin;
import com.etoile.makeupbarr.rest.ApiInterface;
import com.google.gson.Gson;
import com.romainpiel.shimmer.Shimmer;
import com.romainpiel.shimmer.ShimmerTextView;

import java.text.DecimalFormat;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Scheduler;
import rx.schedulers.Schedulers;

public class AppController extends Application {

    public static final String TAG = AppController.class
            .getSimpleName();
    private static SharedPreferences sharedPreferences;
    private static AppController instance;

    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    private static AppController mInstance;
    private ProgressDialog progressDialog;
    private SharedPreferences pref;
    private Gson gson;
    private Scheduler defaultSubscribeScheduler;
    private RxBus bus;
    private ProgressDialog progressDialogImages;
    private Dialog mProgressDlg;
    private ShimmerTextView tv;
    private Shimmer shimmer;

    public static synchronized AppController getInstance() {

        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(AppController.this);
        //instantiate user from preferences
        String userData = AppController.getInstance().getPreference().getString(TAGS.USER_JSON, "");
        if (!userData.equalsIgnoreCase(""))
            DataForLogin.setInstance(userData);

        bus = new RxBus();
    }


    public RxBus bus() {
        return bus;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }


    public void showProgressDialog(Context context) {

        mProgressDlg = new Dialog(context);
        mProgressDlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(mProgressDlg.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        mProgressDlg.getWindow().setAttributes(lp);
        mProgressDlg.setContentView(R.layout.user_pass_dialog);
        tv = (ShimmerTextView) mProgressDlg.findViewById(R.id.shimmer_tv);

        if (shimmer != null && shimmer.isAnimating()) {
            shimmer.cancel();
        } else {
            shimmer = new Shimmer();
            shimmer.start(tv);
        }
        mProgressDlg.show();
    }

    public void hideProgressDialog() {
        if (mProgressDlg != null) {
            if (mProgressDlg.isShowing())
                mProgressDlg.dismiss();
        }

    }

    public void showProgressDialogforImages(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(getResources().getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void hideProgressDialogforImages() {
        if (progressDialog != null) {
            if (progressDialog.isShowing())
                progressDialog.dismiss();
        }
    }
    public SharedPreferences getPreference() {
        if (pref == null)
            pref = PreferenceManager.getDefaultSharedPreferences(AppController.this);
        return pref;
    }

    /**
     * This method is used to get instance of Default Shared preference editor
     * which can be used throughout the application.
     *
     * @return singleton instance of default shared preference eidtor
     */
    public SharedPreferences.Editor getPreferenceEditor() {
        if (pref == null)
            pref = PreferenceManager.getDefaultSharedPreferences(AppController.this);
        return pref.edit();
    }

    public static void savePreferences(String key, String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }
    public static String getPreferences(String key, String val) {
        String value = sharedPreferences.getString(key, val);
        return value;
    }
    public Gson getGson() {
        if (gson == null)
            gson = new Gson();
        return gson;
    }

    public ApiInterface getRetrofitServices() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder().connectTimeout(30, TimeUnit.SECONDS).readTimeout(30, TimeUnit.SECONDS).addInterceptor(logging);
        // add your other interceptors …

        // add logging as last interceptor
        httpClient.addInterceptor(logging);  // <-- this is the important line!
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(httpClient.build())
                .build();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        return apiInterface;
    }

    public Scheduler defaultSubscribeScheduler() {
        if (defaultSubscribeScheduler == null) {
            defaultSubscribeScheduler = Schedulers.io();
        }
        return defaultSubscribeScheduler;
    }
    public String decimalValue(int s1){
        DecimalFormat formatter = new DecimalFormat("#,###,###.00");
        String yourFormattedString = formatter.format(s1);
        return yourFormattedString;
    }


}
