package com.etoile.makeupbarr.login;

import android.app.SearchManager;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.Utility.CheckinUtility;
import com.etoile.makeupbarr.Utility.TAGS;
import com.etoile.makeupbarr.model.DataForEditProfile;
import com.etoile.makeupbarr.model.DataForLogin;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Gunjan on 25-12-2016.
 */
public class EditProfileActivity extends AppCompatActivity {
    private static final String TAG = EditProfileActivity.class.getSimpleName();
    private static OnUpdateListener mListener;
    @Bind(R.id.im_profile)
    CircleImageView imProfile;
    @Bind(R.id.editProfileName)
    AppCompatButton editProfileButton;
    @Bind(R.id.et_first_name)
    EditText etFirstName;
    @Bind(R.id.et_email)
    EditText etEmail;
    @Bind(R.id.et_mobileno)
    EditText etMobileno;
    @Bind(R.id.btn_save)
    Button btnSave;
    @Bind(R.id.myprofile)
    CoordinatorLayout coordinatorLayout;
    @Bind(R.id.toolbar)
    Toolbar toolbar;

    private String email;
    private String password;
    private String phone;
    private String fist_name;
    private DataForLogin dataForLogin;
    private DataForLogin.ResponseBean response;
    private String uid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.my_profile);
        ButterKnife.bind(this);
        toolbar = getToolbar();
        getUserProfileData();
        EditProfileActivity.setOnUpdateListener(new OnUpdateListener() {
            @Override
            public void onUpdate() {
                // Toast.makeText(EditProfileActivity.this, "Successfully  Closed\u200E", Toast.LENGTH_SHORT).show();
                etMobileno.setEnabled(false);
                etFirstName.setEnabled(false);
                editProfileButton.setVisibility(View.VISIBLE);
                btnSave.setVisibility(View.GONE);
                getUserProfileData();
            }
        });

    }

    private Toolbar getToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
        });
        return toolbar;
    }

    private void getUserProfileData() {
        String userdata = AppController.getInstance().getPreference().getString(TAGS.JSON_USER_DATA, "");
        dataForLogin = AppController.getInstance().getGson().fromJson(userdata, DataForLogin.class);
        response = dataForLogin.getResponse();
        etEmail.setText(response.getEmail());
        etFirstName.setText(response.getFname());
        etMobileno.setText(response.getPhone());

    }

    @OnClick({R.id.editProfileName, R.id.btn_save})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.editProfileName:
                //etEmail.setEnabled(true);
                etMobileno.setEnabled(true);
                etFirstName.setEnabled(true);
                editProfileButton.setVisibility(View.GONE);
                btnSave.setVisibility(View.VISIBLE);
                /*if (validation()) {
                    registerForUser();
                }*/
                break;
            case R.id.btn_save:
                if (validation()) {
                    registerForUser();
                }
                break;
        }
    }

    private void registerForUser() {
        AppController.getInstance().showProgressDialog(this);
        uid = response.getUid();
        Call<DataForEditProfile> mService = AppController.getInstance().getRetrofitServices().editProfile(uid, fist_name, phone);
        mService.enqueue(new Callback<DataForEditProfile>() {
            @Override
            public void onResponse(Call<DataForEditProfile> call, Response<DataForEditProfile> response) {
                AppController.getInstance().hideProgressDialog();
                DataForEditProfile loginJsonData = response.body();
                String jsonValueforEditProfile = AppController.getInstance().getGson().toJson(loginJsonData);
                AppController.getInstance().getPreferenceEditor().putString(TAGS.JSON_USER_DATA, jsonValueforEditProfile).commit();
                // Receiving side
                if (loginJsonData.getStatus().equals("success")) {//register successfull
                    Log.d(TAG, "login successful");
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, loginJsonData.getMessage(), Snackbar.LENGTH_LONG);
                    snackbar.show();
                    if (mListener != null) {
                        mListener.onUpdate();
                    }
               /*     Intent intent = new Intent(EditProfileActivity.this, SplashActivity.class);
                    startActivity(intent);
                    intent.putExtra(TAGS.SHOULD_START_FROM_LOGIN,true);
                    finish();*/
                } else {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, loginJsonData.getMessage(), Snackbar.LENGTH_LONG);
                    snackbar.show();

                }
            }

            @Override
            public void onFailure(Call<DataForEditProfile> call, Throwable t) {
                call.cancel();
                Toast.makeText(EditProfileActivity.this, "Please check your network connection and internet permission", Toast.LENGTH_LONG).show();
            }
        });

    }

    private boolean validation() {
        boolean validation = true;
        email = etEmail.getText().toString().trim();
        phone = etMobileno.getText().toString().trim();
        fist_name = etFirstName.getText().toString();
        //for First_name
        if (TextUtils.isEmpty(fist_name)) {
            Snackbar snackbar = Snackbar.make(coordinatorLayout, getResources().getString(R.string.error_first_name), Snackbar.LENGTH_LONG);
            View view = snackbar.getView();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            snackbar.show();
            return false;
        }
        //for phone number
        if (TextUtils.isEmpty(phone)) {
            Snackbar snackbar = Snackbar.make(coordinatorLayout, getResources().getString(R.string.error_mobile), Snackbar.LENGTH_LONG);
            View view = snackbar.getView();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            snackbar.show();
            return false;
        } else if (!CheckinUtility.isPhoneNumberValid(phone)) {
            Snackbar snackbar = Snackbar.make(coordinatorLayout, getResources().getString(R.string.error_invalid_mobile), Snackbar.LENGTH_LONG);
            View view = snackbar.getView();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            snackbar.show();
            return false;
        }

        //if no error found
        return validation;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.meni_with_dot, menu);
       /* MenuItem searchItem = menu.findItem(R.id.action_search);
        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(this.SEARCH_SERVICE);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(true); // Iconify the widget
        searchView.setOnQueryTextListener(this);*/
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
       this.finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    public static void setOnUpdateListener(OnUpdateListener listener) {

        mListener = listener;
    }

    interface OnUpdateListener {
        void onUpdate();
    }
}
