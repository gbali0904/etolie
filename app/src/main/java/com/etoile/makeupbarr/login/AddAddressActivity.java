package com.etoile.makeupbarr.login;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.Utility.CheckinUtility;
import com.etoile.makeupbarr.Utility.TAGS;
import com.etoile.makeupbarr.activity.AddressAPIActivity;
import com.etoile.makeupbarr.model.DataForLogin;
import com.etoile.makeupbarr.model.UserAddress;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by AndroidPC on 03-01-2017.
 */

public class AddAddressActivity extends AppCompatActivity {
    public static final String TAG = AddAddressActivity.class.getSimpleName();
    private static OnUpdateListener mListener;
    @Bind(R.id.et_pin)
    EditText etPin;
    @Bind(R.id.et_name)
    EditText etName;
    @Bind(R.id.et_address)
    EditText etAddress;
    @Bind(R.id.et_locality)
    EditText etLocality;
    @Bind(R.id.et_citydiss)
    EditText etCitydiss;
    @Bind(R.id.et_state)
    EditText etState;
    @Bind(R.id.phone_et)
    EditText phoneEt;
    @Bind(R.id.ll2)
    LinearLayout ll2;
    @Bind(R.id.btn_addressadd)
    AppCompatButton btnSignup;
    @Bind(R.id.myprofile)
    CoordinatorLayout myaddress;
    private String address;
    private String pin;
    private String myname;
    private String locality;
    private String city;
    private String state;
    private String phone;
    private String uid;
    private String address_id = "";
    private String flagstring;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_my_address);
        ButterKnife.bind(this);

        String json_string = AppController.getInstance().getPreference().getString(TAGS.JSON_USER_DATA, "");
        DataForLogin dataForLogin = AppController.getInstance().getGson().fromJson(json_string, DataForLogin.class);
        DataForLogin.ResponseBean response = dataForLogin.getResponse();
        uid = response.getUid();
        String addressset = AppController.getInstance().getPreference().getString(TAGS.USER_DATA, "");
        if (!addressset.isEmpty()) {
            UserAddress userAddress = AppController.getInstance().getGson().fromJson(addressset, UserAddress.class);
            etAddress.setText(userAddress.getAddress());
            etPin.setText(userAddress.getPincode());
            etName.setText(userAddress.getName());
            etLocality.setText(userAddress.getLocality());
            etCitydiss.setText(userAddress.getCity());
            etState.setText(userAddress.getState());
            phoneEt.setText(userAddress.getMobile());
            AppController.getInstance().getPreferenceEditor().putString(TAGS.USER_DATA, "").commit();
        }
    }

    @OnClick(R.id.btn_addressadd)
    public void onClick() {
        if (validation()) {
            address_id = AppController.getInstance().getPreference().getString(TAGS.ADDRESSID_JSON_DATA, "");
            AddressAPIActivity.addresstoserver(uid, pin, myname, address, locality, city, state, phone, address_id);
            this.finish();
           if (mListener != null) {
                mListener.onUpdate();
               }

            AppController.getInstance().getPreferenceEditor().putString(TAGS.ADDRESSID_JSON_DATA, " ").commit();
        }
    }
    private boolean validation() {
        boolean validation = true;
        address = etAddress.getText().toString().trim();
        pin = etPin.getText().toString().trim();
        myname = etName.getText().toString().trim();
        locality = etLocality.getText().toString().trim();
        city = etCitydiss.getText().toString().trim();
        state = etState.getText().toString().trim();
        phone = phoneEt.getText().toString().trim();
        if (TextUtils.isEmpty(pin)) {
            Snackbar snackbar = Snackbar.make(myaddress, getResources().getString(R.string.error_empty_address), Snackbar.LENGTH_LONG);
            View view = snackbar.getView();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            snackbar.show();
            return false;
        }
        if (TextUtils.isEmpty(myname)) {
            Snackbar snackbar = Snackbar.make(myaddress, getResources().getString(R.string.error_empty_address), Snackbar.LENGTH_LONG);
            View view = snackbar.getView();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            snackbar.show();
            return false;
        }
        if (TextUtils.isEmpty(address)) {
            Snackbar snackbar = Snackbar.make(myaddress, getResources().getString(R.string.error_empty_address), Snackbar.LENGTH_LONG);
            View view = snackbar.getView();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            snackbar.show();
            return false;
        }

        if (TextUtils.isEmpty(locality)) {
            Snackbar snackbar = Snackbar.make(myaddress, getResources().getString(R.string.error_empty_address), Snackbar.LENGTH_LONG);
            View view = snackbar.getView();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            snackbar.show();
            return false;
        }
        if (TextUtils.isEmpty(city)) {
            Snackbar snackbar = Snackbar.make(myaddress, getResources().getString(R.string.error_empty_address), Snackbar.LENGTH_LONG);
            View view = snackbar.getView();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            snackbar.show();
            return false;
        }
        if (TextUtils.isEmpty(state)) {
            Snackbar snackbar = Snackbar.make(myaddress, getResources().getString(R.string.error_empty_address), Snackbar.LENGTH_LONG);
            View view = snackbar.getView();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            snackbar.show();
            return false;
        }

        //for phone number
        if (TextUtils.isEmpty(phone)) {
            Snackbar snackbar = Snackbar.make(myaddress, getResources().getString(R.string.error_mobile), Snackbar.LENGTH_LONG);
            View view = snackbar.getView();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            snackbar.show();
            return false;
        } else if (!CheckinUtility.isPhoneNumberValid(phone)) {
            Snackbar snackbar = Snackbar.make(myaddress, getResources().getString(R.string.error_invalid_mobile), Snackbar.LENGTH_LONG);
            View view = snackbar.getView();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            snackbar.show();
            return false;
        }

        return validation;
    }


    public static void setOnUpdateListener(OnUpdateListener listener) {
        mListener = listener;
    }

    public interface OnUpdateListener {
        void onUpdate();
    }
}
