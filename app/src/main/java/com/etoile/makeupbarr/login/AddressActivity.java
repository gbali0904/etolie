package com.etoile.makeupbarr.login;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.Utility.TAGS;
import com.etoile.makeupbarr.adapter.RecyclerViewAdapterForAddressView;
import com.etoile.makeupbarr.model.DataForLogin;
import com.etoile.makeupbarr.model.DataForViewAddress;
import com.payUMoney.sdk.PayUmoneySdkInitilizer;
import com.payUMoney.sdk.SdkConstants;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by AndroidPC on 07-01-2017.
 */

public class AddressActivity extends AppCompatActivity {

    private static final String TAG = AddressActivity.class.getSimpleName();
    /*
       @Bind(R.id.et_address)
    */
    TextView etaddress;
    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;
    @Bind(R.id.layout)
    CoordinatorLayout layout;
    @Bind(R.id.payment)
    AppCompatButton payment;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    private String uid;
    private RecyclerViewAdapterForAddressView recyclerViewAdapter;
    private List<DataForViewAddress.AddressBean> address;
    private String flagstring;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_address);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if (bundle != null) {
            flagstring = bundle.getString(TAGS.FLAG);
        }

        if (flagstring.equalsIgnoreCase("false")) {
            payment.setVisibility(View.INVISIBLE);
        }
        toolbar = getToolbar();
        String json_string = AppController.getInstance().getPreference().getString(TAGS.JSON_USER_DATA, "");
        DataForLogin dataForLogin = AppController.getInstance().getGson().fromJson(json_string, DataForLogin.class);
        DataForLogin.ResponseBean response = dataForLogin.getResponse();
        uid = response.getUid();

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(AddressActivity.this));

        RecyclerViewAdapterForAddressView.setOnUpdateListener(new RecyclerViewAdapterForAddressView.OnUpdateListener() {
            @Override
            public void onUpdate() {
                // Toast.makeText(EditProfileActivity.this, "Successfully  Closed\u200E", Toast.LENGTH_SHORT).show();
                //      response1.clear();
                //address.clear();
                address.clear();
                viewAddress(uid);
                //     recyclerViewAdapter.notifyDataSetChanged();
            }
        });
        AddAddressActivity.setOnUpdateListener(new AddAddressActivity.OnUpdateListener() {
            @Override
            public void onUpdate() {
                // Toast.makeText(EditProfileActivity.this, "Successfully  Closed\u200E", Toast.LENGTH_SHORT).show();
                //      response1.clear();
                //address.clear();
                address.clear();
                viewAddress(uid);
                recyclerViewAdapter.notifyDataSetChanged();
            }
        });
        addressToServer();
        viewAddress(uid);

    }

    private Toolbar getToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportFragmentManager().addOnBackStackChangedListener(new android.support.v4.app.FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
        });
        return toolbar;
    }

    private void viewAddress(String uid) {

        AppController.getInstance().showProgressDialog(this);
        AppController.getInstance().getRetrofitServices().viewaddress(uid).enqueue(new Callback<DataForViewAddress>() {
            @Override
            public void onResponse(Call<DataForViewAddress> call, Response<DataForViewAddress> response) {
                AppController.getInstance().hideProgressDialog();
                DataForViewAddress categoryjson = response.body();

                if (categoryjson.getStatus().equalsIgnoreCase("success")) {
                    address = categoryjson.getAddress();
                    recyclerViewAdapter = new RecyclerViewAdapterForAddressView(AddressActivity.this, address, layout, payment, flagstring);
                    recyclerView.setAdapter(recyclerViewAdapter);
                }
            }

            @Override
            public void onFailure(Call<DataForViewAddress> call, Throwable t) {
                Toast.makeText(AddressActivity.this, "Server error can't download the categories", Toast.LENGTH_LONG).show();
                finish();
            }
        });

    }

    private void addressToServer() {
        etaddress = (TextView) findViewById(R.id.et_address);
        etaddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AddressActivity.this, AddAddressActivity.class);
                intent.putExtra(TAGS.FLAG, "false");
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            Log.i("MainActivity", "popping backstack");
            fm.popBackStack();
        } else {
            Log.i("MainActivity", "nothing on backstack, calling super");
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        RecyclerViewAdapterForAddressView.setOnUpdateListener(new RecyclerViewAdapterForAddressView.OnUpdateListener() {
            @Override
            public void onUpdate() {
                // Toast.makeText(EditProfileActivity.this, "Successfully  Closed\u200E", Toast.LENGTH_SHORT).show();
                //      response1.clear();
                //address.clear();
                address.clear();
                viewAddress(uid);
                recyclerViewAdapter.notifyDataSetChanged();
            }
        });
        AddAddressActivity.setOnUpdateListener(new AddAddressActivity.OnUpdateListener() {
            @Override
            public void onUpdate() {
                // Toast.makeText(EditProfileActivity.this, "Successfully  Closed\u200E", Toast.LENGTH_SHORT).show();
                //      response1.clear();
                //address.clear();
                address.clear();
                viewAddress(uid);
                recyclerViewAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        RecyclerViewAdapterForAddressView.setOnUpdateListener(new RecyclerViewAdapterForAddressView.OnUpdateListener() {
            @Override
            public void onUpdate() {
                // Toast.makeText(EditProfileActivity.this, "Successfully  Closed\u200E", Toast.LENGTH_SHORT).show();
                //      response1.clear();
                //address.clear();
                address.clear();
                viewAddress(uid);
                recyclerViewAdapter.notifyDataSetChanged();
            }
        });
        AddAddressActivity.setOnUpdateListener(new AddAddressActivity.OnUpdateListener() {
            @Override
            public void onUpdate() {
                // Toast.makeText(EditProfileActivity.this, "Successfully  Closed\u200E", Toast.LENGTH_SHORT).show();
                //      response1.clear();
                //address.clear();
                address.clear();
                viewAddress(uid);
                recyclerViewAdapter.notifyDataSetChanged();
            }
        });
    }



    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == PayUmoneySdkInitilizer.PAYU_SDK_PAYMENT_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Log.i(TAG, "Success - Payment ID : " + data.getStringExtra(SdkConstants.PAYMENT_ID));
                String paymentId = data.getStringExtra(SdkConstants.PAYMENT_ID);
                showDialogMessage("Payment Success Id : " + paymentId);
            }

            else if (resultCode == RESULT_CANCELED) {
                Log.i(TAG, "failure");
                showDialogMessage("cancelled");
            }

            else if (resultCode == PayUmoneySdkInitilizer.RESULT_FAILED) {
                Log.i("app_activity", "failure");

                if (data != null) {
                    if (data.getStringExtra(SdkConstants.RESULT).equals("cancel")) {

                    } else {
                        showDialogMessage("failure");
                    }
                }
                //Write your code if there's no result
            } else if (resultCode == PayUmoneySdkInitilizer.RESULT_BACK) {
                Log.i(TAG, "User returned without login");
                showDialogMessage("User returned without login");
            }
        }
    }

    private void showDialogMessage(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(TAG);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();

    }
}
