package com.etoile.makeupbarr.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.Utility.TAGS;
import com.etoile.makeupbarr.activity.MainActivity;
import com.etoile.makeupbarr.activity.WishListActivity;
import com.etoile.makeupbarr.model.DataForCurrentPassword;
import com.etoile.makeupbarr.model.DataForLogin;
import com.facebook.login.LoginManager;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Gunjan on 25-12-2016.
 */
public class ProfileActivity extends AppCompatActivity {

    @Bind(R.id.im_profile)
    CircleImageView imProfile;
    @Bind(R.id.editProfileName)
    TextView editProfileName;
    @Bind(R.id.myprofile)
    TextView myprofile;
    @Bind(R.id.myorder)
    TextView myorder;
    @Bind(R.id.wishlist)
    TextView wishlist;
    @Bind(R.id.handPickedForYou)
    TextView handPickedForYou;
    @Bind(R.id.savecads)
    TextView savecads;
    @Bind(R.id.coupons)
    TextView coupons;
    @Bind(R.id.mycashback)
    TextView mycashback;
    @Bind(R.id.myaddress)
    TextView myaddress;
    @Bind(R.id.logout)
    TextView logout;
    @Bind(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @Bind(R.id.viw_profil)
    LinearLayout viwProfil;
    @Bind(R.id.et_address)
    TextView etchangepassword;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    private DataForLogin dataForLogin;
    private DataForLogin.ResponseBean response;
    private String uid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.my_account);
        ButterKnife.bind(this);
        toolbar = getToolbar();
        String string = AppController.getInstance().getPreference().getString(TAGS.JSON_USER_DATA, "");
        dataForLogin = AppController.getInstance().getGson().fromJson(string, DataForLogin.class);
        response = dataForLogin.getResponse();
        uid = response.getUid();
        editProfileName.setText(response.getFname().toUpperCase());
    }

    private Toolbar getToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
        });
        return toolbar;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(ProfileActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @OnClick({R.id.editProfileName, R.id.myprofile, R.id.myorder, R.id.wishlist, R.id.handPickedForYou,
            R.id.savecads, R.id.coupons, R.id.mycashback, R.id.myaddress, R.id.logout, R.id.et_address})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.editProfileName:
                break;
            case R.id.myprofile:
                Intent intent = new Intent(ProfileActivity.this, EditProfileActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.myorder:
                break;
            case R.id.wishlist:
                Intent intent1 = new Intent(ProfileActivity.this, WishListActivity.class);
                startActivity(intent1);
                break;
            case R.id.handPickedForYou:
                break;
            case R.id.savecads:
                break;
            case R.id.coupons:
                break;
            case R.id.mycashback:
                break;
            case R.id.myaddress:
                Intent intent4 = new Intent(ProfileActivity.this, AddressActivity.class);
                intent4.putExtra(TAGS.FLAG,"false");
                startActivity(intent4);
              /*  AddAddressActivity fr = new AddAddressActivity();
                FragmentManagerUtil.replaceFragment(getSupportFragmentManager(), R.id.coordinatorLayout, fr, false, fr.TAG);*/
       /*         FragmentManager manager = getFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.add(R.id.coordinatorLayout,fr);
                transaction.addToBackStack(null);
                transaction.commit();*/
                break;
            case R.id.et_address:
                changePassword();
                break;
            case R.id.logout:
                LoginManager.getInstance().logOut();
                AppController.getInstance().getPreferenceEditor().putString(TAGS.JSON_USER_DATA, "").commit();
                Intent intent2 = new Intent(ProfileActivity.this, LoginPage.class);
                startActivity(intent2);
                finish();
                break;
        }
    }

    private void changePassword() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.password_activity, null);
        dialogBuilder.setView(dialogView);

        final EditText currentpass = (EditText) dialogView.findViewById(R.id.currentPassword);
        final EditText changepass = (EditText) dialogView.findViewById(R.id.ChangePassword);
        AppCompatButton btnchangepass = (AppCompatButton) dialogView.findViewById(R.id.btn_chng);


        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.setTitle("Change Password");
        alertDialog.show();
        btnchangepass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation(changepass)) {
                    String currentpassword = currentpass.getText().toString();
                    String changedpassword = changepass.getText().toString();
                    AppController.getInstance().getRetrofitServices().changepassword(uid, currentpassword, changedpassword).enqueue(new Callback<DataForCurrentPassword>() {
                        @Override
                        public void onResponse(Call<DataForCurrentPassword> call, Response<DataForCurrentPassword> response) {
                            DataForCurrentPassword dataForCurrentPassword = response.body();
                            if (dataForCurrentPassword.getStatus().equalsIgnoreCase("success")) {
                                Snackbar snackbar = Snackbar.make(coordinatorLayout, dataForCurrentPassword.getMessage(), Snackbar.LENGTH_LONG);
                                snackbar.show();
                                alertDialog.dismiss();
                            } else {
                                Snackbar snackbar = Snackbar.make(coordinatorLayout, dataForCurrentPassword.getMessage(), Snackbar.LENGTH_LONG);
                                snackbar.show();
                                alertDialog.dismiss();
                            }

                        }

                        @Override
                        public void onFailure(Call<DataForCurrentPassword> call, Throwable t) {
                            Toast.makeText(ProfileActivity.this, "Server error can't download the categories", Toast.LENGTH_LONG).show();
                            finish();
                        }
                    });
                }
            }
        });

/*      */
    }

    private boolean validation(EditText changepass) {
        boolean validation = true;
        String password = changepass.getText().toString();
        if (TextUtils.isEmpty(password)) {
            Snackbar snackbar = Snackbar.make(coordinatorLayout, getResources().getString(R.string.error_empty_password), Snackbar.LENGTH_LONG);
            View view = snackbar.getView();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            snackbar.show();
            return false;
        }
        //if no error found
        return validation;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}
