package com.etoile.makeupbarr.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.Utility.CheckinUtility;
import com.etoile.makeupbarr.Utility.TAGS;
import com.etoile.makeupbarr.activity.MainActivity;
import com.etoile.makeupbarr.model.DataForLogin;
import com.etoile.makeupbarr.model.DataForOTPVerify;
import com.etoile.makeupbarr.model.DataForResetPassword;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by AndroidPC on 26-12-2016.
 */

public class ResetPasswordActivity extends AppCompatActivity {
    private static final String TAG = ResetPasswordActivity.class.getSimpleName();

    private static final int SHORT_DELAY = 2000;
    @Bind(R.id.et_newPassword)
    EditText etNewPassword;
    @Bind(R.id.btn_ResetPassword)
    AppCompatButton btnResetPassword;
    @Bind(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    private String newpassword;
    private String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reset_password_activity);
        ButterKnife.bind(this);
        Intent intent=getIntent();
        email = intent.getExtras().getString(TAGS.USER_EMAIL);
    }
    @OnClick(R.id.btn_ResetPassword)
    public void onClick() {
        if (validation())
        {            resetPassword();
        }
    }
    private void resetPassword() {
        AppController.getInstance().showProgressDialog(this);
        Call<DataForResetPassword> mService = AppController.getInstance().getRetrofitServices().resetPassword(email, newpassword);
        mService.enqueue(new Callback<DataForResetPassword>() {
            @Override
            public void onResponse(Call<DataForResetPassword> call, Response<DataForResetPassword> response) {
                AppController.getInstance().hideProgressDialog();
                DataForResetPassword resetpassword_JsonData = response.body();
                if (resetpassword_JsonData.getStatus().equals("success")) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, resetpassword_JsonData.getMessage(),SHORT_DELAY);
                    snackbar.show();
                    TimerTask task = new TimerTask() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(ResetPasswordActivity.this, LoginPage.class);
                            startActivity(intent);
                            finish();
                        }
                    };
                    Timer t = new Timer();
                    t.schedule(task, SHORT_DELAY);

                } else {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout,"this"+ resetpassword_JsonData.getMessage(), Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }

            @Override
            public void onFailure(Call<DataForResetPassword> call, Throwable t) {
                call.cancel();
                Toast.makeText(ResetPasswordActivity.this, "Please check your network connection and internet permission", Toast.LENGTH_LONG).show();
            }
        });

    }
    private boolean validation() {
        boolean validation = true;
        newpassword = etNewPassword.getText().toString().trim();
        if (TextUtils.isEmpty(newpassword)) {
            Snackbar snackbar = Snackbar.make(coordinatorLayout, getResources().getString(R.string.error_empty_newpassword), Snackbar.LENGTH_LONG);
            View view = snackbar.getView();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            snackbar.show();
            return false;
        }else if (!CheckinUtility.isPasswordValid(newpassword)) {
            Snackbar snackbar = Snackbar.make(coordinatorLayout, getResources().getString(R.string.error_invalid_password), Snackbar.LENGTH_LONG);
            View view = snackbar.getView();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            snackbar.show();
            return false;
        }
        //if no error found
        return validation;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}