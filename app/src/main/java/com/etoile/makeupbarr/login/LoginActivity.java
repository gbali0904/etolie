package com.etoile.makeupbarr.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.Utility.TAGS;
import com.etoile.makeupbarr.activity.MainActivity;
import com.etoile.makeupbarr.model.DataForFacebook;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by admin on 12/10/2016.
 */

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    private static final String TAG = LoginActivity.class.getSimpleName();
    LoginButton loginFacebookButton;
    @Bind(R.id.btn_login_for_login_Singup)
    AppCompatButton btnLoginForLoginSingup;
    @Bind(R.id.btn_signupfor_login_Singup)
    AppCompatButton btnSignupforLoginSingup;
    @Bind(R.id.tv_skip)
    TextView tvSkip;
    @Bind(R.id.cooridinate)
    CoordinatorLayout cooridinate;
    private CallbackManager callbackManager;
    private String facebook_id, f_name, m_name, l_name, gender, profile_image, full_name, email_id;
    private SignInButton googleSignInButton;
    private GoogleApiClient mGoogleApiClient;
    private int RC_SIGN_IN = 100;
    private String google_name;
    private String google_email;
    private String glink;
    private String google_f_name;
    private String google_l_name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //for facebook login
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();


        setContentView(R.layout.login_signup_detail_page);
        ButterKnife.bind(this);

        loginFacebookButton = (LoginButton) findViewById(R.id.loginFacebookButton);
        loginFacebookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginWithFb();
            }
        });


        //google
        googleSigninViews();
        //google signin event fire here
        googleSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptGooglesignIn();//this method handle google signin
            }
        });
    }
   //for google
    private void attemptGooglesignIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    private void googleSigninViews() {
        //Initializing google signin option
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        //Initializing signinbutton
        googleSignInButton = (SignInButton) findViewById(R.id.btn_google);
        googleSignInButton.setSize(SignInButton.SIZE_STANDARD);
        googleSignInButton.setScopes(gso.getScopeArray());
        //Initializing google api client
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener*/ )
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }
    //After the signing we are calling this function
    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            if (acct != null) {
                google_name = acct.getDisplayName();
                google_email = acct.getEmail();
                glink = acct.getId();
                google_f_name =acct.getGivenName();
                google_l_name = acct.getFamilyName();
               // Toast.makeText(getApplicationContext(),"success"+"\nname"+ google_name +"\nemail"+ google_email,Toast.LENGTH_LONG).show();
                Log.d("google Creidentials","\nname"+ google_name +"\nemail"+ google_email +"\n givenname"+acct.getGivenName()+"\n"+acct.getFamilyName());
                uploadFBandGoogleData(google_email,google_f_name,google_l_name);
            } else {
                Snackbar snackbar = Snackbar.make(cooridinate, "User info not found", Snackbar.LENGTH_LONG);
                 /*   View view = snackbar.getView();
                    CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
                    params.gravity = Gravity.TOP;
                    view.setLayoutParams(params);*/
                snackbar.show();
            }

        } else {
            Snackbar snackbar = Snackbar.make(cooridinate,"Login Failed", Snackbar.LENGTH_LONG);
                 /*   View view = snackbar.getView();
                    CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
                    params.gravity = Gravity.TOP;
                    view.setLayoutParams(params);*/
            snackbar.show();
        }
    }
    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Snackbar snackbar = Snackbar.make(cooridinate,""+result, Snackbar.LENGTH_LONG);
                 /*   View view = snackbar.getView();
                    CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
                    params.gravity = Gravity.TOP;
                    view.setLayoutParams(params);*/
        snackbar.show();
        if (!result.hasResolution()) {
            Toast.makeText(getApplicationContext(), "Login Error",Toast.LENGTH_LONG).show();
            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this, 0).show();
            return;
        }
    }
    //google close
    //for facebook
    private void loginWithFb() {
        loginFacebookButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                System.out.println("Facebook Login Successful!");
                System.out.println("Logged in user Details : ");
                System.out.println("--------------------------");
                System.out.println("User ID  : " + loginResult.getAccessToken().getUserId());
                System.out.println("Authentication Token : " + loginResult.getAccessToken().getToken());
                //  Toast.makeText(LoginActivity.this, "Login Successful!", Toast.LENGTH_LONG).show();
                fetchFacebookUserProfile(loginResult);
            }

            @Override
            public void onCancel() {
                //   Toast.makeText(LoginActivity.this, "Login cancelled by user!", Toast.LENGTH_LONG).show();
                System.out.println("Facebook Login failed!!");
                Snackbar snackbar = Snackbar.make(cooridinate, "Login cancelled by user!", Snackbar.LENGTH_LONG);
                 /*   View view = snackbar.getView();
                    CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
                    params.gravity = Gravity.TOP;
                    view.setLayoutParams(params);*/
                snackbar.show();

            }

            @Override
            public void onError(FacebookException e) {
                // Toast.makeText(LoginActivity.this, "Login unsuccessful!", Toast.LENGTH_LONG).show();
                System.out.println("Facebook Login failed!!");
                Snackbar snackbar = Snackbar.make(cooridinate, "Login unsuccessful!", Snackbar.LENGTH_LONG);
                 /*   View view = snackbar.getView();
                    CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
                    params.gravity = Gravity.TOP;
                    view.setLayoutParams(params);*/
                snackbar.show();
            }
        });
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends", "email"));
    }
    private void fetchFacebookUserProfile(LoginResult loginResult) {


        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {
                            email_id = object.getString("email");
                            Profile profile = Profile.getCurrentProfile();
                            if (profile != null) {
                                facebook_id = profile.getId();
                                f_name = profile.getFirstName();
                                m_name = profile.getMiddleName();
                                l_name = profile.getLastName();
                                full_name = profile.getName();
                                profile_image = profile.getProfilePictureUri(400, 400).toString();
                            }

                            Log.d(TAG, "facebook graph : profile info:" +
                                    "\nName=" + full_name +
                                    "\nemail=" + email_id +
                                    "\ngoogle_f_name=" + f_name +
                                    "\ngoogle_l_name=" + l_name);
                            uploadFBandGoogleData(email_id, f_name, l_name);
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            //  e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,email,name,link,picture.width(400).height(300)");
        request.setParameters(parameters);
        request.executeAsync();
    }
    //facebook close
    //cheking server data wth facebook and google
    private void uploadFBandGoogleData(String email, String fisrtName, String LastName) {
        AppController.getInstance().showProgressDialog(this);
        Call<DataForFacebook> mService = AppController.getInstance().getRetrofitServices().loginwithfacebbok(fisrtName, LastName,email);
        mService.enqueue(new Callback<DataForFacebook>() {
            @Override
            public void onResponse(Call<DataForFacebook> call, Response<DataForFacebook> response) {
                AppController.getInstance().hideProgressDialog();
                DataForFacebook facebook_login_json = response.body();
                String jsonValuefornLogin = AppController.getInstance().getGson().toJson(facebook_login_json);
                AppController.getInstance().getPreferenceEditor().putString(TAGS.JSON_USER_DATA, jsonValuefornLogin).commit();
                // Receiving side
                if (facebook_login_json.getStatus().equals("success")) {//register successfull
                    Log.d(TAG, "login successful");
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();

                } else {
                    Snackbar snackbar = Snackbar.make(cooridinate, facebook_login_json.getMessage(), Snackbar.LENGTH_LONG);
                 /*   View view = snackbar.getView();
                    CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
                    params.gravity = Gravity.TOP;
                    view.setLayoutParams(params);*/
                    snackbar.show();

                }
            }

            @Override
            public void onFailure(Call<DataForFacebook> call, Throwable t) {
                call.cancel();
                Toast.makeText(LoginActivity.this, "Please check your network connection and internet permission", Toast.LENGTH_LONG).show();
            }
        });

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        //If signin
        if (requestCode == RC_SIGN_IN) {
            // Toast.makeText(this, "User is connected!", Toast.LENGTH_LONG).show();
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }

    }
    @OnClick({R.id.btn_login_for_login_Singup, R.id.btn_signupfor_login_Singup, R.id.tv_skip})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login_for_login_Singup:
                Intent intent = new Intent(LoginActivity.this, LoginPage.class);
                startActivity(intent);
                finish();
                break;
            case R.id.btn_signupfor_login_Singup:
                Intent intent1 = new Intent(LoginActivity.this, SignUp.class);
                startActivity(intent1);
                finish();
                break;
            case R.id.tv_skip:
                Intent intent2 = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent2);
                finish();
                break;
        }
    }
}
