package com.etoile.makeupbarr.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.Utility.TAGS;
import com.etoile.makeupbarr.activity.MainActivity;
import com.etoile.makeupbarr.model.DataForDrawerCategories;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {

    private static final String TAG = SplashActivity.class.getSimpleName();
    @Bind(R.id.tv_title)
    TextView tvTitle;
    private static int Splash_TIME_DELEY = 300;
    private boolean shouldStartfromLogin;
    private boolean aBoolean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);
        ButterKnife.bind(this);

        AppController.getInstance().getRetrofitServices().category().enqueue(new Callback<DataForDrawerCategories>() {
            @Override
            public void onResponse(Call<DataForDrawerCategories> call, Response<DataForDrawerCategories> response) {
                DataForDrawerCategories.setInstance(response.body());
                DataForDrawerCategories categoryjson = response.body();
                String jsonValueforCategory = AppController.getInstance().getGson().toJson(categoryjson);
                AppController.getInstance().getPreferenceEditor().putString(TAGS.JSON_CATEGORY, jsonValueforCategory).commit();
                if(categoryjson.getStatus().equalsIgnoreCase("success")) {
                    nextActivity();
                }
            }

            @Override
            public void onFailure(Call<DataForDrawerCategories> call, Throwable t) {
                Toast.makeText(SplashActivity.this, "       Try Again !!"+"\n"+"Check Your Internet !!", Toast.LENGTH_LONG).show();
               // finish();
            }
        });


    }

    private void nextActivity() {
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                if (!AppController.getPreferences(TAGS.JSON_USER_DATA, "").equalsIgnoreCase("")) {
                    Intent i = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        };
        Timer t = new Timer();
        t.schedule(task, Splash_TIME_DELEY);
    }


}
