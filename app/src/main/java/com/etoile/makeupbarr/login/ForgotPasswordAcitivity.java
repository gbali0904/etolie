package com.etoile.makeupbarr.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.Utility.CheckinUtility;
import com.etoile.makeupbarr.Utility.TAGS;
import com.etoile.makeupbarr.model.DataForForgotPassword;
import com.etoile.makeupbarr.model.DataForOTPVerify;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by AndroidPC on 26-12-2016.
 */

public class ForgotPasswordAcitivity extends AppCompatActivity {
    private static final String TAG = ForgotPasswordAcitivity.class.getSimpleName();
    @Bind(R.id.et_newPassword)
    EditText etEmail;
    @Bind(R.id.Et_otp)
    EditText etOtp;
    @Bind(R.id.btn_ResetPassword)
    AppCompatButton btnforgot;
    @Bind(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @Bind(R.id.btn_otpverify)
    AppCompatButton btnOTP;
    TextView tvRetry;
    private String email_id;
    private String otp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password_activity);
        ButterKnife.bind(this);
        tvRetry=(TextView) findViewById(R.id.tv_retry);
        tvRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getForgotPassword();
            }
        });
    }


    @OnClick({R.id.btn_ResetPassword, R.id.btn_otpverify})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_ResetPassword:
                if (validation()) {
                    getForgotPassword();
                }
                break;
            case R.id.btn_otpverify:
                if (otpValidation()) {
                    getOTPVerify();
                }
        }

    }

    private void getForgotPassword() {
        AppController.getInstance().showProgressDialog(this);
        Call<DataForForgotPassword> mService = AppController.getInstance().getRetrofitServices().forgotPassword(email_id);
        mService.enqueue(new Callback<DataForForgotPassword>() {
            @Override
            public void onResponse(Call<DataForForgotPassword> call, Response<DataForForgotPassword> response) {
                AppController.getInstance().hideProgressDialog();
                DataForForgotPassword forgotJsonData = response.body();
                if (forgotJsonData.getStatus().equals("success")) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, forgotJsonData.getMessage(), Snackbar.LENGTH_LONG);
                    snackbar.show();
                    etOtp.setVisibility(View.VISIBLE);
                    etEmail.setVisibility(View.GONE);
                    btnforgot.setVisibility(View.GONE);
                    tvRetry.setVisibility(View.VISIBLE);
                    btnOTP.setVisibility(View.VISIBLE);
                } else {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, forgotJsonData.getMessage(), Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }

            @Override
            public void onFailure(Call<DataForForgotPassword> call, Throwable t) {
                call.cancel();
                Toast.makeText(ForgotPasswordAcitivity.this, "Please check your network connection and internet permission", Toast.LENGTH_LONG).show();
            }
        });

    }

    private void getOTPVerify() {
        AppController.getInstance().showProgressDialog(this);
        Call<DataForOTPVerify> mService = AppController.getInstance().getRetrofitServices().otpVerify(email_id, otp);
        mService.enqueue(new Callback<DataForOTPVerify>() {
            @Override
            public void onResponse(Call<DataForOTPVerify> call, Response<DataForOTPVerify> response) {
                AppController.getInstance().hideProgressDialog();
                DataForOTPVerify OTPverifyJsonData = response.body();
                if (OTPverifyJsonData.getStatus().equals("success")) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, OTPverifyJsonData.getMessage(), Snackbar.LENGTH_LONG);
                    snackbar.show();
                    Intent intent = new Intent(ForgotPasswordAcitivity.this, ResetPasswordActivity.class);
                    intent.putExtra(TAGS.USER_EMAIL, email_id);
                    startActivity(intent);
                    finish();
                } else {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, OTPverifyJsonData.getMessage(), Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }

            @Override
            public void onFailure(Call<DataForOTPVerify> call, Throwable t) {
                call.cancel();
                Toast.makeText(ForgotPasswordAcitivity.this, "Please check your network connection and internet permission", Toast.LENGTH_LONG).show();
            }
        });

    }

    private boolean validation() {
        boolean validation = true;
        email_id = etEmail.getText().toString().trim();
        AppController.getInstance().getPreferenceEditor().putString(TAGS.USER_EMAIL, email_id).commit();
        if (TextUtils.isEmpty(email_id)) {
            Snackbar snackbar = Snackbar.make(coordinatorLayout, getResources().getString(R.string.error_empty_email), Snackbar.LENGTH_LONG);
            View view = snackbar.getView();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            snackbar.show();

            return false;
        } else if (!CheckinUtility.isEmailValid(email_id)) {
            Snackbar snackbar = Snackbar.make(coordinatorLayout, getResources().getString(R.string.error_invalid_email), Snackbar.LENGTH_LONG);
            View view = snackbar.getView();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            snackbar.show();
        }
        return validation;
    }

    private boolean otpValidation() {

        boolean otpValidation = true;
        otp = etOtp.getText().toString().trim();
        if (TextUtils.isEmpty(otp)) {
            Snackbar snackbar = Snackbar.make(coordinatorLayout, getResources().getString(R.string.error_empty_OTP), Snackbar.LENGTH_LONG);
            View view = snackbar.getView();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            snackbar.show();
            return false;
        }
        return otpValidation;

    }


}
