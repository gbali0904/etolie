package com.etoile.makeupbarr.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.Utility.CheckinUtility;
import com.etoile.makeupbarr.Utility.TAGS;
import com.etoile.makeupbarr.activity.MainActivity;
import com.etoile.makeupbarr.model.DataForLogin;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by admin on 12/10/2016.
 */

public class LoginPage extends AppCompatActivity {


    private static final String TAG = LoginPage.class.getSimpleName();
    private static LoginPage mInstance;
    @Bind(R.id.et_email)
    EditText etEmail;
    @Bind(R.id.et_password)
    EditText etPassword;
    @Bind(R.id.btn_login)
    AppCompatButton btnLogin;
    @Bind(R.id.forgotPassword)
    TextView tvForgetPassword;
    @Bind(R.id.errorLine)
    TextView errorLine;
    @Bind(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;


    private String email_id;
    private String password;

    public LoginPage() {
        // Required empty public constructor
    }

    public static LoginPage getInstance() {
        if (mInstance == null)
            mInstance = new LoginPage();
        return new LoginPage();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_page);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.btn_login, R.id.forgotPassword})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login:
                if (validation()) {
                    //  progress.setVisibility(View.VISIBLE);
                    loginProcessWithRetrofit(email_id, password);

                    //   login();
                }
                break;
            case R.id.forgotPassword:
                Intent intent=new Intent(LoginPage.this,ForgotPasswordAcitivity.class);
                startActivity(intent);
                break;
        }
    }

    private void loginProcessWithRetrofit(final String email, String password) {
        AppController.getInstance().showProgressDialog(this);
        Call<DataForLogin> mService = AppController.getInstance().getRetrofitServices().login(email, password);
        mService.enqueue(new Callback<DataForLogin>() {
            @Override
            public void onResponse(Call<DataForLogin> call, Response<DataForLogin> response) {
                AppController.getInstance().hideProgressDialog();
                DataForLogin loginJsonData = response.body();
                String jsonValuefornLogin = AppController.getInstance().getGson().toJson(loginJsonData);
                Log.e(TAG,jsonValuefornLogin);
                AppController.getInstance().getPreferenceEditor().putString(TAGS.JSON_USER_DATA, jsonValuefornLogin).commit();
                // Receiving side
                if (loginJsonData.getMessage().equals("Login Successfully")) {//register successfull
                    Log.d(TAG, "login successful");
                    Intent intent = new Intent(LoginPage.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, loginJsonData.getMessage(), Snackbar.LENGTH_LONG);
                 /*   View view = snackbar.getView();
                    CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
                    params.gravity = Gravity.TOP;
                    view.setLayoutParams(params);*/
                    snackbar.show();

                }
            }

            @Override
            public void onFailure(Call<DataForLogin> call, Throwable t) {
                call.cancel();
                Toast.makeText(LoginPage.this, "Please check your network connection and internet permission", Toast.LENGTH_LONG).show();
            }
        });
    }

    private boolean validation() {
        boolean validation = true;
        email_id = etEmail.getText().toString().trim();
        password = etPassword.getText().toString().trim();
        if (TextUtils.isEmpty(email_id)) {
            Snackbar snackbar = Snackbar.make(coordinatorLayout, getResources().getString(R.string.error_empty_email), Snackbar.LENGTH_LONG);
            View view = snackbar.getView();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            snackbar.show();

            return false;
        } else if (!CheckinUtility.isEmailValid(email_id)) {
            Snackbar snackbar = Snackbar.make(coordinatorLayout, getResources().getString(R.string.error_invalid_email), Snackbar.LENGTH_LONG);
            View view = snackbar.getView();

            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);

            snackbar.show();
        }
        if (TextUtils.isEmpty(password)) {
            Snackbar snackbar = Snackbar.make(coordinatorLayout, getResources().getString(R.string.error_empty_password), Snackbar.LENGTH_LONG);
            View view = snackbar.getView();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            snackbar.show();
            return false;
        }
        //if no error found
        return validation;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(LoginPage.this, LoginActivity.class);
        startActivity(intent);
        finish();
        super.onBackPressed();

    }
}
