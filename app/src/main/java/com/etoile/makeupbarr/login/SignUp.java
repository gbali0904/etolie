package com.etoile.makeupbarr.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.etoile.makeupbarr.R;
import com.etoile.makeupbarr.Utility.AppController;
import com.etoile.makeupbarr.Utility.CheckinUtility;
import com.etoile.makeupbarr.Utility.TAGS;
import com.etoile.makeupbarr.activity.MainActivity;
import com.etoile.makeupbarr.model.DataForRegister;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by admin on 12/10/2016.
 */
public class SignUp extends AppCompatActivity {


    private static final String TAG = SignUp.class.getSimpleName();
    @Bind(R.id.et_email)
    EditText etEmail;
    @Bind(R.id.et_phone)
    EditText etPhone;
    @Bind(R.id.et_password)
    EditText etPassword;
    @Bind(R.id.btn_addressadd)
    AppCompatButton btnSignup;
    @Bind(R.id.forgotPassword)
    TextView checkPrivacy;
    @Bind(R.id.myprofile)
    CoordinatorLayout singUpLayout;
    @Bind(R.id.et_fname)
    EditText etFname;
    private String email;
    private String password;
    private String phone;
    private String fist_name;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_page);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.btn_addressadd})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_addressadd:
                if (validation()) {
                    registerForUser();
                }
                break;
        }
    }

    private void registerForUser() {

        AppController.getInstance().showProgressDialog(this);
        Call<DataForRegister> mService = AppController.getInstance().getRetrofitServices().register(fist_name, email, phone, password);
        mService.enqueue(new Callback<DataForRegister>() {
            @Override
            public void onResponse(Call<DataForRegister> call, Response<DataForRegister> response) {
                AppController.getInstance().hideProgressDialog();
                DataForRegister registerJsonData = response.body();
                String jsonValuefornRegister = AppController.getInstance().getGson().toJson(registerJsonData);
                AppController.getInstance().getPreferenceEditor().putString(TAGS.JSON_USER_DATA, jsonValuefornRegister).commit();
                if (registerJsonData.getStatus().equals("success")) {//register successfull
                    Log.d(TAG, "login successful");
                    Intent intent = new Intent(SignUp.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Snackbar snackbar = Snackbar.make(singUpLayout, registerJsonData.getMessage(), Snackbar.LENGTH_LONG);
                    View view = snackbar.getView();
                    CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
                    params.gravity = Gravity.TOP;
                    view.setLayoutParams(params);
                    snackbar.show();
                }
            }

            @Override
            public void onFailure(Call<DataForRegister> call, Throwable t) {
                call.cancel();
                Toast.makeText(SignUp.this, "Please check your network connection and internet permission", Toast.LENGTH_LONG).show();

            }
        });
    }

    private boolean validation() {
        boolean validation = true;
        email = etEmail.getText().toString().trim();
        password = etPassword.getText().toString().trim();
        phone = etPhone.getText().toString().trim();
        fist_name = etFname.getText().toString();


        //for First_name
        if (TextUtils.isEmpty(fist_name)) {
            Snackbar snackbar = Snackbar.make(singUpLayout, getResources().getString(R.string.error_first_name), Snackbar.LENGTH_LONG);
            View view = snackbar.getView();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            snackbar.show();
            return false;
        }

           //for email
        if (TextUtils.isEmpty(email)) {
            Snackbar snackbar = Snackbar.make(singUpLayout, getResources().getString(R.string.error_empty_email), Snackbar.LENGTH_LONG);
            View view = snackbar.getView();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            snackbar.show();
            return false;
        } else if (!CheckinUtility.isEmailValid(email)) {
            Snackbar snackbar = Snackbar.make(singUpLayout, getResources().getString(R.string.error_invalid_email), Snackbar.LENGTH_LONG);
            View view = snackbar.getView();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            snackbar.show();
            return false;
        }

        //for phone number
        if (TextUtils.isEmpty(phone)) {
            Snackbar snackbar = Snackbar.make(singUpLayout, getResources().getString(R.string.error_mobile), Snackbar.LENGTH_LONG);
            View view = snackbar.getView();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            snackbar.show();
            return false;
        } else if (!CheckinUtility.isPhoneNumberValid(phone)) {
            Snackbar snackbar = Snackbar.make(singUpLayout, getResources().getString(R.string.error_invalid_mobile), Snackbar.LENGTH_LONG);
            View view = snackbar.getView();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            snackbar.show();
            return false;
        }
        //for password
        if (TextUtils.isEmpty(password)) {
            Snackbar snackbar = Snackbar.make(singUpLayout, getResources().getString(R.string.error_empty_password), Snackbar.LENGTH_LONG);
            View view = snackbar.getView();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            snackbar.show();
            return false;
        } else if (!CheckinUtility.isPasswordValid(password)) {
            Snackbar snackbar = Snackbar.make(singUpLayout, getResources().getString(R.string.error_invalid_password), Snackbar.LENGTH_LONG);
            View view = snackbar.getView();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            snackbar.show();
            return false;
        }

        //if no error found
        return validation;
    }
}
